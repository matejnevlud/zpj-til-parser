// Generated from TILGrammar.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link TILGrammarParser}.
 */
public interface TILGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(TILGrammarParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(TILGrammarParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#sentence}.
	 * @param ctx the parse tree
	 */
	void enterSentence(TILGrammarParser.SentenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#sentence}.
	 * @param ctx the parse tree
	 */
	void exitSentence(TILGrammarParser.SentenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#sentence_content}.
	 * @param ctx the parse tree
	 */
	void enterSentence_content(TILGrammarParser.Sentence_contentContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#sentence_content}.
	 * @param ctx the parse tree
	 */
	void exitSentence_content(TILGrammarParser.Sentence_contentContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#termination}.
	 * @param ctx the parse tree
	 */
	void enterTermination(TILGrammarParser.TerminationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#termination}.
	 * @param ctx the parse tree
	 */
	void exitTermination(TILGrammarParser.TerminationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#type_definition}.
	 * @param ctx the parse tree
	 */
	void enterType_definition(TILGrammarParser.Type_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#type_definition}.
	 * @param ctx the parse tree
	 */
	void exitType_definition(TILGrammarParser.Type_definitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#entity_definition}.
	 * @param ctx the parse tree
	 */
	void enterEntity_definition(TILGrammarParser.Entity_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#entity_definition}.
	 * @param ctx the parse tree
	 */
	void exitEntity_definition(TILGrammarParser.Entity_definitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#construction}.
	 * @param ctx the parse tree
	 */
	void enterConstruction(TILGrammarParser.ConstructionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#construction}.
	 * @param ctx the parse tree
	 */
	void exitConstruction(TILGrammarParser.ConstructionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#global_variable_definition}.
	 * @param ctx the parse tree
	 */
	void enterGlobal_variable_definition(TILGrammarParser.Global_variable_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#global_variable_definition}.
	 * @param ctx the parse tree
	 */
	void exitGlobal_variable_definition(TILGrammarParser.Global_variable_definitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#data_type}.
	 * @param ctx the parse tree
	 */
	void enterData_type(TILGrammarParser.Data_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#data_type}.
	 * @param ctx the parse tree
	 */
	void exitData_type(TILGrammarParser.Data_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#embeded_type}.
	 * @param ctx the parse tree
	 */
	void enterEmbeded_type(TILGrammarParser.Embeded_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#embeded_type}.
	 * @param ctx the parse tree
	 */
	void exitEmbeded_type(TILGrammarParser.Embeded_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#list_type}.
	 * @param ctx the parse tree
	 */
	void enterList_type(TILGrammarParser.List_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#list_type}.
	 * @param ctx the parse tree
	 */
	void exitList_type(TILGrammarParser.List_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#touple_type}.
	 * @param ctx the parse tree
	 */
	void enterTouple_type(TILGrammarParser.Touple_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#touple_type}.
	 * @param ctx the parse tree
	 */
	void exitTouple_type(TILGrammarParser.Touple_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#user_type}.
	 * @param ctx the parse tree
	 */
	void enterUser_type(TILGrammarParser.User_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#user_type}.
	 * @param ctx the parse tree
	 */
	void exitUser_type(TILGrammarParser.User_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#enclosed_data_type}.
	 * @param ctx the parse tree
	 */
	void enterEnclosed_data_type(TILGrammarParser.Enclosed_data_typeContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#enclosed_data_type}.
	 * @param ctx the parse tree
	 */
	void exitEnclosed_data_type(TILGrammarParser.Enclosed_data_typeContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(TILGrammarParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(TILGrammarParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#trivialisation}.
	 * @param ctx the parse tree
	 */
	void enterTrivialisation(TILGrammarParser.TrivialisationContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#trivialisation}.
	 * @param ctx the parse tree
	 */
	void exitTrivialisation(TILGrammarParser.TrivialisationContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#composition}.
	 * @param ctx the parse tree
	 */
	void enterComposition(TILGrammarParser.CompositionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#composition}.
	 * @param ctx the parse tree
	 */
	void exitComposition(TILGrammarParser.CompositionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#closure}.
	 * @param ctx the parse tree
	 */
	void enterClosure(TILGrammarParser.ClosureContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#closure}.
	 * @param ctx the parse tree
	 */
	void exitClosure(TILGrammarParser.ClosureContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#lambda_variables}.
	 * @param ctx the parse tree
	 */
	void enterLambda_variables(TILGrammarParser.Lambda_variablesContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#lambda_variables}.
	 * @param ctx the parse tree
	 */
	void exitLambda_variables(TILGrammarParser.Lambda_variablesContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#typed_variables}.
	 * @param ctx the parse tree
	 */
	void enterTyped_variables(TILGrammarParser.Typed_variablesContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#typed_variables}.
	 * @param ctx the parse tree
	 */
	void exitTyped_variables(TILGrammarParser.Typed_variablesContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#typed_variable}.
	 * @param ctx the parse tree
	 */
	void enterTyped_variable(TILGrammarParser.Typed_variableContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#typed_variable}.
	 * @param ctx the parse tree
	 */
	void exitTyped_variable(TILGrammarParser.Typed_variableContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#n_execution}.
	 * @param ctx the parse tree
	 */
	void enterN_execution(TILGrammarParser.N_executionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#n_execution}.
	 * @param ctx the parse tree
	 */
	void exitN_execution(TILGrammarParser.N_executionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#entity}.
	 * @param ctx the parse tree
	 */
	void enterEntity(TILGrammarParser.EntityContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#entity}.
	 * @param ctx the parse tree
	 */
	void exitEntity(TILGrammarParser.EntityContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#type_name}.
	 * @param ctx the parse tree
	 */
	void enterType_name(TILGrammarParser.Type_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#type_name}.
	 * @param ctx the parse tree
	 */
	void exitType_name(TILGrammarParser.Type_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#entity_name}.
	 * @param ctx the parse tree
	 */
	void enterEntity_name(TILGrammarParser.Entity_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#entity_name}.
	 * @param ctx the parse tree
	 */
	void exitEntity_name(TILGrammarParser.Entity_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#variable_name}.
	 * @param ctx the parse tree
	 */
	void enterVariable_name(TILGrammarParser.Variable_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#variable_name}.
	 * @param ctx the parse tree
	 */
	void exitVariable_name(TILGrammarParser.Variable_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#keyword}.
	 * @param ctx the parse tree
	 */
	void enterKeyword(TILGrammarParser.KeywordContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#keyword}.
	 * @param ctx the parse tree
	 */
	void exitKeyword(TILGrammarParser.KeywordContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#symbols}.
	 * @param ctx the parse tree
	 */
	void enterSymbols(TILGrammarParser.SymbolsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#symbols}.
	 * @param ctx the parse tree
	 */
	void exitSymbols(TILGrammarParser.SymbolsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#anything}.
	 * @param ctx the parse tree
	 */
	void enterAnything(TILGrammarParser.AnythingContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#anything}.
	 * @param ctx the parse tree
	 */
	void exitAnything(TILGrammarParser.AnythingContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#number}.
	 * @param ctx the parse tree
	 */
	void enterNumber(TILGrammarParser.NumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#number}.
	 * @param ctx the parse tree
	 */
	void exitNumber(TILGrammarParser.NumberContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#upperletter_name}.
	 * @param ctx the parse tree
	 */
	void enterUpperletter_name(TILGrammarParser.Upperletter_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#upperletter_name}.
	 * @param ctx the parse tree
	 */
	void exitUpperletter_name(TILGrammarParser.Upperletter_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#lowerletter_name}.
	 * @param ctx the parse tree
	 */
	void enterLowerletter_name(TILGrammarParser.Lowerletter_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#lowerletter_name}.
	 * @param ctx the parse tree
	 */
	void exitLowerletter_name(TILGrammarParser.Lowerletter_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#quoted_name}.
	 * @param ctx the parse tree
	 */
	void enterQuoted_name(TILGrammarParser.Quoted_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#quoted_name}.
	 * @param ctx the parse tree
	 */
	void exitQuoted_name(TILGrammarParser.Quoted_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#whitespace}.
	 * @param ctx the parse tree
	 */
	void enterWhitespace(TILGrammarParser.WhitespaceContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#whitespace}.
	 * @param ctx the parse tree
	 */
	void exitWhitespace(TILGrammarParser.WhitespaceContext ctx);
	/**
	 * Enter a parse tree produced by {@link TILGrammarParser#optional_whitespace}.
	 * @param ctx the parse tree
	 */
	void enterOptional_whitespace(TILGrammarParser.Optional_whitespaceContext ctx);
	/**
	 * Exit a parse tree produced by {@link TILGrammarParser#optional_whitespace}.
	 * @param ctx the parse tree
	 */
	void exitOptional_whitespace(TILGrammarParser.Optional_whitespaceContext ctx);
}