
// Generated from TILGrammar.g4 by ANTLR 4.9.2

#pragma once


#include "antlr4-runtime.h"
#include "TILGrammarListener.h"


/**
 * This class provides an empty implementation of TILGrammarListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  TILGrammarBaseListener : public TILGrammarListener {
public:

  virtual void enterStart(TILGrammarParser::StartContext * /*ctx*/) override { }
  virtual void exitStart(TILGrammarParser::StartContext * /*ctx*/) override { }

  virtual void enterSentence(TILGrammarParser::SentenceContext * /*ctx*/) override { }
  virtual void exitSentence(TILGrammarParser::SentenceContext * /*ctx*/) override { }

  virtual void enterSentence_content(TILGrammarParser::Sentence_contentContext * /*ctx*/) override { }
  virtual void exitSentence_content(TILGrammarParser::Sentence_contentContext * /*ctx*/) override { }

  virtual void enterTermination(TILGrammarParser::TerminationContext * /*ctx*/) override { }
  virtual void exitTermination(TILGrammarParser::TerminationContext * /*ctx*/) override { }

  virtual void enterType_definition(TILGrammarParser::Type_definitionContext * /*ctx*/) override { }
  virtual void exitType_definition(TILGrammarParser::Type_definitionContext * /*ctx*/) override { }

  virtual void enterEntity_definition(TILGrammarParser::Entity_definitionContext * /*ctx*/) override { }
  virtual void exitEntity_definition(TILGrammarParser::Entity_definitionContext * /*ctx*/) override { }

  virtual void enterConstruction(TILGrammarParser::ConstructionContext * /*ctx*/) override { }
  virtual void exitConstruction(TILGrammarParser::ConstructionContext * /*ctx*/) override { }

  virtual void enterGlobal_variable_definition(TILGrammarParser::Global_variable_definitionContext * /*ctx*/) override { }
  virtual void exitGlobal_variable_definition(TILGrammarParser::Global_variable_definitionContext * /*ctx*/) override { }

  virtual void enterData_type(TILGrammarParser::Data_typeContext * /*ctx*/) override { }
  virtual void exitData_type(TILGrammarParser::Data_typeContext * /*ctx*/) override { }

  virtual void enterEmbeded_type(TILGrammarParser::Embeded_typeContext * /*ctx*/) override { }
  virtual void exitEmbeded_type(TILGrammarParser::Embeded_typeContext * /*ctx*/) override { }

  virtual void enterList_type(TILGrammarParser::List_typeContext * /*ctx*/) override { }
  virtual void exitList_type(TILGrammarParser::List_typeContext * /*ctx*/) override { }

  virtual void enterTouple_type(TILGrammarParser::Touple_typeContext * /*ctx*/) override { }
  virtual void exitTouple_type(TILGrammarParser::Touple_typeContext * /*ctx*/) override { }

  virtual void enterUser_type(TILGrammarParser::User_typeContext * /*ctx*/) override { }
  virtual void exitUser_type(TILGrammarParser::User_typeContext * /*ctx*/) override { }

  virtual void enterEnclosed_data_type(TILGrammarParser::Enclosed_data_typeContext * /*ctx*/) override { }
  virtual void exitEnclosed_data_type(TILGrammarParser::Enclosed_data_typeContext * /*ctx*/) override { }

  virtual void enterVariable(TILGrammarParser::VariableContext * /*ctx*/) override { }
  virtual void exitVariable(TILGrammarParser::VariableContext * /*ctx*/) override { }

  virtual void enterTrivialisation(TILGrammarParser::TrivialisationContext * /*ctx*/) override { }
  virtual void exitTrivialisation(TILGrammarParser::TrivialisationContext * /*ctx*/) override { }

  virtual void enterComposition(TILGrammarParser::CompositionContext * /*ctx*/) override { }
  virtual void exitComposition(TILGrammarParser::CompositionContext * /*ctx*/) override { }

  virtual void enterClosure(TILGrammarParser::ClosureContext * /*ctx*/) override { }
  virtual void exitClosure(TILGrammarParser::ClosureContext * /*ctx*/) override { }

  virtual void enterLambda_variables(TILGrammarParser::Lambda_variablesContext * /*ctx*/) override { }
  virtual void exitLambda_variables(TILGrammarParser::Lambda_variablesContext * /*ctx*/) override { }

  virtual void enterTyped_variables(TILGrammarParser::Typed_variablesContext * /*ctx*/) override { }
  virtual void exitTyped_variables(TILGrammarParser::Typed_variablesContext * /*ctx*/) override { }

  virtual void enterTyped_variable(TILGrammarParser::Typed_variableContext * /*ctx*/) override { }
  virtual void exitTyped_variable(TILGrammarParser::Typed_variableContext * /*ctx*/) override { }

  virtual void enterN_execution(TILGrammarParser::N_executionContext * /*ctx*/) override { }
  virtual void exitN_execution(TILGrammarParser::N_executionContext * /*ctx*/) override { }

  virtual void enterEntity(TILGrammarParser::EntityContext * /*ctx*/) override { }
  virtual void exitEntity(TILGrammarParser::EntityContext * /*ctx*/) override { }

  virtual void enterType_name(TILGrammarParser::Type_nameContext * /*ctx*/) override { }
  virtual void exitType_name(TILGrammarParser::Type_nameContext * /*ctx*/) override { }

  virtual void enterEntity_name(TILGrammarParser::Entity_nameContext * /*ctx*/) override { }
  virtual void exitEntity_name(TILGrammarParser::Entity_nameContext * /*ctx*/) override { }

  virtual void enterVariable_name(TILGrammarParser::Variable_nameContext * /*ctx*/) override { }
  virtual void exitVariable_name(TILGrammarParser::Variable_nameContext * /*ctx*/) override { }

  virtual void enterKeyword(TILGrammarParser::KeywordContext * /*ctx*/) override { }
  virtual void exitKeyword(TILGrammarParser::KeywordContext * /*ctx*/) override { }

  virtual void enterSymbols(TILGrammarParser::SymbolsContext * /*ctx*/) override { }
  virtual void exitSymbols(TILGrammarParser::SymbolsContext * /*ctx*/) override { }

  virtual void enterAnything(TILGrammarParser::AnythingContext * /*ctx*/) override { }
  virtual void exitAnything(TILGrammarParser::AnythingContext * /*ctx*/) override { }

  virtual void enterNumber(TILGrammarParser::NumberContext * /*ctx*/) override { }
  virtual void exitNumber(TILGrammarParser::NumberContext * /*ctx*/) override { }

  virtual void enterUpperletter_name(TILGrammarParser::Upperletter_nameContext * /*ctx*/) override { }
  virtual void exitUpperletter_name(TILGrammarParser::Upperletter_nameContext * /*ctx*/) override { }

  virtual void enterLowerletter_name(TILGrammarParser::Lowerletter_nameContext * /*ctx*/) override { }
  virtual void exitLowerletter_name(TILGrammarParser::Lowerletter_nameContext * /*ctx*/) override { }

  virtual void enterQuoted_name(TILGrammarParser::Quoted_nameContext * /*ctx*/) override { }
  virtual void exitQuoted_name(TILGrammarParser::Quoted_nameContext * /*ctx*/) override { }

  virtual void enterWhitespace(TILGrammarParser::WhitespaceContext * /*ctx*/) override { }
  virtual void exitWhitespace(TILGrammarParser::WhitespaceContext * /*ctx*/) override { }

  virtual void enterOptional_whitespace(TILGrammarParser::Optional_whitespaceContext * /*ctx*/) override { }
  virtual void exitOptional_whitespace(TILGrammarParser::Optional_whitespaceContext * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

