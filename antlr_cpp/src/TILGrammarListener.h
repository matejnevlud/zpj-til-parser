
// Generated from TILGrammar.g4 by ANTLR 4.9.2

#pragma once


#include "antlr4-runtime.h"
#include "TILGrammarParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by TILGrammarParser.
 */
class  TILGrammarListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterStart(TILGrammarParser::StartContext *ctx) = 0;
  virtual void exitStart(TILGrammarParser::StartContext *ctx) = 0;

  virtual void enterSentence(TILGrammarParser::SentenceContext *ctx) = 0;
  virtual void exitSentence(TILGrammarParser::SentenceContext *ctx) = 0;

  virtual void enterSentence_content(TILGrammarParser::Sentence_contentContext *ctx) = 0;
  virtual void exitSentence_content(TILGrammarParser::Sentence_contentContext *ctx) = 0;

  virtual void enterTermination(TILGrammarParser::TerminationContext *ctx) = 0;
  virtual void exitTermination(TILGrammarParser::TerminationContext *ctx) = 0;

  virtual void enterType_definition(TILGrammarParser::Type_definitionContext *ctx) = 0;
  virtual void exitType_definition(TILGrammarParser::Type_definitionContext *ctx) = 0;

  virtual void enterEntity_definition(TILGrammarParser::Entity_definitionContext *ctx) = 0;
  virtual void exitEntity_definition(TILGrammarParser::Entity_definitionContext *ctx) = 0;

  virtual void enterConstruction(TILGrammarParser::ConstructionContext *ctx) = 0;
  virtual void exitConstruction(TILGrammarParser::ConstructionContext *ctx) = 0;

  virtual void enterGlobal_variable_definition(TILGrammarParser::Global_variable_definitionContext *ctx) = 0;
  virtual void exitGlobal_variable_definition(TILGrammarParser::Global_variable_definitionContext *ctx) = 0;

  virtual void enterData_type(TILGrammarParser::Data_typeContext *ctx) = 0;
  virtual void exitData_type(TILGrammarParser::Data_typeContext *ctx) = 0;

  virtual void enterEmbeded_type(TILGrammarParser::Embeded_typeContext *ctx) = 0;
  virtual void exitEmbeded_type(TILGrammarParser::Embeded_typeContext *ctx) = 0;

  virtual void enterList_type(TILGrammarParser::List_typeContext *ctx) = 0;
  virtual void exitList_type(TILGrammarParser::List_typeContext *ctx) = 0;

  virtual void enterTouple_type(TILGrammarParser::Touple_typeContext *ctx) = 0;
  virtual void exitTouple_type(TILGrammarParser::Touple_typeContext *ctx) = 0;

  virtual void enterUser_type(TILGrammarParser::User_typeContext *ctx) = 0;
  virtual void exitUser_type(TILGrammarParser::User_typeContext *ctx) = 0;

  virtual void enterEnclosed_data_type(TILGrammarParser::Enclosed_data_typeContext *ctx) = 0;
  virtual void exitEnclosed_data_type(TILGrammarParser::Enclosed_data_typeContext *ctx) = 0;

  virtual void enterVariable(TILGrammarParser::VariableContext *ctx) = 0;
  virtual void exitVariable(TILGrammarParser::VariableContext *ctx) = 0;

  virtual void enterTrivialisation(TILGrammarParser::TrivialisationContext *ctx) = 0;
  virtual void exitTrivialisation(TILGrammarParser::TrivialisationContext *ctx) = 0;

  virtual void enterComposition(TILGrammarParser::CompositionContext *ctx) = 0;
  virtual void exitComposition(TILGrammarParser::CompositionContext *ctx) = 0;

  virtual void enterClosure(TILGrammarParser::ClosureContext *ctx) = 0;
  virtual void exitClosure(TILGrammarParser::ClosureContext *ctx) = 0;

  virtual void enterLambda_variables(TILGrammarParser::Lambda_variablesContext *ctx) = 0;
  virtual void exitLambda_variables(TILGrammarParser::Lambda_variablesContext *ctx) = 0;

  virtual void enterTyped_variables(TILGrammarParser::Typed_variablesContext *ctx) = 0;
  virtual void exitTyped_variables(TILGrammarParser::Typed_variablesContext *ctx) = 0;

  virtual void enterTyped_variable(TILGrammarParser::Typed_variableContext *ctx) = 0;
  virtual void exitTyped_variable(TILGrammarParser::Typed_variableContext *ctx) = 0;

  virtual void enterN_execution(TILGrammarParser::N_executionContext *ctx) = 0;
  virtual void exitN_execution(TILGrammarParser::N_executionContext *ctx) = 0;

  virtual void enterEntity(TILGrammarParser::EntityContext *ctx) = 0;
  virtual void exitEntity(TILGrammarParser::EntityContext *ctx) = 0;

  virtual void enterType_name(TILGrammarParser::Type_nameContext *ctx) = 0;
  virtual void exitType_name(TILGrammarParser::Type_nameContext *ctx) = 0;

  virtual void enterEntity_name(TILGrammarParser::Entity_nameContext *ctx) = 0;
  virtual void exitEntity_name(TILGrammarParser::Entity_nameContext *ctx) = 0;

  virtual void enterVariable_name(TILGrammarParser::Variable_nameContext *ctx) = 0;
  virtual void exitVariable_name(TILGrammarParser::Variable_nameContext *ctx) = 0;

  virtual void enterKeyword(TILGrammarParser::KeywordContext *ctx) = 0;
  virtual void exitKeyword(TILGrammarParser::KeywordContext *ctx) = 0;

  virtual void enterSymbols(TILGrammarParser::SymbolsContext *ctx) = 0;
  virtual void exitSymbols(TILGrammarParser::SymbolsContext *ctx) = 0;

  virtual void enterAnything(TILGrammarParser::AnythingContext *ctx) = 0;
  virtual void exitAnything(TILGrammarParser::AnythingContext *ctx) = 0;

  virtual void enterNumber(TILGrammarParser::NumberContext *ctx) = 0;
  virtual void exitNumber(TILGrammarParser::NumberContext *ctx) = 0;

  virtual void enterUpperletter_name(TILGrammarParser::Upperletter_nameContext *ctx) = 0;
  virtual void exitUpperletter_name(TILGrammarParser::Upperletter_nameContext *ctx) = 0;

  virtual void enterLowerletter_name(TILGrammarParser::Lowerletter_nameContext *ctx) = 0;
  virtual void exitLowerletter_name(TILGrammarParser::Lowerletter_nameContext *ctx) = 0;

  virtual void enterQuoted_name(TILGrammarParser::Quoted_nameContext *ctx) = 0;
  virtual void exitQuoted_name(TILGrammarParser::Quoted_nameContext *ctx) = 0;

  virtual void enterWhitespace(TILGrammarParser::WhitespaceContext *ctx) = 0;
  virtual void exitWhitespace(TILGrammarParser::WhitespaceContext *ctx) = 0;

  virtual void enterOptional_whitespace(TILGrammarParser::Optional_whitespaceContext *ctx) = 0;
  virtual void exitOptional_whitespace(TILGrammarParser::Optional_whitespaceContext *ctx) = 0;


};

