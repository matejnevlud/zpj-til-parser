
#pragma once


#include "antlr4-runtime.h"
#include "TILGrammarParser.h"
#include <vector>
#include <string>
#include <variant>

using namespace std;
using namespace antlr4;
using namespace tree;

/**
 * This class defines an abstract visitor for a parse tree
 * produced by TILGrammarParser.
 */



inline bool ends_with(std::string const & value, std::string const & ending)
{
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

inline std::string trim(const std::string &s)
{
   auto wsfront=std::find_if_not(s.begin(),s.end(),[](int c){return std::isspace(c);});
   auto wsback=std::find_if_not(s.rbegin(),s.rend(),[](int c){return std::isspace(c);}).base();
   return (wsback<=wsfront ? std::string() : std::string(wsfront,wsback));
}

typedef std::variant<int, double, float> arithmetic;

typedef vector<string> Type;
typedef vector<Type> EnclosedType;


struct RecuType {
	vector<variant<RecuType *, string>> dataTypes = {};

	void addEmbedded(string t) {
		dataTypes.push_back(t);
	}

	void addEnclosed(RecuType * r) {
		dataTypes.push_back(r);
	}

	RecuType(){};

	string toString() {
		string ret = "";

		ret += "( ";
		
		for (auto n : dataTypes) {
			if(n.index() == 1) {
				ret += get<string>(n).c_str();
				ret += " ";
			} else {
				ret += get<RecuType *>(n)->toString();
			}
		}

		ret += ") ";

		return ret;

	}

	string serialize() {
		string ret = "";

		ret += "( ";
		
		for (auto n : dataTypes) {
			if(n.index() == 1) {
				ret += get<string>(n).c_str();
				ret += " ";
			} else {
				ret += get<RecuType *>(n)->toString();
			}
		}

		ret += ") ";

		return ret;

	}

	bool empty() {
		return dataTypes.empty();
	}

	variant<RecuType *, string> back() {

		if(dataTypes.back().index() == 0)
			return get<RecuType *>(dataTypes.back())->back();
			//dataTypes = get<RecuType *>(dataTypes.back())->dataTypes;

		return dataTypes.back();
	}

	void pop_back() {
		if(dataTypes.back().index() == 0)
			get<RecuType *>(dataTypes.back())->pop_back();
		else 
			dataTypes.pop_back();

		if(dataTypes.size() > 1)
			if(dataTypes.back().index() == 0)
				if(get<RecuType *>(dataTypes.back())->empty())
					dataTypes.pop_back();

		if(dataTypes.size() == 1)
			if(dataTypes.back().index() == 0)
				dataTypes = get<RecuType *>(dataTypes.back())->dataTypes;
		

	}

	void clean() {
		for (auto type : dataTypes) {

		}
	}



	static RecuType * parseEnclosedType(TILGrammarParser::Enclosed_data_typeContext *context) {

		RecuType * ret = new RecuType();

		for (auto dataType : context->data_type()) {
			
			if(dataType->embeded_type()) {
				ret->addEmbedded(dataType->embeded_type()->getText());
			}

			if(dataType->enclosed_data_type()) {
				ret->addEnclosed(RecuType::parseEnclosedType(dataType->enclosed_data_type()));
			}

			if (ends_with(trim(dataType->getText()), "@tw")) {
				RecuType * time = new RecuType();
				time->addEnclosed(ret);
				time->addEmbedded("Time");

				RecuType * world = new RecuType();
				world->addEnclosed(time);
				world->addEmbedded("World");

				//ret = world;
			}
		}

		

		return ret;
	}

	static RecuType * parseEmbeddedType(TILGrammarParser::Embeded_typeContext *context) {

		RecuType * ret = new RecuType();

		ret->addEmbedded(context->getText());

		return ret;
	}

};


struct TypeV2 {

	vector<Type> arguments;
	Type outType;

	TypeV2(){};

	TypeV2(Type oType) {
		outType = oType;
	}

	Type nextArgument() {
		if(arguments.empty()) exit(10);

		Type ret = arguments.back();
		arguments.pop_back();
		return ret;
	}

};


struct EntityV2 {
	string name;	
	EnclosedType arg;

	RecuType * type = nullptr;

	EntityV2(){};

	EntityV2(string n, EnclosedType a) {
		name = n;
		arg = a;
	}
};

enum Occurence {
	INTENSIONAL,
	EXTENSIONAL,
	HYPER
};


struct ConstructionV2 {
	string id; //1#1#1
	RecuType * dataType; // (Int Int), (((Real, Real) Time) World)
	string constructionType; // closure, construction, composition, trivialization
	Occurence occurence = EXTENSIONAL; // intensional, extensional, hyperintensional
	string print; // Text reproduction as if in TIL-Script

	vector<ConstructionV2 *> children = {};
};

class  TILGrammarVisitorV2 : public antlr4::tree::AbstractParseTreeVisitor {
public:


	vector<EntityV2> definedEntities = {};
	vector<EntityV2> runtimeEntities = {};

	vector<ConstructionV2> runtimeConstructions = {};

	bool wasFirstW, wasFirstT = false;


	int deepLevel = 0;

	string xmlOutput = "";

	string typeToString(Type t) {
		string tmp = "( ";
		for (auto dt : t)
			tmp += dt + " ";
		tmp += ")";
		return tmp;
	}

	string typeToString(EnclosedType t) {
		string tmp = "";
        for (int i = 0; i < t.size(); i++)
            tmp += "( ";

		for (auto dt : t) {
            for (auto ddt : dt) 
                tmp += ddt + " ";
            tmp += ") ";
        }
		
		return tmp;
	}

	string occurenceToString(Occurence o) {
		switch(o) {
			case INTENSIONAL:
				return "Intensional";
			case EXTENSIONAL:
				return "Extensional";
			case HYPER:
				return "Hyperintensional";
			default:
				return "Invalid";
		}
	}

	void indent(int n) {
		printf("%*s", n * 4, "");
	}

	void red () {
	printf("\033[1;31m");
	}

	void green() {
		printf("\033[0;32m");
	}

	void reset() {
		printf("\033[0m");
	}

	void error(string errMsg = "") {
		red();

		printf(errMsg.c_str());

		printf("\n");
		printf("▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
		printf("◀ Analysis ended with ERR ▶\n");
		printf("▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
		printf("\n\n");

		throw 0;
		exit(0);
	}

	void analysisOK() {
		green();


		printf("\n");
		printf("▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
		printf("◀ Analysis ended OK ▶\n");
		printf("▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
		printf("\n\n");

		reset();
	}

	void exportToXML() {

		/* xmlOutput += "<construction constructionType='n_execution' print=\""+context->getText()+"\">\n";

		ret = visitN_execution(context->n_execution());
		
		xmlOutput += "<dataType>" + typeToString(ret) + "</dataType>\n";
		xmlOutput += "</construction>\n"; */

        xmlOutput = "<start>\n" + xmlOutput + "</start>\n";

		ofstream xmlFile;
		xmlFile.open ("out.xml");
		xmlFile << xmlOutput;
		xmlFile.close();
	}
  /**
   * Visit parse trees produced by TILGrammarParser.
   */
    
  	void printIntensExtensSummary() {
		
		vector<ConstructionV2> hyper = {};
		vector<ConstructionV2> intens = {};
		vector<ConstructionV2> extens = {};

		for( auto construction : runtimeConstructions )
			if (construction.occurence == HYPER) hyper.push_back(construction);
			else if (construction.occurence == INTENSIONAL) intens.push_back(construction);
			else if (construction.occurence == EXTENSIONAL) extens.push_back(construction);

		cout << endl << "Rozdělení podle výskytu" << endl << endl;

		cout << "Hyperintensionální:" << endl;
		for( auto c : hyper )
			cout << '\t' << c.print << endl;
		
		cout << "Intensionální:" << endl;
		for( auto i : intens )
			cout << '\t' << i.print << endl;
		
		cout << "Extenzionální:" << endl;
		for( auto e : extens )
			cout << '\t' << e.print << endl;
		
	}


	
	virtual RecuType * visitStart(TILGrammarParser::StartContext *context) {
		//printf("visitStart\n");

		for(auto sentence : context->sentence())
			visitSentence(sentence);


		//printf(xmlOutput.c_str());
		exportToXML();
		printIntensExtensSummary();
		return nullptr;
	}

    virtual RecuType * visitSentence(TILGrammarParser::SentenceContext *context) {
		//printf("visitSentence\n");

		return visitSentence_content(context->sentence_content());
	}

    virtual RecuType * visitSentence_content(TILGrammarParser::Sentence_contentContext *context) {
		//printf("visitSentence_content\n");

		if(context->type_definition())
			return visitType_definition(context->type_definition());

		if(context->global_variable_definition())
			return visitGlobal_variable_definition(context->global_variable_definition());

		if(context->entity_definition())
			return visitEntity_definition(context->entity_definition());

		if(context->construction()) {
			auto retVal = visitConstruction(context->construction());
			printf("\nTyp celé věty -> %s \n", retVal->toString().c_str());
			analysisOK();
			return retVal;
		}


		return nullptr;
	}


    virtual RecuType * visitGlobal_variable_definition(TILGrammarParser::Global_variable_definitionContext *context) {
		//printf("visitGlobal_variable_definition\n");

		EntityV2 variable;
		variable.name = context->variable_name()[0]->getText();
		

		if(context->data_type()->enclosed_data_type())
			variable.type = RecuType::parseEnclosedType(context->data_type()->enclosed_data_type());
		
		if(context->data_type()->embeded_type())
			variable.type = RecuType::parseEmbeddedType(context->data_type()->embeded_type());

		// Check if entity is extensionalized i.e.: @tw
		if (context->data_type()->getText().find("@tw") != std::string::npos) {
            RecuType * time = new RecuType();
			time->addEnclosed(variable.type);
			time->addEmbedded("Time");

			RecuType * world = new RecuType();
			world->addEnclosed(time);
			world->addEmbedded("World");

			variable.type = world;
		}



		//printf("Entity name: %s %s\n", entity.name.c_str(), typeToString(entity.arg).c_str());
		printf("Variable name: %s %s\n", variable.name.c_str(), variable.type->toString().c_str());

		xmlOutput += "<variable name=\""+variable.name+"\" dataType=\""+variable.type->toString()+"\"/>\n";

		definedEntities.push_back(variable);

		return nullptr;
	}

    virtual RecuType * visitType_definition(TILGrammarParser::Type_definitionContext *context) {
		//printf("visitType_definition\n");
		return nullptr;
	}


    virtual RecuType * visitEntity_definition(TILGrammarParser::Entity_definitionContext *context) {
		//printf("visitEntity_definition\n");
		

		EntityV2 entity;
		entity.name = context->entity_name()[0]->getText();
		

		if(context->data_type()->enclosed_data_type())
			entity.type = RecuType::parseEnclosedType(context->data_type()->enclosed_data_type());
		
		if(context->data_type()->embeded_type())
			entity.type = RecuType::parseEmbeddedType(context->data_type()->embeded_type());

		// Check if entity is extensionalized i.e.: @tw
		if (context->data_type()->getText().find("@tw") != std::string::npos) {
            RecuType * time = new RecuType();
			time->addEnclosed(entity.type);
			time->addEmbedded("Time");

			RecuType * world = new RecuType();
			world->addEnclosed(time);
			world->addEmbedded("World");

			entity.type = world;
		}



		//printf("Entity name: %s %s\n", entity.name.c_str(), typeToString(entity.arg).c_str());
		printf("Entity name: %s %s\n", entity.name.c_str(), entity.type->toString().c_str());

		xmlOutput += "<entity name=\""+entity.name+"\" dataType=\""+entity.type->toString()+"\"/>\n";

		definedEntities.push_back(entity);

		return nullptr;
	}

    virtual RecuType * visitData_type(TILGrammarParser::Data_typeContext *context) {
		//printf("visitData_type\n");
		return nullptr;
	}

    virtual RecuType * visitEmbeded_type(TILGrammarParser::Embeded_typeContext *context) {
		//printf("visitEmbeded_type\n");
		return nullptr;
	}

    virtual RecuType * visitList_type(TILGrammarParser::List_typeContext *context) {
		//printf("visitList_type\n");
		return nullptr;
	}

    virtual RecuType * visitTouple_type(TILGrammarParser::Touple_typeContext *context) {
		//printf("visitTouple_type\n");
		return nullptr;
	}

    virtual RecuType * visitUser_type(TILGrammarParser::User_typeContext *context) {
		//printf("visitUser_type\n");
		return nullptr;
	}

    virtual RecuType * visitEnclosed_data_type(TILGrammarParser::Enclosed_data_typeContext *context) {
		//printf("visitEnclosed_data_type\n");
		return nullptr;
	}




    virtual RecuType * visitConstruction(TILGrammarParser::ConstructionContext *context, Occurence prev_occurance = EXTENSIONAL, bool is_closure = false) {
		//printf("visitConstruction\n");
		RecuType * ret = {};
		bool hasShortWT = context->getText().find("@wt") != std::string::npos;

		ConstructionV2 construction;

		construction.print = context->getText();
		construction.occurence = prev_occurance;

		
		if(context->trivialisation()) {
			xmlOutput += "<construction constructionType='trivialisation' print=\""+context->getText()+"\" occurence=\""+occurenceToString(prev_occurance)+"\">\n";

			ret = visitTrivialisation(context->trivialisation(), hasShortWT, prev_occurance, is_closure);
			
			xmlOutput += "<dataType>" + ret->toString() + "</dataType>\n";
			xmlOutput += "</construction>\n";
			
			construction.constructionType = "trivialisation";
			construction.dataType = ret;
		}

		if(context->variable()) {
			xmlOutput += "<construction constructionType='variable' print=\""+context->getText()+"\" occurence=\""+occurenceToString(prev_occurance)+"\">\n";

			ret = visitVariable(context->variable(), prev_occurance, is_closure);
			
			xmlOutput += "<dataType>" + ret->toString() + "</dataType>\n";
			xmlOutput += "</construction>\n";
			
			construction.constructionType = "variable";
			construction.dataType = ret;
		}

		if(context->closure()) {
			xmlOutput += "<construction constructionType='closure' print=\""+context->getText()+"\" occurence=\""+occurenceToString(prev_occurance)+"\">\n";

			ret = visitClosure(context->closure(), prev_occurance, is_closure);
			
			xmlOutput += "<dataType>" + ret->toString() + "</dataType>\n";
			xmlOutput += "</construction>\n";
			
			construction.constructionType = "closure";
			construction.dataType = ret;
		}

		if(context->n_execution()) {
			xmlOutput += "<construction constructionType='n_execution' print=\""+context->getText()+"\" occurence=\""+occurenceToString(prev_occurance)+"\">\n";

			ret = visitN_execution(context->n_execution(), prev_occurance, is_closure);
			
			xmlOutput += "<dataType>" + ret->toString() + "</dataType>\n";
			xmlOutput += "</construction>\n";
			
			construction.constructionType = "n_execution";
			construction.dataType = ret;
		}

		if(context->composition()) {
			xmlOutput += "<construction constructionType='composition' print=\""+context->getText()+"\" occurence=\""+occurenceToString(prev_occurance)+"\">\n";

			ret = visitComposition(context->composition(), prev_occurance, is_closure);
			
			xmlOutput += "<dataType>" + ret->toString() + "</dataType>\n";
			xmlOutput += "</construction>\n";
			
			construction.constructionType = "composition";
			construction.dataType = ret;
		}

		cout << context->getText() << " -> " << ret->toString() << endl;
		runtimeConstructions.push_back(construction);

		return ret;
	}




    virtual RecuType * visitVariable(TILGrammarParser::VariableContext *context, Occurence prev_occurance = EXTENSIONAL, bool is_closure = false) {
		//printf("visitVariable\n");

		EntityV2 retVariable;

		for(auto ent: definedEntities)
			if(ent.name == context->getText()) {
				runtimeEntities.push_back(ent);
				RecuType * duplic = new RecuType();
				duplic->dataTypes = ent.type->dataTypes;
				return duplic;
			}

		string error_str = "Not defined variable ";
		error_str += context->getText();
		error(error_str);

		return nullptr;
	}

    virtual RecuType * visitTrivialisation(TILGrammarParser::TrivialisationContext *context, bool hasShortWT = false, Occurence prev_occurance = EXTENSIONAL, bool is_closure = false) {
		//printf("visitTrivialisation\n");
		RecuType * ret = new RecuType();
		ret->addEmbedded("*");

		
		if(context->construction()){
			visitConstruction(context->construction(), HYPER);
		}
			

		if(context->entity())
			ret = visitEntity(context->entity(), hasShortWT);

		return ret;
	}

    virtual RecuType * visitComposition(TILGrammarParser::CompositionContext *context, Occurence prev_occurance = EXTENSIONAL, bool is_closure = false) {
		//printf("visitComposition\n");

		auto constructions = context->construction();
		auto firstChild = constructions[0];

		vector<TILGrammarParser::ConstructionContext*> otherChildren = {};
		for (int i = 1; i < constructions.size(); i++)
			otherChildren.push_back(constructions[i]);


		// Funkce
		RecuType * functionType = visitConstruction(firstChild, (prev_occurance == HYPER) ? HYPER : ((is_closure) ? INTENSIONAL : EXTENSIONAL), is_closure);

		

		// Argumenty
		vector<RecuType *> argumentsType = {};
		for(auto child : otherChildren)
			argumentsType.push_back(visitConstruction(child, (prev_occurance == HYPER) ? HYPER : INTENSIONAL, is_closure));


		// Evaluate function with arguments
		//cout << "Composition: " << context->getText() << endl;
		//cout << "\tFunction: " << typeToString(functionType) << endl;

		//Do Type Analysis, match argument return type with argument 
		while(!argumentsType.empty()) {
			RecuType * argRet = argumentsType.back();
			
			while(!argRet->empty()) {
				argRet->back() == functionType->back();

				//cout << "  " << get<string>(argRet->back()) << " == " << get<string>(functionType->back()) << endl;
				
				if (argRet->back() != functionType->back())
					error("Not matching types");

				argRet->pop_back();
				functionType->pop_back();
			}
			argumentsType.pop_back();
		}

		//if(functionType.size() != 1) error("Not matching arguments count");
		//cout << "  Out: " << functionType->toString() << endl;


		return functionType;
	}




    virtual RecuType * visitClosure(TILGrammarParser::ClosureContext *context, Occurence prev_occurance = EXTENSIONAL, bool is_closure = false) {
		//printf("visitClosure\n");
		RecuType * lambdaRet = nullptr;
		RecuType * constrRet = nullptr;

		
		string lambdaName = "";

		if(context->lambda_variables())
			lambdaRet = visitLambda_variables(context->lambda_variables(), lambdaName);

		// Determine construction 
		Occurence next_occurance = (prev_occurance == HYPER) ? HYPER : INTENSIONAL;
		next_occurance = (lambdaName == "t" || lambdaName == "w") ? EXTENSIONAL : next_occurance;

		is_closure = (lambdaName != "t" && lambdaName != "w");

		if(context->construction())
			constrRet = visitConstruction(context->construction(), next_occurance, is_closure);

		RecuType * retCombined = new RecuType();
		retCombined->addEnclosed(constrRet);
		retCombined->addEmbedded(get<string>(lambdaRet->dataTypes.back()));

		cout << "Lambda: " << lambdaRet->toString() << endl;

		return retCombined;
	}



    virtual RecuType * visitLambda_variables(TILGrammarParser::Lambda_variablesContext *context, string &lambdaName) {
		//printf("visitLambda_variables\n");
		EntityV2 lastVar;
		for(auto variable : context->typed_variables()->typed_variable()) {
			EntityV2 var;
			var.name = variable->lowerletter_name()->getText();

			lambdaName = var.name;
			if(var.name == "w") wasFirstW = true;
			if(var.name == "t") wasFirstT = true;

			if(variable->data_type()->enclosed_data_type())
				var.type = RecuType::parseEnclosedType(variable->data_type()->enclosed_data_type());
			
			if(variable->data_type()->embeded_type())
				var.type = RecuType::parseEmbeddedType(variable->data_type()->embeded_type());
			
			definedEntities.push_back(var);
			lastVar = var;


			RecuType * duplic = new RecuType();
			duplic->dataTypes = var.type->dataTypes;
			return duplic;

			xmlOutput += "<visitvariable name=\""+var.name+"\" dataType=\""+var.type->toString()+"\"/>\n";
		}
		return lastVar.type;
	}

    virtual RecuType * visitTyped_variables(TILGrammarParser::Typed_variablesContext *context) {
		//printf("visitTyped_variables\n");
		return nullptr;
	}

    virtual RecuType * visitTyped_variable(TILGrammarParser::Typed_variableContext *context) {
		//printf("visitTyped_variable\n");
		return nullptr;
	}

    virtual RecuType * visitN_execution(TILGrammarParser::N_executionContext *context, Occurence prev_occurance = EXTENSIONAL, bool is_closure = false) {
		//printf("visitN_execution\n");
		return nullptr;
	}

    virtual RecuType * visitEntity(TILGrammarParser::EntityContext *context, bool hasShortWT = false) {
		//printf("visitEntity\n");
		//cout << context->getText() << endl;


		// If has shortend @t w 
		if (hasShortWT) {
			EntityV2 entity;
			
			for(auto ent: definedEntities)
				if(ent.name == context->getText()) {
					runtimeEntities.push_back(ent);
					entity = ent;
				}

			
			

			return entity.type;
		}
		

		for(auto ent: definedEntities)
			if(ent.name == context->getText()) {
				runtimeEntities.push_back(ent);
				RecuType * duplic = new RecuType();
				duplic->dataTypes = ent.type->dataTypes;
				return duplic;
			}
		
		error("Not defined entity");

		return nullptr;

	}

    virtual RecuType * visitType_name(TILGrammarParser::Type_nameContext *context) {
		//printf("visitType_name\n");
		return nullptr;
	}

    virtual RecuType * visitEntity_name(TILGrammarParser::Entity_nameContext *context) {
		//printf("visitEntity_name\n");
		return nullptr;
	}

    virtual RecuType * visitVariable_name(TILGrammarParser::Variable_nameContext *context) {
		//printf("visitVariable_name\n");
		return nullptr;
	}

    virtual RecuType * visitKeyword(TILGrammarParser::KeywordContext *context) {
		//printf("visitKeyword\n");
		return nullptr;
	}

    virtual RecuType * visitSymbols(TILGrammarParser::SymbolsContext *context) {
		//printf("visitSymbols\n");
		return nullptr;
	}

    virtual RecuType * visitAnything(TILGrammarParser::AnythingContext *context) {
		//printf("visitAnything\n");
		return nullptr;
	}

    virtual RecuType * visitNumber(TILGrammarParser::NumberContext *context) {
		//printf("visitNumber\n");
		return nullptr;
	}

    virtual RecuType * visitUpperletter_name(TILGrammarParser::Upperletter_nameContext *context) {
		//printf("visitUpperletter_name\n");
		return nullptr;
	}

    virtual RecuType * visitLowerletter_name(TILGrammarParser::Lowerletter_nameContext *context) {
		//printf("visitLowerletter_name\n");
		return nullptr;
	}

    virtual RecuType * visitQuoted_name(TILGrammarParser::Quoted_nameContext *context) {
		//printf("visitQuoted_name\n");
		return nullptr;
	}

    virtual RecuType * visitWhitespace(TILGrammarParser::WhitespaceContext *context) {
		//printf("visitWhitespace\n");
		return nullptr;
	}

    virtual RecuType * visitOptional_whitespace(TILGrammarParser::Optional_whitespaceContext *context) {
		//printf("visitOptional_whitespace\n");
		return nullptr;
	}

    virtual RecuType * visitTermination(TILGrammarParser::TerminationContext *context) {
		//printf("visitTermination\n");
		return nullptr;
	}



};


