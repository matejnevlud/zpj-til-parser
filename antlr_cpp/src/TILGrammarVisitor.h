// Generated from TILGrammar.g4 by ANTLR 4.9.2

#pragma once


#include "antlr4-runtime.h"
#include "TILGrammarParser.h"
#include <vector>
#include <string>

using namespace std;
using namespace antlr4;
using namespace tree;


class  TILGrammarVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by TILGrammarParser.
   */
    virtual antlrcpp::Any visitStart(TILGrammarParser::StartContext *context) = 0;

    virtual antlrcpp::Any visitSentence(TILGrammarParser::SentenceContext *context) = 0;

    virtual antlrcpp::Any visitSentence_content(TILGrammarParser::Sentence_contentContext *context) = 0;

    virtual antlrcpp::Any visitTermination(TILGrammarParser::TerminationContext *context) = 0;

    virtual antlrcpp::Any visitType_definition(TILGrammarParser::Type_definitionContext *context) = 0;

    virtual antlrcpp::Any visitEntity_definition(TILGrammarParser::Entity_definitionContext *context) = 0;

    virtual antlrcpp::Any visitConstruction(TILGrammarParser::ConstructionContext *context) = 0;

    virtual antlrcpp::Any visitGlobal_variable_definition(TILGrammarParser::Global_variable_definitionContext *context) = 0;

    virtual antlrcpp::Any visitData_type(TILGrammarParser::Data_typeContext *context) = 0;

    virtual antlrcpp::Any visitEmbeded_type(TILGrammarParser::Embeded_typeContext *context) = 0;

    virtual antlrcpp::Any visitList_type(TILGrammarParser::List_typeContext *context) = 0;

    virtual antlrcpp::Any visitTouple_type(TILGrammarParser::Touple_typeContext *context) = 0;

    virtual antlrcpp::Any visitUser_type(TILGrammarParser::User_typeContext *context) = 0;

    virtual antlrcpp::Any visitEnclosed_data_type(TILGrammarParser::Enclosed_data_typeContext *context) = 0;

    virtual antlrcpp::Any visitVariable(TILGrammarParser::VariableContext *context) = 0;

    virtual antlrcpp::Any visitTrivialisation(TILGrammarParser::TrivialisationContext *context) = 0;

    virtual antlrcpp::Any visitComposition(TILGrammarParser::CompositionContext *context) = 0;

    virtual antlrcpp::Any visitClosure(TILGrammarParser::ClosureContext *context) = 0;

    virtual antlrcpp::Any visitLambda_variables(TILGrammarParser::Lambda_variablesContext *context) = 0;

    virtual antlrcpp::Any visitTyped_variables(TILGrammarParser::Typed_variablesContext *context) = 0;

    virtual antlrcpp::Any visitTyped_variable(TILGrammarParser::Typed_variableContext *context) = 0;

    virtual antlrcpp::Any visitN_execution(TILGrammarParser::N_executionContext *context) = 0;

    virtual antlrcpp::Any visitEntity(TILGrammarParser::EntityContext *context) = 0;

    virtual antlrcpp::Any visitType_name(TILGrammarParser::Type_nameContext *context) = 0;

    virtual antlrcpp::Any visitEntity_name(TILGrammarParser::Entity_nameContext *context) = 0;

    virtual antlrcpp::Any visitVariable_name(TILGrammarParser::Variable_nameContext *context) = 0;

    virtual antlrcpp::Any visitKeyword(TILGrammarParser::KeywordContext *context) = 0;

    virtual antlrcpp::Any visitSymbols(TILGrammarParser::SymbolsContext *context) = 0;

    virtual antlrcpp::Any visitAnything(TILGrammarParser::AnythingContext *context) = 0;

    virtual antlrcpp::Any visitNumber(TILGrammarParser::NumberContext *context) = 0;

    virtual antlrcpp::Any visitUpperletter_name(TILGrammarParser::Upperletter_nameContext *context) = 0;

    virtual antlrcpp::Any visitLowerletter_name(TILGrammarParser::Lowerletter_nameContext *context) = 0;

    virtual antlrcpp::Any visitQuoted_name(TILGrammarParser::Quoted_nameContext *context) = 0;

    virtual antlrcpp::Any visitWhitespace(TILGrammarParser::WhitespaceContext *context) = 0;

    virtual antlrcpp::Any visitOptional_whitespace(TILGrammarParser::Optional_whitespaceContext *context) = 0;


};

