
// Generated from TILGrammar.g4 by ANTLR 4.9.2

#pragma once


#include "antlr4-runtime.h"
#include "TILGrammarVisitor.h"


/**
 * This class provides an empty implementation of TILGrammarVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  TILGrammarBaseVisitor : public TILGrammarVisitor {
public:

  virtual antlrcpp::Any visitStart(TILGrammarParser::StartContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSentence(TILGrammarParser::SentenceContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSentence_content(TILGrammarParser::Sentence_contentContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTermination(TILGrammarParser::TerminationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitType_definition(TILGrammarParser::Type_definitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitEntity_definition(TILGrammarParser::Entity_definitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitConstruction(TILGrammarParser::ConstructionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitGlobal_variable_definition(TILGrammarParser::Global_variable_definitionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitData_type(TILGrammarParser::Data_typeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitEmbeded_type(TILGrammarParser::Embeded_typeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitList_type(TILGrammarParser::List_typeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTouple_type(TILGrammarParser::Touple_typeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitUser_type(TILGrammarParser::User_typeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitEnclosed_data_type(TILGrammarParser::Enclosed_data_typeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVariable(TILGrammarParser::VariableContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTrivialisation(TILGrammarParser::TrivialisationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitComposition(TILGrammarParser::CompositionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitClosure(TILGrammarParser::ClosureContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLambda_variables(TILGrammarParser::Lambda_variablesContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTyped_variables(TILGrammarParser::Typed_variablesContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTyped_variable(TILGrammarParser::Typed_variableContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitN_execution(TILGrammarParser::N_executionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitEntity(TILGrammarParser::EntityContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitType_name(TILGrammarParser::Type_nameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitEntity_name(TILGrammarParser::Entity_nameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitVariable_name(TILGrammarParser::Variable_nameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitKeyword(TILGrammarParser::KeywordContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSymbols(TILGrammarParser::SymbolsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAnything(TILGrammarParser::AnythingContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNumber(TILGrammarParser::NumberContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitUpperletter_name(TILGrammarParser::Upperletter_nameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLowerletter_name(TILGrammarParser::Lowerletter_nameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitQuoted_name(TILGrammarParser::Quoted_nameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitWhitespace(TILGrammarParser::WhitespaceContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitOptional_whitespace(TILGrammarParser::Optional_whitespaceContext *ctx) override {
    return visitChildren(ctx);
  }


};

