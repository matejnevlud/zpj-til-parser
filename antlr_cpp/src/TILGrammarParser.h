
// Generated from TILGrammar.g4 by ANTLR 4.9.2

#pragma once


#include "antlr4-runtime.h"




class  TILGrammarParser : public antlr4::Parser {
public:
  enum {
    T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, 
    T__7 = 8, T__8 = 9, T__9 = 10, T__10 = 11, T__11 = 12, T__12 = 13, T__13 = 14, 
    T__14 = 15, T__15 = 16, T__16 = 17, T__17 = 18, T__18 = 19, T__19 = 20, 
    T__20 = 21, T__21 = 22, T__22 = 23, T__23 = 24, T__24 = 25, T__25 = 26, 
    T__26 = 27, T__27 = 28, T__28 = 29, T__29 = 30, T__30 = 31, T__31 = 32, 
    T__32 = 33, T__33 = 34, T__34 = 35, T__35 = 36, T__36 = 37, T__37 = 38, 
    T__38 = 39, T__39 = 40, T__40 = 41, T__41 = 42, T__42 = 43, T__43 = 44, 
    T__44 = 45, T__45 = 46, T__46 = 47, T__47 = 48, T__48 = 49, T__49 = 50, 
    T__50 = 51, T__51 = 52, T__52 = 53, LOWERCASE_LETTER = 54, UPPERCASE_LETTER = 55, 
    ZERO = 56, NONZERO_DIGIT = 57, WHITESPACE_CHARACTER = 58
  };

  enum {
    RuleStart = 0, RuleSentence = 1, RuleSentence_content = 2, RuleTermination = 3, 
    RuleType_definition = 4, RuleEntity_definition = 5, RuleConstruction = 6, 
    RuleGlobal_variable_definition = 7, RuleData_type = 8, RuleEmbeded_type = 9, 
    RuleList_type = 10, RuleTouple_type = 11, RuleUser_type = 12, RuleEnclosed_data_type = 13, 
    RuleVariable = 14, RuleTrivialisation = 15, RuleComposition = 16, RuleClosure = 17, 
    RuleLambda_variables = 18, RuleTyped_variables = 19, RuleTyped_variable = 20, 
    RuleN_execution = 21, RuleEntity = 22, RuleType_name = 23, RuleEntity_name = 24, 
    RuleVariable_name = 25, RuleKeyword = 26, RuleSymbols = 27, RuleAnything = 28, 
    RuleNumber = 29, RuleUpperletter_name = 30, RuleLowerletter_name = 31, 
    RuleQuoted_name = 32, RuleWhitespace = 33, RuleOptional_whitespace = 34
  };

  explicit TILGrammarParser(antlr4::TokenStream *input);
  ~TILGrammarParser();

  virtual std::string getGrammarFileName() const override;
  virtual const antlr4::atn::ATN& getATN() const override { return _atn; };
  virtual const std::vector<std::string>& getTokenNames() const override { return _tokenNames; }; // deprecated: use vocabulary instead.
  virtual const std::vector<std::string>& getRuleNames() const override;
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;


  class StartContext;
  class SentenceContext;
  class Sentence_contentContext;
  class TerminationContext;
  class Type_definitionContext;
  class Entity_definitionContext;
  class ConstructionContext;
  class Global_variable_definitionContext;
  class Data_typeContext;
  class Embeded_typeContext;
  class List_typeContext;
  class Touple_typeContext;
  class User_typeContext;
  class Enclosed_data_typeContext;
  class VariableContext;
  class TrivialisationContext;
  class CompositionContext;
  class ClosureContext;
  class Lambda_variablesContext;
  class Typed_variablesContext;
  class Typed_variableContext;
  class N_executionContext;
  class EntityContext;
  class Type_nameContext;
  class Entity_nameContext;
  class Variable_nameContext;
  class KeywordContext;
  class SymbolsContext;
  class AnythingContext;
  class NumberContext;
  class Upperletter_nameContext;
  class Lowerletter_nameContext;
  class Quoted_nameContext;
  class WhitespaceContext;
  class Optional_whitespaceContext; 

  class  StartContext : public antlr4::ParserRuleContext {
  public:
    StartContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<SentenceContext *> sentence();
    SentenceContext* sentence(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  StartContext* start();

  class  SentenceContext : public antlr4::ParserRuleContext {
  public:
    SentenceContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    Sentence_contentContext *sentence_content();
    TerminationContext *termination();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  SentenceContext* sentence();

  class  Sentence_contentContext : public antlr4::ParserRuleContext {
  public:
    Sentence_contentContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    Type_definitionContext *type_definition();
    Entity_definitionContext *entity_definition();
    ConstructionContext *construction();
    Global_variable_definitionContext *global_variable_definition();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Sentence_contentContext* sentence_content();

  class  TerminationContext : public antlr4::ParserRuleContext {
  public:
    TerminationContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<Optional_whitespaceContext *> optional_whitespace();
    Optional_whitespaceContext* optional_whitespace(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  TerminationContext* termination();

  class  Type_definitionContext : public antlr4::ParserRuleContext {
  public:
    Type_definitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    WhitespaceContext *whitespace();
    Type_nameContext *type_name();
    std::vector<Optional_whitespaceContext *> optional_whitespace();
    Optional_whitespaceContext* optional_whitespace(size_t i);
    Data_typeContext *data_type();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Type_definitionContext* type_definition();

  class  Entity_definitionContext : public antlr4::ParserRuleContext {
  public:
    Entity_definitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<Entity_nameContext *> entity_name();
    Entity_nameContext* entity_name(size_t i);
    std::vector<Optional_whitespaceContext *> optional_whitespace();
    Optional_whitespaceContext* optional_whitespace(size_t i);
    Data_typeContext *data_type();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Entity_definitionContext* entity_definition();

  class  ConstructionContext : public antlr4::ParserRuleContext {
  public:
    ConstructionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    TrivialisationContext *trivialisation();
    VariableContext *variable();
    ClosureContext *closure();
    N_executionContext *n_execution();
    CompositionContext *composition();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  ConstructionContext* construction();

  class  Global_variable_definitionContext : public antlr4::ParserRuleContext {
  public:
    Global_variable_definitionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<Variable_nameContext *> variable_name();
    Variable_nameContext* variable_name(size_t i);
    std::vector<Optional_whitespaceContext *> optional_whitespace();
    Optional_whitespaceContext* optional_whitespace(size_t i);
    Data_typeContext *data_type();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Global_variable_definitionContext* global_variable_definition();

  class  Data_typeContext : public antlr4::ParserRuleContext {
  public:
    Data_typeContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    Embeded_typeContext *embeded_type();
    List_typeContext *list_type();
    Touple_typeContext *touple_type();
    User_typeContext *user_type();
    Enclosed_data_typeContext *enclosed_data_type();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Data_typeContext* data_type();

  class  Embeded_typeContext : public antlr4::ParserRuleContext {
  public:
    Embeded_typeContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    AnythingContext *anything();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Embeded_typeContext* embeded_type();

  class  List_typeContext : public antlr4::ParserRuleContext {
  public:
    List_typeContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<Optional_whitespaceContext *> optional_whitespace();
    Optional_whitespaceContext* optional_whitespace(size_t i);
    Data_typeContext *data_type();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  List_typeContext* list_type();

  class  Touple_typeContext : public antlr4::ParserRuleContext {
  public:
    Touple_typeContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<Optional_whitespaceContext *> optional_whitespace();
    Optional_whitespaceContext* optional_whitespace(size_t i);
    Data_typeContext *data_type();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Touple_typeContext* touple_type();

  class  User_typeContext : public antlr4::ParserRuleContext {
  public:
    User_typeContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    Type_nameContext *type_name();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  User_typeContext* user_type();

  class  Enclosed_data_typeContext : public antlr4::ParserRuleContext {
  public:
    Enclosed_data_typeContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<Optional_whitespaceContext *> optional_whitespace();
    Optional_whitespaceContext* optional_whitespace(size_t i);
    std::vector<Data_typeContext *> data_type();
    Data_typeContext* data_type(size_t i);
    std::vector<WhitespaceContext *> whitespace();
    WhitespaceContext* whitespace(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Enclosed_data_typeContext* enclosed_data_type();

  class  VariableContext : public antlr4::ParserRuleContext {
  public:
    VariableContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    Variable_nameContext *variable_name();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  VariableContext* variable();

  class  TrivialisationContext : public antlr4::ParserRuleContext {
  public:
    TrivialisationContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    Optional_whitespaceContext *optional_whitespace();
    ConstructionContext *construction();
    EntityContext *entity();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  TrivialisationContext* trivialisation();

  class  CompositionContext : public antlr4::ParserRuleContext {
  public:
    CompositionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<Optional_whitespaceContext *> optional_whitespace();
    Optional_whitespaceContext* optional_whitespace(size_t i);
    std::vector<ConstructionContext *> construction();
    ConstructionContext* construction(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  CompositionContext* composition();

  class  ClosureContext : public antlr4::ParserRuleContext {
  public:
    ClosureContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<Optional_whitespaceContext *> optional_whitespace();
    Optional_whitespaceContext* optional_whitespace(size_t i);
    Lambda_variablesContext *lambda_variables();
    ConstructionContext *construction();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  ClosureContext* closure();

  class  Lambda_variablesContext : public antlr4::ParserRuleContext {
  public:
    Lambda_variablesContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    Optional_whitespaceContext *optional_whitespace();
    Typed_variablesContext *typed_variables();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Lambda_variablesContext* lambda_variables();

  class  Typed_variablesContext : public antlr4::ParserRuleContext {
  public:
    Typed_variablesContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<Typed_variableContext *> typed_variable();
    Typed_variableContext* typed_variable(size_t i);
    std::vector<Optional_whitespaceContext *> optional_whitespace();
    Optional_whitespaceContext* optional_whitespace(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Typed_variablesContext* typed_variables();

  class  Typed_variableContext : public antlr4::ParserRuleContext {
  public:
    Typed_variableContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    Lowerletter_nameContext *lowerletter_name();
    std::vector<Optional_whitespaceContext *> optional_whitespace();
    Optional_whitespaceContext* optional_whitespace(size_t i);
    Data_typeContext *data_type();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Typed_variableContext* typed_variable();

  class  N_executionContext : public antlr4::ParserRuleContext {
  public:
    N_executionContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<Optional_whitespaceContext *> optional_whitespace();
    Optional_whitespaceContext* optional_whitespace(size_t i);
    antlr4::tree::TerminalNode *NONZERO_DIGIT();
    ConstructionContext *construction();
    EntityContext *entity();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  N_executionContext* n_execution();

  class  EntityContext : public antlr4::ParserRuleContext {
  public:
    EntityContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    KeywordContext *keyword();
    Entity_nameContext *entity_name();
    NumberContext *number();
    SymbolsContext *symbols();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  EntityContext* entity();

  class  Type_nameContext : public antlr4::ParserRuleContext {
  public:
    Type_nameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    Upperletter_nameContext *upperletter_name();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Type_nameContext* type_name();

  class  Entity_nameContext : public antlr4::ParserRuleContext {
  public:
    Entity_nameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    Upperletter_nameContext *upperletter_name();
    Quoted_nameContext *quoted_name();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Entity_nameContext* entity_name();

  class  Variable_nameContext : public antlr4::ParserRuleContext {
  public:
    Variable_nameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    Lowerletter_nameContext *lowerletter_name();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Variable_nameContext* variable_name();

  class  KeywordContext : public antlr4::ParserRuleContext {
  public:
    KeywordContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  KeywordContext* keyword();

  class  SymbolsContext : public antlr4::ParserRuleContext {
  public:
    SymbolsContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  SymbolsContext* symbols();

  class  AnythingContext : public antlr4::ParserRuleContext {
  public:
    AnythingContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<antlr4::tree::TerminalNode *> ZERO();
    antlr4::tree::TerminalNode* ZERO(size_t i);
    std::vector<antlr4::tree::TerminalNode *> NONZERO_DIGIT();
    antlr4::tree::TerminalNode* NONZERO_DIGIT(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  AnythingContext* anything();

  class  NumberContext : public antlr4::ParserRuleContext {
  public:
    NumberContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<antlr4::tree::TerminalNode *> ZERO();
    antlr4::tree::TerminalNode* ZERO(size_t i);
    std::vector<antlr4::tree::TerminalNode *> NONZERO_DIGIT();
    antlr4::tree::TerminalNode* NONZERO_DIGIT(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  NumberContext* number();

  class  Upperletter_nameContext : public antlr4::ParserRuleContext {
  public:
    Upperletter_nameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<antlr4::tree::TerminalNode *> UPPERCASE_LETTER();
    antlr4::tree::TerminalNode* UPPERCASE_LETTER(size_t i);
    std::vector<antlr4::tree::TerminalNode *> LOWERCASE_LETTER();
    antlr4::tree::TerminalNode* LOWERCASE_LETTER(size_t i);
    std::vector<antlr4::tree::TerminalNode *> ZERO();
    antlr4::tree::TerminalNode* ZERO(size_t i);
    std::vector<antlr4::tree::TerminalNode *> NONZERO_DIGIT();
    antlr4::tree::TerminalNode* NONZERO_DIGIT(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Upperletter_nameContext* upperletter_name();

  class  Lowerletter_nameContext : public antlr4::ParserRuleContext {
  public:
    Lowerletter_nameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<antlr4::tree::TerminalNode *> LOWERCASE_LETTER();
    antlr4::tree::TerminalNode* LOWERCASE_LETTER(size_t i);
    std::vector<antlr4::tree::TerminalNode *> UPPERCASE_LETTER();
    antlr4::tree::TerminalNode* UPPERCASE_LETTER(size_t i);
    std::vector<antlr4::tree::TerminalNode *> ZERO();
    antlr4::tree::TerminalNode* ZERO(size_t i);
    std::vector<antlr4::tree::TerminalNode *> NONZERO_DIGIT();
    antlr4::tree::TerminalNode* NONZERO_DIGIT(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Lowerletter_nameContext* lowerletter_name();

  class  Quoted_nameContext : public antlr4::ParserRuleContext {
  public:
    Quoted_nameContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Quoted_nameContext* quoted_name();

  class  WhitespaceContext : public antlr4::ParserRuleContext {
  public:
    WhitespaceContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    antlr4::tree::TerminalNode *WHITESPACE_CHARACTER();
    Optional_whitespaceContext *optional_whitespace();

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  WhitespaceContext* whitespace();

  class  Optional_whitespaceContext : public antlr4::ParserRuleContext {
  public:
    Optional_whitespaceContext(antlr4::ParserRuleContext *parent, size_t invokingState);
    virtual size_t getRuleIndex() const override;
    std::vector<antlr4::tree::TerminalNode *> WHITESPACE_CHARACTER();
    antlr4::tree::TerminalNode* WHITESPACE_CHARACTER(size_t i);

    virtual void enterRule(antlr4::tree::ParseTreeListener *listener) override;
    virtual void exitRule(antlr4::tree::ParseTreeListener *listener) override;
   
  };

  Optional_whitespaceContext* optional_whitespace();


private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

