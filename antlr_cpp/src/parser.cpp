#include <iostream>
#include <strstream>
#include <string>
#include "antlr4-runtime.h"
#include "TILGrammarLexer.h"
#include "TILGrammarParser.h"
#include "TILGrammarBaseListener.h"
#include "TILGrammarAnalysis.h"
#include <pugixml.hpp>

using namespace antlr4;
using namespace tree;
using namespace pugi;




class TILGrammarErrorListener: public BaseErrorListener {
	virtual void syntaxError(
		Recognizer *recognizer,
		Token *offendingSymbol,
		size_t line,
		size_t charPositionInLine,
		const std::string &msg,
		std::exception_ptr e) override {
	std::ostrstream s;
	s << "Line(" << line << ":" << charPositionInLine << ") Error(" << msg << ")";
	throw std::invalid_argument(s.str());
	}
};

class TILGrammarWalkerListener: public ParseTreeListener {
	virtual void enterEveryRule (ParserRuleContext *ctx) override {
		printf("enterEveryRule\n");
		//printf(ctx->getText().c_str());
		//printf(ctx->getToken()->getSymbol()->getText().c_str());

		
		printf("%s\n", ctx->children[0]->getText().c_str());
		printf("\n");

		
	}
	
	virtual void exitEveryRule (ParserRuleContext *ctx) override {
		printf("exitEveryRule\n");
	}
	
	virtual void visitTerminal (TerminalNode *node) override {
		printf("visitTerminal\n");
		//printf(node->getText().c_str());
		printf("\n");
	}

	virtual void visitErrorNode (ErrorNode *node) override {
		printf("visitErrorNode\n");
	}
};



ParseTree * traverseTree(ParseTree * node) {
	for (auto child : node->children) {
		std::cout << child->getText() << std::endl;
		traverseTree(child);
	}
}


void indent(int n) {
	printf("%*s", n * 4, "");
}

void red () {
  printf("\033[1;31m");
}

void green() {
	printf("\033[0;32m");
}

void reset() {
	printf("\033[0m");
}

void error(string errMsg = "") {
	red();

	printf(errMsg.c_str());

	printf("\n");
	printf("▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
	printf("◀ Analysis ended with ERR ▶\n");
	printf("▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
	printf("\n\n");

	throw 0;
	exit(0);
}



string toGreek(string str) {
	if(str == "Bool") return "ο";
	if(str == "Indiv") return "ι";
	if(str == "Time") return "τ";
	if(str == "String") return "ι";
	if(str == "World") return "ω";
	if(str == "Real") return "τ";
	if(str == "Int" ) return "τ";
	if(str == "*") return "*";
}


struct Entity {
	string name;
	string out;
	vector<string> arg;
};

struct Composition {
	Entity func;
	vector<Entity> arg;
};


class TILTypeAnalysis {
	public:
		xml_document doc;

		vector<Entity *> entities;
		vector<Entity> variables;

		int deepLevel = 0;
		

	TILTypeAnalysis(){};

	void loadString(string XMLString) {
		xml_parse_result result = doc.load_string(XMLString.c_str());
	}

	void loadFile(string filePathXML) {
		xml_parse_result result = doc.load_file(filePathXML.c_str());
	}

	void analyze() {
		
		//for (xml_attribute attr: start.attributes()) {
		//	std::cout << " " << attr.name() << "=" << attr.value();
		//}
		printf("\nStarting analysis of supplied TIL string...\n");

		for (xml_node start: doc.children("start")) {
			int counter = 1;
			for (xml_node sentence: start.children("sentence")) {
				
				green();
				printf("\nAnalyzing sentence %d\n", counter);
				reset();

				if (xml_node entityDefinition = sentence.child("entityDefinition"))
					defineEntity(entityDefinition);
				else
					evaluateConstruction(sentence.first_child());
				
				counter++;
				printf("Done\n");
			}
		}

		green();
		printf("\n");
		printf("▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
		printf("◀ Analysis completed OK ▶\n");
		printf("▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼\n");
		printf("\n\n");
	}


	void defineEntity(xml_node entityDefinition) {
		Entity * newEntity = new Entity();
		newEntity->name = entityDefinition.child("name").child_value();
					
		bool isFirstDataType = true;
		for (xml_node dataType: entityDefinition.children("dataType")) {
			if (isFirstDataType) {
				isFirstDataType = false;
				newEntity->out = dataType.child_value();
			} else {
				newEntity->arg.push_back(dataType.child_value());
			}
		}

		printf("  Defined entity %s / (%s", newEntity->name.c_str(), toGreek(newEntity->out).c_str());
		for(auto ar: newEntity->arg)
			printf(" %s", toGreek(ar).c_str());
		printf(")\n");

		entities.push_back(newEntity);
	}

	void defineLambdaVariable(xml_node variableDefinition) {
		Entity * newEntity = new Entity();
		newEntity->name = variableDefinition.child("name").child_value();
					
		bool isFirstDataType = true;
		for (xml_node dataType: variableDefinition.children("dataType")) {
			if (isFirstDataType) {
				isFirstDataType = false;
				newEntity->out = dataType.child_value();
			} else {
				newEntity->arg.push_back(dataType.child_value());
			}
		}

		printf("  Defined λ-variable %s -> %s\n", newEntity->name.c_str(), toGreek(newEntity->out).c_str());
		entities.push_back(newEntity);
	}



	string evaluateConstruction(xml_node construction) {

		if (string(construction.name()) == "composition")
			return evaluateComposition(construction);

		if (string(construction.name()) == "closure")
			return evaluateClosure(construction);

		
		if (string(construction.name()) == "trivialization")
			return evaluateTrivialization(construction);

		if (string(construction.name()) == "variable")
			return evaluateVariable(construction);

		return "*";
	}


	string evaluateClosure(xml_node closure) {

		// Define variables for use in closure
		for (xml_node variableDefinition: closure.child("lambdaVariables").children()) {
			defineLambdaVariable(variableDefinition);
		}


		// Next closure, probably world -> time -> composition...
		if (xml_node nextClosure = closure.child("closure")) {
			evaluateConstruction(nextClosure);

			return "*";

			printf("  ERR:	Undefined closure %s\n", closure.child_value());
			error();
			
		}


		// Done with closures, move to composition
		if (xml_node composition = closure.child("composition")) {
			evaluateConstruction(composition);

			return "*";

			printf("  ERR:	Undefined composition %s\n", composition.child_value());
			error();
			
		}



		printf("  ERR:	No return in evaluateClosure\n");
		error();
		
	}



	string evaluateComposition(xml_node composition) {

		int currLevel = ++deepLevel;

		if(string(composition.first_child().name()) != "trivialization")
			evaluateConstruction(composition.first_child());


		// Get function and its arguments
		// Should return 
		xml_node functionNode = composition.first_child();
		Entity * function = getFunctionDefinition(composition.first_child());

		printf("%*s", currLevel * 4, "");
		printf("START composition %s\n", function->name.c_str());

		
		
		// Evaluate argument and get return values
		vector<string> arguments = {};
		bool isFirst = true;
		for(xml_node argumentNode : composition.children()) {
			if(isFirst) { isFirst = false; continue; }

			arguments.push_back(evaluateConstruction(argumentNode));
		}



		for(int i = 0; i < arguments.size(); i++) {
			int j = function->arg.size() - 1 - i;
			int k = arguments.size() - 1 - i;

			// Fix for last argument in function...
			if(i == 1 && j == -1) j = 0;

			if(function->arg[j] == arguments[k]) {
				printf("%*s", currLevel * 4, "");
				printf("%s == %s\n", toGreek(function->arg[j]).c_str(), toGreek(arguments[k]).c_str());
				function->arg.erase(function->arg.begin() + j);
			} else {
				printf("%*s", currLevel * 4, "");
				printf("%s != %s\n", toGreek(function->arg[j]).c_str(), toGreek(arguments[k]).c_str());

				printf("%*s", currLevel * 4, "");
				printf("ERR: Entity types doesn't match\n");
				error();
						
			}
		}
			

		printf("%*s", currLevel * 4, "");
		printf("Returning -> %s\n", toGreek(function->out).c_str());

		printf("%*s", currLevel * 4, "");
		printf("END composition %s\n", function->name.c_str());

		return function->out;
	}


	struct find_first_trivialization {
		bool operator()(xml_attribute attr) const {
			return true;
		}

		bool operator()(xml_node node) const {
			return string(node.name()) == "trivialization";
		}
	};


	Entity * getFunctionDefinition(xml_node function) {
		
		// Function in in trivialization, return entity or evaluate with lambdas
		if(string(function.name()) == "trivialization") {
			if (xml_node entity = function.child("entity")) {
				for (Entity * ent : entities)
					if (ent->name == entity.child_value())
						return ent;

				printf("  ERR:	Undefined entity %s\n", entity.child_value());
				error();
				
			}
		}

		// Try to deep-first search of trivialization, maybe from last composition
		if(xml_node deep_function = function.find_node(find_first_trivialization())) {
			if (xml_node entity = deep_function.child("entity")) {
				for (Entity * ent : entities)
					if (ent->name == entity.child_value())
						return ent;

				printf("  ERR:	Undefined deep entity %s\n", entity.child_value());
				error();
				
			}
		}


		printf("  ERR:	No return in getFunctionDefinition\n");
		error();
		
	}

	string evaluateTrivialization(xml_node trivialization) {

		if (xml_node entity = trivialization.child("entity")) {
			for (Entity * ent : entities)
				if (ent->name == entity.child_value())
					return ent->out;

			printf("  ERR:	Undefined entity %s\n", entity.child_value());
			error();
			
		}

		if (xml_node composition = trivialization.child("composition")) {
			evaluateComposition(composition);
			return "*";
		}


		printf("  ERR:	No return in evaluateTrivialization\n");
		error();
		
	}

	

	string evaluateVariable(xml_node variable) {

		for (Entity * ent : entities)
			if (ent->name == variable.child_value())
				return ent->out;

		printf("  ERR:	Undefined variable %s\n", variable.child_value());
		error();
		
	}

	

	vector<Entity> getFunctionArguments(xml_node composition) {

		vector<Entity> retVal = {};

		bool isFirst = true;
		for(xml_node arg : composition.children()) {
			if(isFirst) { isFirst = false; continue; }



		}
		
	}




};







int main(int argc, char *argv[]) {

	std::string tom_resi_sin_x_je_0 = "\
Tom/Indiv.\n\
Resi/(Bool Indiv *)@tw.\n\
Sin/(Real Real).\n\
Nula/Real.\n\
Rovna_se/(Bool Real Real).\n\
[\\x:Real[\\w:World[\\t:Time [[['Resi w]t] 'Tom '['Rovna_se ['Sin x] 'Nula] ]]]].\n\
	";

	std::string adam_je_studentem = "\
Byt_student/(Bool Indiv)@tw.\n\
Adam/Indiv.\n\
[\\w:World[\\t:Time [ [['Byt_student w]t] 'Adam ] ]].\n\
	";

	std::string til_je_izy = "\
Til/Indiv.\n\
Byt_izy/(Bool Indiv).\n\
['Byt_izy 'Til].\n\
	";

	std::string test = "\
Test/(* *).\n\
['Test w].\n\
['Test 'Dpa '['Plus 'Kuba]].\n\
	";



	string main_input_string = "vstup.txt";
	
	if(argv[1])
		main_input_string = argv[1];
	//ANTLRInputStream input(argv[1]);
	ifstream main_input_stream (main_input_string);
	ANTLRInputStream input(main_input_stream);

	TILGrammarLexer lexer(&input);
	CommonTokenStream tokens(&lexer);

	TILGrammarErrorListener errorListener;

	tokens.fill();
	// Only if you want to list the tokens
	for (auto token : tokens.getTokens()) {
		//std::cout << token->getType() << std::endl;
		//std::cout << token->getText() << std::endl;
	}

	TILGrammarParser parser(&tokens);
	parser.removeErrorListeners();
	parser.addErrorListener(&errorListener);
	try {

		
		TILGrammarParser::StartContext* startCtx = parser.start();
		

		ParseTree* tree = startCtx;
		//ReducedTILGrammarVisitor visitor;
		//visitor.visitStart(startCtx);
		
		//XMLTILGrammarVisitor visitor;
		TILGrammarVisitorV2 visitor;
		auto output = visitor.visitStart(startCtx);

		
		return 0;
	} catch (std::invalid_argument &e) {
		red();

		cout << endl << e.what() << endl;
		
		printf("\n");
		printf("▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲\n");
		printf("◀ Parsing ended with ERR ▶\n");
		printf("▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲▼▲\n");
		printf("\n\n");
		return 10;
	}
}