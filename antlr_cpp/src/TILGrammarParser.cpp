
// Generated from TILGrammar.g4 by ANTLR 4.9.2


#include "TILGrammarListener.h"

#include "TILGrammarParser.h"


using namespace antlrcpp;
using namespace antlr4;

TILGrammarParser::TILGrammarParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

TILGrammarParser::~TILGrammarParser() {
  delete _interpreter;
}

std::string TILGrammarParser::getGrammarFileName() const {
  return "TILGrammar.g4";
}

const std::vector<std::string>& TILGrammarParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& TILGrammarParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- StartContext ------------------------------------------------------------------

TILGrammarParser::StartContext::StartContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<TILGrammarParser::SentenceContext *> TILGrammarParser::StartContext::sentence() {
  return getRuleContexts<TILGrammarParser::SentenceContext>();
}

TILGrammarParser::SentenceContext* TILGrammarParser::StartContext::sentence(size_t i) {
  return getRuleContext<TILGrammarParser::SentenceContext>(i);
}


size_t TILGrammarParser::StartContext::getRuleIndex() const {
  return TILGrammarParser::RuleStart;
}

void TILGrammarParser::StartContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStart(this);
}

void TILGrammarParser::StartContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStart(this);
}

TILGrammarParser::StartContext* TILGrammarParser::start() {
  StartContext *_localctx = _tracker.createInstance<StartContext>(_ctx, getState());
  enterRule(_localctx, 0, TILGrammarParser::RuleStart);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(73);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << TILGrammarParser::T__1)
      | (1ULL << TILGrammarParser::T__20)
      | (1ULL << TILGrammarParser::T__21)
      | (1ULL << TILGrammarParser::T__25)
      | (1ULL << TILGrammarParser::T__52)
      | (1ULL << TILGrammarParser::LOWERCASE_LETTER)
      | (1ULL << TILGrammarParser::UPPERCASE_LETTER))) != 0)) {
      setState(70);
      sentence();
      setState(75);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SentenceContext ------------------------------------------------------------------

TILGrammarParser::SentenceContext::SentenceContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::Sentence_contentContext* TILGrammarParser::SentenceContext::sentence_content() {
  return getRuleContext<TILGrammarParser::Sentence_contentContext>(0);
}

TILGrammarParser::TerminationContext* TILGrammarParser::SentenceContext::termination() {
  return getRuleContext<TILGrammarParser::TerminationContext>(0);
}


size_t TILGrammarParser::SentenceContext::getRuleIndex() const {
  return TILGrammarParser::RuleSentence;
}

void TILGrammarParser::SentenceContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterSentence(this);
}

void TILGrammarParser::SentenceContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitSentence(this);
}

TILGrammarParser::SentenceContext* TILGrammarParser::sentence() {
  SentenceContext *_localctx = _tracker.createInstance<SentenceContext>(_ctx, getState());
  enterRule(_localctx, 2, TILGrammarParser::RuleSentence);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(76);
    sentence_content();
    setState(77);
    termination();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Sentence_contentContext ------------------------------------------------------------------

TILGrammarParser::Sentence_contentContext::Sentence_contentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::Type_definitionContext* TILGrammarParser::Sentence_contentContext::type_definition() {
  return getRuleContext<TILGrammarParser::Type_definitionContext>(0);
}

TILGrammarParser::Entity_definitionContext* TILGrammarParser::Sentence_contentContext::entity_definition() {
  return getRuleContext<TILGrammarParser::Entity_definitionContext>(0);
}

TILGrammarParser::ConstructionContext* TILGrammarParser::Sentence_contentContext::construction() {
  return getRuleContext<TILGrammarParser::ConstructionContext>(0);
}

TILGrammarParser::Global_variable_definitionContext* TILGrammarParser::Sentence_contentContext::global_variable_definition() {
  return getRuleContext<TILGrammarParser::Global_variable_definitionContext>(0);
}


size_t TILGrammarParser::Sentence_contentContext::getRuleIndex() const {
  return TILGrammarParser::RuleSentence_content;
}

void TILGrammarParser::Sentence_contentContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterSentence_content(this);
}

void TILGrammarParser::Sentence_contentContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitSentence_content(this);
}

TILGrammarParser::Sentence_contentContext* TILGrammarParser::sentence_content() {
  Sentence_contentContext *_localctx = _tracker.createInstance<Sentence_contentContext>(_ctx, getState());
  enterRule(_localctx, 4, TILGrammarParser::RuleSentence_content);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(83);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 1, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(79);
      type_definition();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(80);
      entity_definition();
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(81);
      construction();
      break;
    }

    case 4: {
      enterOuterAlt(_localctx, 4);
      setState(82);
      global_variable_definition();
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TerminationContext ------------------------------------------------------------------

TILGrammarParser::TerminationContext::TerminationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<TILGrammarParser::Optional_whitespaceContext *> TILGrammarParser::TerminationContext::optional_whitespace() {
  return getRuleContexts<TILGrammarParser::Optional_whitespaceContext>();
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::TerminationContext::optional_whitespace(size_t i) {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(i);
}


size_t TILGrammarParser::TerminationContext::getRuleIndex() const {
  return TILGrammarParser::RuleTermination;
}

void TILGrammarParser::TerminationContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterTermination(this);
}

void TILGrammarParser::TerminationContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitTermination(this);
}

TILGrammarParser::TerminationContext* TILGrammarParser::termination() {
  TerminationContext *_localctx = _tracker.createInstance<TerminationContext>(_ctx, getState());
  enterRule(_localctx, 6, TILGrammarParser::RuleTermination);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(85);
    optional_whitespace();
    setState(86);
    match(TILGrammarParser::T__0);
    setState(87);
    optional_whitespace();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Type_definitionContext ------------------------------------------------------------------

TILGrammarParser::Type_definitionContext::Type_definitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::WhitespaceContext* TILGrammarParser::Type_definitionContext::whitespace() {
  return getRuleContext<TILGrammarParser::WhitespaceContext>(0);
}

TILGrammarParser::Type_nameContext* TILGrammarParser::Type_definitionContext::type_name() {
  return getRuleContext<TILGrammarParser::Type_nameContext>(0);
}

std::vector<TILGrammarParser::Optional_whitespaceContext *> TILGrammarParser::Type_definitionContext::optional_whitespace() {
  return getRuleContexts<TILGrammarParser::Optional_whitespaceContext>();
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::Type_definitionContext::optional_whitespace(size_t i) {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(i);
}

TILGrammarParser::Data_typeContext* TILGrammarParser::Type_definitionContext::data_type() {
  return getRuleContext<TILGrammarParser::Data_typeContext>(0);
}


size_t TILGrammarParser::Type_definitionContext::getRuleIndex() const {
  return TILGrammarParser::RuleType_definition;
}

void TILGrammarParser::Type_definitionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterType_definition(this);
}

void TILGrammarParser::Type_definitionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitType_definition(this);
}

TILGrammarParser::Type_definitionContext* TILGrammarParser::type_definition() {
  Type_definitionContext *_localctx = _tracker.createInstance<Type_definitionContext>(_ctx, getState());
  enterRule(_localctx, 8, TILGrammarParser::RuleType_definition);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(89);
    match(TILGrammarParser::T__1);
    setState(90);
    whitespace();
    setState(91);
    type_name();
    setState(92);
    optional_whitespace();
    setState(93);
    match(TILGrammarParser::T__2);
    setState(94);
    optional_whitespace();
    setState(95);
    data_type();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Entity_definitionContext ------------------------------------------------------------------

TILGrammarParser::Entity_definitionContext::Entity_definitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<TILGrammarParser::Entity_nameContext *> TILGrammarParser::Entity_definitionContext::entity_name() {
  return getRuleContexts<TILGrammarParser::Entity_nameContext>();
}

TILGrammarParser::Entity_nameContext* TILGrammarParser::Entity_definitionContext::entity_name(size_t i) {
  return getRuleContext<TILGrammarParser::Entity_nameContext>(i);
}

std::vector<TILGrammarParser::Optional_whitespaceContext *> TILGrammarParser::Entity_definitionContext::optional_whitespace() {
  return getRuleContexts<TILGrammarParser::Optional_whitespaceContext>();
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::Entity_definitionContext::optional_whitespace(size_t i) {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(i);
}

TILGrammarParser::Data_typeContext* TILGrammarParser::Entity_definitionContext::data_type() {
  return getRuleContext<TILGrammarParser::Data_typeContext>(0);
}


size_t TILGrammarParser::Entity_definitionContext::getRuleIndex() const {
  return TILGrammarParser::RuleEntity_definition;
}

void TILGrammarParser::Entity_definitionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterEntity_definition(this);
}

void TILGrammarParser::Entity_definitionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitEntity_definition(this);
}

TILGrammarParser::Entity_definitionContext* TILGrammarParser::entity_definition() {
  Entity_definitionContext *_localctx = _tracker.createInstance<Entity_definitionContext>(_ctx, getState());
  enterRule(_localctx, 10, TILGrammarParser::RuleEntity_definition);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(97);
    entity_name();
    setState(105);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 2, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(98);
        optional_whitespace();
        setState(99);
        match(TILGrammarParser::T__3);
        setState(100);
        optional_whitespace();
        setState(101);
        entity_name(); 
      }
      setState(107);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 2, _ctx);
    }
    setState(108);
    optional_whitespace();
    setState(109);
    match(TILGrammarParser::T__4);
    setState(110);
    optional_whitespace();
    setState(111);
    data_type();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ConstructionContext ------------------------------------------------------------------

TILGrammarParser::ConstructionContext::ConstructionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::TrivialisationContext* TILGrammarParser::ConstructionContext::trivialisation() {
  return getRuleContext<TILGrammarParser::TrivialisationContext>(0);
}

TILGrammarParser::VariableContext* TILGrammarParser::ConstructionContext::variable() {
  return getRuleContext<TILGrammarParser::VariableContext>(0);
}

TILGrammarParser::ClosureContext* TILGrammarParser::ConstructionContext::closure() {
  return getRuleContext<TILGrammarParser::ClosureContext>(0);
}

TILGrammarParser::N_executionContext* TILGrammarParser::ConstructionContext::n_execution() {
  return getRuleContext<TILGrammarParser::N_executionContext>(0);
}

TILGrammarParser::CompositionContext* TILGrammarParser::ConstructionContext::composition() {
  return getRuleContext<TILGrammarParser::CompositionContext>(0);
}


size_t TILGrammarParser::ConstructionContext::getRuleIndex() const {
  return TILGrammarParser::RuleConstruction;
}

void TILGrammarParser::ConstructionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterConstruction(this);
}

void TILGrammarParser::ConstructionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitConstruction(this);
}

TILGrammarParser::ConstructionContext* TILGrammarParser::construction() {
  ConstructionContext *_localctx = _tracker.createInstance<ConstructionContext>(_ctx, getState());
  enterRule(_localctx, 12, TILGrammarParser::RuleConstruction);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(118);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 3, _ctx)) {
    case 1: {
      setState(113);
      trivialisation();
      break;
    }

    case 2: {
      setState(114);
      variable();
      break;
    }

    case 3: {
      setState(115);
      closure();
      break;
    }

    case 4: {
      setState(116);
      n_execution();
      break;
    }

    case 5: {
      setState(117);
      composition();
      break;
    }

    default:
      break;
    }
    setState(121);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx)) {
    case 1: {
      setState(120);
      match(TILGrammarParser::T__5);
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Global_variable_definitionContext ------------------------------------------------------------------

TILGrammarParser::Global_variable_definitionContext::Global_variable_definitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<TILGrammarParser::Variable_nameContext *> TILGrammarParser::Global_variable_definitionContext::variable_name() {
  return getRuleContexts<TILGrammarParser::Variable_nameContext>();
}

TILGrammarParser::Variable_nameContext* TILGrammarParser::Global_variable_definitionContext::variable_name(size_t i) {
  return getRuleContext<TILGrammarParser::Variable_nameContext>(i);
}

std::vector<TILGrammarParser::Optional_whitespaceContext *> TILGrammarParser::Global_variable_definitionContext::optional_whitespace() {
  return getRuleContexts<TILGrammarParser::Optional_whitespaceContext>();
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::Global_variable_definitionContext::optional_whitespace(size_t i) {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(i);
}

TILGrammarParser::Data_typeContext* TILGrammarParser::Global_variable_definitionContext::data_type() {
  return getRuleContext<TILGrammarParser::Data_typeContext>(0);
}


size_t TILGrammarParser::Global_variable_definitionContext::getRuleIndex() const {
  return TILGrammarParser::RuleGlobal_variable_definition;
}

void TILGrammarParser::Global_variable_definitionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterGlobal_variable_definition(this);
}

void TILGrammarParser::Global_variable_definitionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitGlobal_variable_definition(this);
}

TILGrammarParser::Global_variable_definitionContext* TILGrammarParser::global_variable_definition() {
  Global_variable_definitionContext *_localctx = _tracker.createInstance<Global_variable_definitionContext>(_ctx, getState());
  enterRule(_localctx, 14, TILGrammarParser::RuleGlobal_variable_definition);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(123);
    variable_name();
    setState(131);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 5, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(124);
        optional_whitespace();
        setState(125);
        match(TILGrammarParser::T__3);
        setState(126);
        optional_whitespace();
        setState(127);
        variable_name(); 
      }
      setState(133);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 5, _ctx);
    }
    setState(134);
    optional_whitespace();
    setState(135);
    match(TILGrammarParser::T__6);
    setState(136);
    optional_whitespace();
    setState(137);
    data_type();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Data_typeContext ------------------------------------------------------------------

TILGrammarParser::Data_typeContext::Data_typeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::Embeded_typeContext* TILGrammarParser::Data_typeContext::embeded_type() {
  return getRuleContext<TILGrammarParser::Embeded_typeContext>(0);
}

TILGrammarParser::List_typeContext* TILGrammarParser::Data_typeContext::list_type() {
  return getRuleContext<TILGrammarParser::List_typeContext>(0);
}

TILGrammarParser::Touple_typeContext* TILGrammarParser::Data_typeContext::touple_type() {
  return getRuleContext<TILGrammarParser::Touple_typeContext>(0);
}

TILGrammarParser::User_typeContext* TILGrammarParser::Data_typeContext::user_type() {
  return getRuleContext<TILGrammarParser::User_typeContext>(0);
}

TILGrammarParser::Enclosed_data_typeContext* TILGrammarParser::Data_typeContext::enclosed_data_type() {
  return getRuleContext<TILGrammarParser::Enclosed_data_typeContext>(0);
}


size_t TILGrammarParser::Data_typeContext::getRuleIndex() const {
  return TILGrammarParser::RuleData_type;
}

void TILGrammarParser::Data_typeContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterData_type(this);
}

void TILGrammarParser::Data_typeContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitData_type(this);
}

TILGrammarParser::Data_typeContext* TILGrammarParser::data_type() {
  Data_typeContext *_localctx = _tracker.createInstance<Data_typeContext>(_ctx, getState());
  enterRule(_localctx, 16, TILGrammarParser::RuleData_type);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(144);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case TILGrammarParser::T__8:
      case TILGrammarParser::T__9:
      case TILGrammarParser::T__10:
      case TILGrammarParser::T__11:
      case TILGrammarParser::T__12:
      case TILGrammarParser::T__13:
      case TILGrammarParser::T__14:
      case TILGrammarParser::T__15:
      case TILGrammarParser::T__50: {
        setState(139);
        embeded_type();
        break;
      }

      case TILGrammarParser::T__16: {
        setState(140);
        list_type();
        break;
      }

      case TILGrammarParser::T__19: {
        setState(141);
        touple_type();
        break;
      }

      case TILGrammarParser::UPPERCASE_LETTER: {
        setState(142);
        user_type();
        break;
      }

      case TILGrammarParser::T__17: {
        setState(143);
        enclosed_data_type();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    setState(147);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == TILGrammarParser::T__7) {
      setState(146);
      match(TILGrammarParser::T__7);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Embeded_typeContext ------------------------------------------------------------------

TILGrammarParser::Embeded_typeContext::Embeded_typeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::AnythingContext* TILGrammarParser::Embeded_typeContext::anything() {
  return getRuleContext<TILGrammarParser::AnythingContext>(0);
}


size_t TILGrammarParser::Embeded_typeContext::getRuleIndex() const {
  return TILGrammarParser::RuleEmbeded_type;
}

void TILGrammarParser::Embeded_typeContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterEmbeded_type(this);
}

void TILGrammarParser::Embeded_typeContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitEmbeded_type(this);
}

TILGrammarParser::Embeded_typeContext* TILGrammarParser::embeded_type() {
  Embeded_typeContext *_localctx = _tracker.createInstance<Embeded_typeContext>(_ctx, getState());
  enterRule(_localctx, 18, TILGrammarParser::RuleEmbeded_type);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(158);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case TILGrammarParser::T__8: {
        enterOuterAlt(_localctx, 1);
        setState(149);
        match(TILGrammarParser::T__8);
        break;
      }

      case TILGrammarParser::T__9: {
        enterOuterAlt(_localctx, 2);
        setState(150);
        match(TILGrammarParser::T__9);
        break;
      }

      case TILGrammarParser::T__10: {
        enterOuterAlt(_localctx, 3);
        setState(151);
        match(TILGrammarParser::T__10);
        break;
      }

      case TILGrammarParser::T__11: {
        enterOuterAlt(_localctx, 4);
        setState(152);
        match(TILGrammarParser::T__11);
        break;
      }

      case TILGrammarParser::T__12: {
        enterOuterAlt(_localctx, 5);
        setState(153);
        match(TILGrammarParser::T__12);
        break;
      }

      case TILGrammarParser::T__13: {
        enterOuterAlt(_localctx, 6);
        setState(154);
        match(TILGrammarParser::T__13);
        break;
      }

      case TILGrammarParser::T__14: {
        enterOuterAlt(_localctx, 7);
        setState(155);
        match(TILGrammarParser::T__14);
        break;
      }

      case TILGrammarParser::T__50: {
        enterOuterAlt(_localctx, 8);
        setState(156);
        anything();
        break;
      }

      case TILGrammarParser::T__15: {
        enterOuterAlt(_localctx, 9);
        setState(157);
        match(TILGrammarParser::T__15);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- List_typeContext ------------------------------------------------------------------

TILGrammarParser::List_typeContext::List_typeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<TILGrammarParser::Optional_whitespaceContext *> TILGrammarParser::List_typeContext::optional_whitespace() {
  return getRuleContexts<TILGrammarParser::Optional_whitespaceContext>();
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::List_typeContext::optional_whitespace(size_t i) {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(i);
}

TILGrammarParser::Data_typeContext* TILGrammarParser::List_typeContext::data_type() {
  return getRuleContext<TILGrammarParser::Data_typeContext>(0);
}


size_t TILGrammarParser::List_typeContext::getRuleIndex() const {
  return TILGrammarParser::RuleList_type;
}

void TILGrammarParser::List_typeContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterList_type(this);
}

void TILGrammarParser::List_typeContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitList_type(this);
}

TILGrammarParser::List_typeContext* TILGrammarParser::list_type() {
  List_typeContext *_localctx = _tracker.createInstance<List_typeContext>(_ctx, getState());
  enterRule(_localctx, 20, TILGrammarParser::RuleList_type);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(160);
    match(TILGrammarParser::T__16);
    setState(161);
    optional_whitespace();
    setState(162);
    match(TILGrammarParser::T__17);
    setState(163);
    optional_whitespace();
    setState(164);
    data_type();
    setState(165);
    optional_whitespace();
    setState(166);
    match(TILGrammarParser::T__18);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Touple_typeContext ------------------------------------------------------------------

TILGrammarParser::Touple_typeContext::Touple_typeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<TILGrammarParser::Optional_whitespaceContext *> TILGrammarParser::Touple_typeContext::optional_whitespace() {
  return getRuleContexts<TILGrammarParser::Optional_whitespaceContext>();
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::Touple_typeContext::optional_whitespace(size_t i) {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(i);
}

TILGrammarParser::Data_typeContext* TILGrammarParser::Touple_typeContext::data_type() {
  return getRuleContext<TILGrammarParser::Data_typeContext>(0);
}


size_t TILGrammarParser::Touple_typeContext::getRuleIndex() const {
  return TILGrammarParser::RuleTouple_type;
}

void TILGrammarParser::Touple_typeContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterTouple_type(this);
}

void TILGrammarParser::Touple_typeContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitTouple_type(this);
}

TILGrammarParser::Touple_typeContext* TILGrammarParser::touple_type() {
  Touple_typeContext *_localctx = _tracker.createInstance<Touple_typeContext>(_ctx, getState());
  enterRule(_localctx, 22, TILGrammarParser::RuleTouple_type);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(168);
    match(TILGrammarParser::T__19);
    setState(169);
    optional_whitespace();
    setState(170);
    match(TILGrammarParser::T__17);
    setState(171);
    optional_whitespace();
    setState(172);
    data_type();
    setState(173);
    optional_whitespace();
    setState(174);
    match(TILGrammarParser::T__18);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- User_typeContext ------------------------------------------------------------------

TILGrammarParser::User_typeContext::User_typeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::Type_nameContext* TILGrammarParser::User_typeContext::type_name() {
  return getRuleContext<TILGrammarParser::Type_nameContext>(0);
}


size_t TILGrammarParser::User_typeContext::getRuleIndex() const {
  return TILGrammarParser::RuleUser_type;
}

void TILGrammarParser::User_typeContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterUser_type(this);
}

void TILGrammarParser::User_typeContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitUser_type(this);
}

TILGrammarParser::User_typeContext* TILGrammarParser::user_type() {
  User_typeContext *_localctx = _tracker.createInstance<User_typeContext>(_ctx, getState());
  enterRule(_localctx, 24, TILGrammarParser::RuleUser_type);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(176);
    type_name();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Enclosed_data_typeContext ------------------------------------------------------------------

TILGrammarParser::Enclosed_data_typeContext::Enclosed_data_typeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<TILGrammarParser::Optional_whitespaceContext *> TILGrammarParser::Enclosed_data_typeContext::optional_whitespace() {
  return getRuleContexts<TILGrammarParser::Optional_whitespaceContext>();
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::Enclosed_data_typeContext::optional_whitespace(size_t i) {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(i);
}

std::vector<TILGrammarParser::Data_typeContext *> TILGrammarParser::Enclosed_data_typeContext::data_type() {
  return getRuleContexts<TILGrammarParser::Data_typeContext>();
}

TILGrammarParser::Data_typeContext* TILGrammarParser::Enclosed_data_typeContext::data_type(size_t i) {
  return getRuleContext<TILGrammarParser::Data_typeContext>(i);
}

std::vector<TILGrammarParser::WhitespaceContext *> TILGrammarParser::Enclosed_data_typeContext::whitespace() {
  return getRuleContexts<TILGrammarParser::WhitespaceContext>();
}

TILGrammarParser::WhitespaceContext* TILGrammarParser::Enclosed_data_typeContext::whitespace(size_t i) {
  return getRuleContext<TILGrammarParser::WhitespaceContext>(i);
}


size_t TILGrammarParser::Enclosed_data_typeContext::getRuleIndex() const {
  return TILGrammarParser::RuleEnclosed_data_type;
}

void TILGrammarParser::Enclosed_data_typeContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterEnclosed_data_type(this);
}

void TILGrammarParser::Enclosed_data_typeContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitEnclosed_data_type(this);
}

TILGrammarParser::Enclosed_data_typeContext* TILGrammarParser::enclosed_data_type() {
  Enclosed_data_typeContext *_localctx = _tracker.createInstance<Enclosed_data_typeContext>(_ctx, getState());
  enterRule(_localctx, 26, TILGrammarParser::RuleEnclosed_data_type);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(178);
    match(TILGrammarParser::T__17);
    setState(179);
    optional_whitespace();
    setState(180);
    data_type();
    setState(186);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 9, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(181);
        whitespace();
        setState(182);
        data_type(); 
      }
      setState(188);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 9, _ctx);
    }
    setState(189);
    optional_whitespace();
    setState(190);
    match(TILGrammarParser::T__18);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableContext ------------------------------------------------------------------

TILGrammarParser::VariableContext::VariableContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::Variable_nameContext* TILGrammarParser::VariableContext::variable_name() {
  return getRuleContext<TILGrammarParser::Variable_nameContext>(0);
}


size_t TILGrammarParser::VariableContext::getRuleIndex() const {
  return TILGrammarParser::RuleVariable;
}

void TILGrammarParser::VariableContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterVariable(this);
}

void TILGrammarParser::VariableContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitVariable(this);
}

TILGrammarParser::VariableContext* TILGrammarParser::variable() {
  VariableContext *_localctx = _tracker.createInstance<VariableContext>(_ctx, getState());
  enterRule(_localctx, 28, TILGrammarParser::RuleVariable);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(192);
    variable_name();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TrivialisationContext ------------------------------------------------------------------

TILGrammarParser::TrivialisationContext::TrivialisationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::TrivialisationContext::optional_whitespace() {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(0);
}

TILGrammarParser::ConstructionContext* TILGrammarParser::TrivialisationContext::construction() {
  return getRuleContext<TILGrammarParser::ConstructionContext>(0);
}

TILGrammarParser::EntityContext* TILGrammarParser::TrivialisationContext::entity() {
  return getRuleContext<TILGrammarParser::EntityContext>(0);
}


size_t TILGrammarParser::TrivialisationContext::getRuleIndex() const {
  return TILGrammarParser::RuleTrivialisation;
}

void TILGrammarParser::TrivialisationContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterTrivialisation(this);
}

void TILGrammarParser::TrivialisationContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitTrivialisation(this);
}

TILGrammarParser::TrivialisationContext* TILGrammarParser::trivialisation() {
  TrivialisationContext *_localctx = _tracker.createInstance<TrivialisationContext>(_ctx, getState());
  enterRule(_localctx, 30, TILGrammarParser::RuleTrivialisation);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(194);
    match(TILGrammarParser::T__20);
    setState(195);
    optional_whitespace();
    setState(198);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case TILGrammarParser::T__20:
      case TILGrammarParser::T__21:
      case TILGrammarParser::T__25:
      case TILGrammarParser::LOWERCASE_LETTER: {
        setState(196);
        construction();
        break;
      }

      case TILGrammarParser::T__4:
      case TILGrammarParser::T__15:
      case TILGrammarParser::T__26:
      case TILGrammarParser::T__27:
      case TILGrammarParser::T__28:
      case TILGrammarParser::T__29:
      case TILGrammarParser::T__30:
      case TILGrammarParser::T__31:
      case TILGrammarParser::T__32:
      case TILGrammarParser::T__33:
      case TILGrammarParser::T__34:
      case TILGrammarParser::T__35:
      case TILGrammarParser::T__36:
      case TILGrammarParser::T__37:
      case TILGrammarParser::T__38:
      case TILGrammarParser::T__39:
      case TILGrammarParser::T__40:
      case TILGrammarParser::T__41:
      case TILGrammarParser::T__42:
      case TILGrammarParser::T__43:
      case TILGrammarParser::T__44:
      case TILGrammarParser::T__45:
      case TILGrammarParser::T__46:
      case TILGrammarParser::T__47:
      case TILGrammarParser::T__48:
      case TILGrammarParser::T__49:
      case TILGrammarParser::T__52:
      case TILGrammarParser::UPPERCASE_LETTER:
      case TILGrammarParser::ZERO:
      case TILGrammarParser::NONZERO_DIGIT: {
        setState(197);
        entity();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CompositionContext ------------------------------------------------------------------

TILGrammarParser::CompositionContext::CompositionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<TILGrammarParser::Optional_whitespaceContext *> TILGrammarParser::CompositionContext::optional_whitespace() {
  return getRuleContexts<TILGrammarParser::Optional_whitespaceContext>();
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::CompositionContext::optional_whitespace(size_t i) {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(i);
}

std::vector<TILGrammarParser::ConstructionContext *> TILGrammarParser::CompositionContext::construction() {
  return getRuleContexts<TILGrammarParser::ConstructionContext>();
}

TILGrammarParser::ConstructionContext* TILGrammarParser::CompositionContext::construction(size_t i) {
  return getRuleContext<TILGrammarParser::ConstructionContext>(i);
}


size_t TILGrammarParser::CompositionContext::getRuleIndex() const {
  return TILGrammarParser::RuleComposition;
}

void TILGrammarParser::CompositionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterComposition(this);
}

void TILGrammarParser::CompositionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitComposition(this);
}

TILGrammarParser::CompositionContext* TILGrammarParser::composition() {
  CompositionContext *_localctx = _tracker.createInstance<CompositionContext>(_ctx, getState());
  enterRule(_localctx, 32, TILGrammarParser::RuleComposition);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(200);
    match(TILGrammarParser::T__21);
    setState(201);
    optional_whitespace();
    setState(202);
    construction();
    setState(203);
    optional_whitespace();
    setState(204);
    construction();
    setState(205);
    optional_whitespace();
    setState(209);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << TILGrammarParser::T__20)
      | (1ULL << TILGrammarParser::T__21)
      | (1ULL << TILGrammarParser::T__25)
      | (1ULL << TILGrammarParser::LOWERCASE_LETTER))) != 0)) {
      setState(206);
      construction();
      setState(211);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(212);
    optional_whitespace();
    setState(213);
    match(TILGrammarParser::T__22);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ClosureContext ------------------------------------------------------------------

TILGrammarParser::ClosureContext::ClosureContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<TILGrammarParser::Optional_whitespaceContext *> TILGrammarParser::ClosureContext::optional_whitespace() {
  return getRuleContexts<TILGrammarParser::Optional_whitespaceContext>();
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::ClosureContext::optional_whitespace(size_t i) {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(i);
}

TILGrammarParser::Lambda_variablesContext* TILGrammarParser::ClosureContext::lambda_variables() {
  return getRuleContext<TILGrammarParser::Lambda_variablesContext>(0);
}

TILGrammarParser::ConstructionContext* TILGrammarParser::ClosureContext::construction() {
  return getRuleContext<TILGrammarParser::ConstructionContext>(0);
}


size_t TILGrammarParser::ClosureContext::getRuleIndex() const {
  return TILGrammarParser::RuleClosure;
}

void TILGrammarParser::ClosureContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterClosure(this);
}

void TILGrammarParser::ClosureContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitClosure(this);
}

TILGrammarParser::ClosureContext* TILGrammarParser::closure() {
  ClosureContext *_localctx = _tracker.createInstance<ClosureContext>(_ctx, getState());
  enterRule(_localctx, 34, TILGrammarParser::RuleClosure);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(215);
    match(TILGrammarParser::T__21);
    setState(216);
    optional_whitespace();
    setState(217);
    lambda_variables();
    setState(218);
    optional_whitespace();
    setState(219);
    construction();
    setState(220);
    optional_whitespace();
    setState(221);
    match(TILGrammarParser::T__22);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Lambda_variablesContext ------------------------------------------------------------------

TILGrammarParser::Lambda_variablesContext::Lambda_variablesContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::Lambda_variablesContext::optional_whitespace() {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(0);
}

TILGrammarParser::Typed_variablesContext* TILGrammarParser::Lambda_variablesContext::typed_variables() {
  return getRuleContext<TILGrammarParser::Typed_variablesContext>(0);
}


size_t TILGrammarParser::Lambda_variablesContext::getRuleIndex() const {
  return TILGrammarParser::RuleLambda_variables;
}

void TILGrammarParser::Lambda_variablesContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLambda_variables(this);
}

void TILGrammarParser::Lambda_variablesContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLambda_variables(this);
}

TILGrammarParser::Lambda_variablesContext* TILGrammarParser::lambda_variables() {
  Lambda_variablesContext *_localctx = _tracker.createInstance<Lambda_variablesContext>(_ctx, getState());
  enterRule(_localctx, 36, TILGrammarParser::RuleLambda_variables);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(223);
    match(TILGrammarParser::T__23);
    setState(224);
    optional_whitespace();
    setState(225);
    typed_variables();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Typed_variablesContext ------------------------------------------------------------------

TILGrammarParser::Typed_variablesContext::Typed_variablesContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<TILGrammarParser::Typed_variableContext *> TILGrammarParser::Typed_variablesContext::typed_variable() {
  return getRuleContexts<TILGrammarParser::Typed_variableContext>();
}

TILGrammarParser::Typed_variableContext* TILGrammarParser::Typed_variablesContext::typed_variable(size_t i) {
  return getRuleContext<TILGrammarParser::Typed_variableContext>(i);
}

std::vector<TILGrammarParser::Optional_whitespaceContext *> TILGrammarParser::Typed_variablesContext::optional_whitespace() {
  return getRuleContexts<TILGrammarParser::Optional_whitespaceContext>();
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::Typed_variablesContext::optional_whitespace(size_t i) {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(i);
}


size_t TILGrammarParser::Typed_variablesContext::getRuleIndex() const {
  return TILGrammarParser::RuleTyped_variables;
}

void TILGrammarParser::Typed_variablesContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterTyped_variables(this);
}

void TILGrammarParser::Typed_variablesContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitTyped_variables(this);
}

TILGrammarParser::Typed_variablesContext* TILGrammarParser::typed_variables() {
  Typed_variablesContext *_localctx = _tracker.createInstance<Typed_variablesContext>(_ctx, getState());
  enterRule(_localctx, 38, TILGrammarParser::RuleTyped_variables);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(227);
    typed_variable();
    setState(234);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 12, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(228);
        optional_whitespace();
        setState(229);
        match(TILGrammarParser::T__3);
        setState(230);
        typed_variable(); 
      }
      setState(236);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 12, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Typed_variableContext ------------------------------------------------------------------

TILGrammarParser::Typed_variableContext::Typed_variableContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::Lowerletter_nameContext* TILGrammarParser::Typed_variableContext::lowerletter_name() {
  return getRuleContext<TILGrammarParser::Lowerletter_nameContext>(0);
}

std::vector<TILGrammarParser::Optional_whitespaceContext *> TILGrammarParser::Typed_variableContext::optional_whitespace() {
  return getRuleContexts<TILGrammarParser::Optional_whitespaceContext>();
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::Typed_variableContext::optional_whitespace(size_t i) {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(i);
}

TILGrammarParser::Data_typeContext* TILGrammarParser::Typed_variableContext::data_type() {
  return getRuleContext<TILGrammarParser::Data_typeContext>(0);
}


size_t TILGrammarParser::Typed_variableContext::getRuleIndex() const {
  return TILGrammarParser::RuleTyped_variable;
}

void TILGrammarParser::Typed_variableContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterTyped_variable(this);
}

void TILGrammarParser::Typed_variableContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitTyped_variable(this);
}

TILGrammarParser::Typed_variableContext* TILGrammarParser::typed_variable() {
  Typed_variableContext *_localctx = _tracker.createInstance<Typed_variableContext>(_ctx, getState());
  enterRule(_localctx, 40, TILGrammarParser::RuleTyped_variable);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(237);
    lowerletter_name();
    setState(243);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 13, _ctx)) {
    case 1: {
      setState(238);
      optional_whitespace();
      setState(239);
      match(TILGrammarParser::T__24);
      setState(240);
      optional_whitespace();
      setState(241);
      data_type();
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- N_executionContext ------------------------------------------------------------------

TILGrammarParser::N_executionContext::N_executionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<TILGrammarParser::Optional_whitespaceContext *> TILGrammarParser::N_executionContext::optional_whitespace() {
  return getRuleContexts<TILGrammarParser::Optional_whitespaceContext>();
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::N_executionContext::optional_whitespace(size_t i) {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(i);
}

tree::TerminalNode* TILGrammarParser::N_executionContext::NONZERO_DIGIT() {
  return getToken(TILGrammarParser::NONZERO_DIGIT, 0);
}

TILGrammarParser::ConstructionContext* TILGrammarParser::N_executionContext::construction() {
  return getRuleContext<TILGrammarParser::ConstructionContext>(0);
}

TILGrammarParser::EntityContext* TILGrammarParser::N_executionContext::entity() {
  return getRuleContext<TILGrammarParser::EntityContext>(0);
}


size_t TILGrammarParser::N_executionContext::getRuleIndex() const {
  return TILGrammarParser::RuleN_execution;
}

void TILGrammarParser::N_executionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterN_execution(this);
}

void TILGrammarParser::N_executionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitN_execution(this);
}

TILGrammarParser::N_executionContext* TILGrammarParser::n_execution() {
  N_executionContext *_localctx = _tracker.createInstance<N_executionContext>(_ctx, getState());
  enterRule(_localctx, 42, TILGrammarParser::RuleN_execution);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(245);
    match(TILGrammarParser::T__25);
    setState(246);
    optional_whitespace();
    setState(247);
    match(TILGrammarParser::NONZERO_DIGIT);
    setState(248);
    optional_whitespace();
    setState(251);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case TILGrammarParser::T__20:
      case TILGrammarParser::T__21:
      case TILGrammarParser::T__25:
      case TILGrammarParser::LOWERCASE_LETTER: {
        setState(249);
        construction();
        break;
      }

      case TILGrammarParser::T__4:
      case TILGrammarParser::T__15:
      case TILGrammarParser::T__26:
      case TILGrammarParser::T__27:
      case TILGrammarParser::T__28:
      case TILGrammarParser::T__29:
      case TILGrammarParser::T__30:
      case TILGrammarParser::T__31:
      case TILGrammarParser::T__32:
      case TILGrammarParser::T__33:
      case TILGrammarParser::T__34:
      case TILGrammarParser::T__35:
      case TILGrammarParser::T__36:
      case TILGrammarParser::T__37:
      case TILGrammarParser::T__38:
      case TILGrammarParser::T__39:
      case TILGrammarParser::T__40:
      case TILGrammarParser::T__41:
      case TILGrammarParser::T__42:
      case TILGrammarParser::T__43:
      case TILGrammarParser::T__44:
      case TILGrammarParser::T__45:
      case TILGrammarParser::T__46:
      case TILGrammarParser::T__47:
      case TILGrammarParser::T__48:
      case TILGrammarParser::T__49:
      case TILGrammarParser::T__52:
      case TILGrammarParser::UPPERCASE_LETTER:
      case TILGrammarParser::ZERO:
      case TILGrammarParser::NONZERO_DIGIT: {
        setState(250);
        entity();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- EntityContext ------------------------------------------------------------------

TILGrammarParser::EntityContext::EntityContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::KeywordContext* TILGrammarParser::EntityContext::keyword() {
  return getRuleContext<TILGrammarParser::KeywordContext>(0);
}

TILGrammarParser::Entity_nameContext* TILGrammarParser::EntityContext::entity_name() {
  return getRuleContext<TILGrammarParser::Entity_nameContext>(0);
}

TILGrammarParser::NumberContext* TILGrammarParser::EntityContext::number() {
  return getRuleContext<TILGrammarParser::NumberContext>(0);
}

TILGrammarParser::SymbolsContext* TILGrammarParser::EntityContext::symbols() {
  return getRuleContext<TILGrammarParser::SymbolsContext>(0);
}


size_t TILGrammarParser::EntityContext::getRuleIndex() const {
  return TILGrammarParser::RuleEntity;
}

void TILGrammarParser::EntityContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterEntity(this);
}

void TILGrammarParser::EntityContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitEntity(this);
}

TILGrammarParser::EntityContext* TILGrammarParser::entity() {
  EntityContext *_localctx = _tracker.createInstance<EntityContext>(_ctx, getState());
  enterRule(_localctx, 44, TILGrammarParser::RuleEntity);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(257);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case TILGrammarParser::T__26:
      case TILGrammarParser::T__27:
      case TILGrammarParser::T__28:
      case TILGrammarParser::T__29:
      case TILGrammarParser::T__30:
      case TILGrammarParser::T__31:
      case TILGrammarParser::T__32:
      case TILGrammarParser::T__33:
      case TILGrammarParser::T__34:
      case TILGrammarParser::T__35:
      case TILGrammarParser::T__36:
      case TILGrammarParser::T__37:
      case TILGrammarParser::T__38:
      case TILGrammarParser::T__39:
      case TILGrammarParser::T__40:
      case TILGrammarParser::T__41:
      case TILGrammarParser::T__42:
      case TILGrammarParser::T__43:
      case TILGrammarParser::T__44:
      case TILGrammarParser::T__45:
      case TILGrammarParser::T__46: {
        enterOuterAlt(_localctx, 1);
        setState(253);
        keyword();
        break;
      }

      case TILGrammarParser::T__52:
      case TILGrammarParser::UPPERCASE_LETTER: {
        enterOuterAlt(_localctx, 2);
        setState(254);
        entity_name();
        break;
      }

      case TILGrammarParser::ZERO:
      case TILGrammarParser::NONZERO_DIGIT: {
        enterOuterAlt(_localctx, 3);
        setState(255);
        number();
        break;
      }

      case TILGrammarParser::T__4:
      case TILGrammarParser::T__15:
      case TILGrammarParser::T__47:
      case TILGrammarParser::T__48:
      case TILGrammarParser::T__49: {
        enterOuterAlt(_localctx, 4);
        setState(256);
        symbols();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Type_nameContext ------------------------------------------------------------------

TILGrammarParser::Type_nameContext::Type_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::Upperletter_nameContext* TILGrammarParser::Type_nameContext::upperletter_name() {
  return getRuleContext<TILGrammarParser::Upperletter_nameContext>(0);
}


size_t TILGrammarParser::Type_nameContext::getRuleIndex() const {
  return TILGrammarParser::RuleType_name;
}

void TILGrammarParser::Type_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterType_name(this);
}

void TILGrammarParser::Type_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitType_name(this);
}

TILGrammarParser::Type_nameContext* TILGrammarParser::type_name() {
  Type_nameContext *_localctx = _tracker.createInstance<Type_nameContext>(_ctx, getState());
  enterRule(_localctx, 46, TILGrammarParser::RuleType_name);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(259);
    upperletter_name();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Entity_nameContext ------------------------------------------------------------------

TILGrammarParser::Entity_nameContext::Entity_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::Upperletter_nameContext* TILGrammarParser::Entity_nameContext::upperletter_name() {
  return getRuleContext<TILGrammarParser::Upperletter_nameContext>(0);
}

TILGrammarParser::Quoted_nameContext* TILGrammarParser::Entity_nameContext::quoted_name() {
  return getRuleContext<TILGrammarParser::Quoted_nameContext>(0);
}


size_t TILGrammarParser::Entity_nameContext::getRuleIndex() const {
  return TILGrammarParser::RuleEntity_name;
}

void TILGrammarParser::Entity_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterEntity_name(this);
}

void TILGrammarParser::Entity_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitEntity_name(this);
}

TILGrammarParser::Entity_nameContext* TILGrammarParser::entity_name() {
  Entity_nameContext *_localctx = _tracker.createInstance<Entity_nameContext>(_ctx, getState());
  enterRule(_localctx, 48, TILGrammarParser::RuleEntity_name);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(263);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case TILGrammarParser::UPPERCASE_LETTER: {
        setState(261);
        upperletter_name();
        break;
      }

      case TILGrammarParser::T__52: {
        setState(262);
        quoted_name();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Variable_nameContext ------------------------------------------------------------------

TILGrammarParser::Variable_nameContext::Variable_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

TILGrammarParser::Lowerletter_nameContext* TILGrammarParser::Variable_nameContext::lowerletter_name() {
  return getRuleContext<TILGrammarParser::Lowerletter_nameContext>(0);
}


size_t TILGrammarParser::Variable_nameContext::getRuleIndex() const {
  return TILGrammarParser::RuleVariable_name;
}

void TILGrammarParser::Variable_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterVariable_name(this);
}

void TILGrammarParser::Variable_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitVariable_name(this);
}

TILGrammarParser::Variable_nameContext* TILGrammarParser::variable_name() {
  Variable_nameContext *_localctx = _tracker.createInstance<Variable_nameContext>(_ctx, getState());
  enterRule(_localctx, 50, TILGrammarParser::RuleVariable_name);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(265);
    lowerletter_name();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- KeywordContext ------------------------------------------------------------------

TILGrammarParser::KeywordContext::KeywordContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t TILGrammarParser::KeywordContext::getRuleIndex() const {
  return TILGrammarParser::RuleKeyword;
}

void TILGrammarParser::KeywordContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterKeyword(this);
}

void TILGrammarParser::KeywordContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitKeyword(this);
}

TILGrammarParser::KeywordContext* TILGrammarParser::keyword() {
  KeywordContext *_localctx = _tracker.createInstance<KeywordContext>(_ctx, getState());
  enterRule(_localctx, 52, TILGrammarParser::RuleKeyword);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(267);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << TILGrammarParser::T__26)
      | (1ULL << TILGrammarParser::T__27)
      | (1ULL << TILGrammarParser::T__28)
      | (1ULL << TILGrammarParser::T__29)
      | (1ULL << TILGrammarParser::T__30)
      | (1ULL << TILGrammarParser::T__31)
      | (1ULL << TILGrammarParser::T__32)
      | (1ULL << TILGrammarParser::T__33)
      | (1ULL << TILGrammarParser::T__34)
      | (1ULL << TILGrammarParser::T__35)
      | (1ULL << TILGrammarParser::T__36)
      | (1ULL << TILGrammarParser::T__37)
      | (1ULL << TILGrammarParser::T__38)
      | (1ULL << TILGrammarParser::T__39)
      | (1ULL << TILGrammarParser::T__40)
      | (1ULL << TILGrammarParser::T__41)
      | (1ULL << TILGrammarParser::T__42)
      | (1ULL << TILGrammarParser::T__43)
      | (1ULL << TILGrammarParser::T__44)
      | (1ULL << TILGrammarParser::T__45)
      | (1ULL << TILGrammarParser::T__46))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SymbolsContext ------------------------------------------------------------------

TILGrammarParser::SymbolsContext::SymbolsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t TILGrammarParser::SymbolsContext::getRuleIndex() const {
  return TILGrammarParser::RuleSymbols;
}

void TILGrammarParser::SymbolsContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterSymbols(this);
}

void TILGrammarParser::SymbolsContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitSymbols(this);
}

TILGrammarParser::SymbolsContext* TILGrammarParser::symbols() {
  SymbolsContext *_localctx = _tracker.createInstance<SymbolsContext>(_ctx, getState());
  enterRule(_localctx, 54, TILGrammarParser::RuleSymbols);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(269);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << TILGrammarParser::T__4)
      | (1ULL << TILGrammarParser::T__15)
      | (1ULL << TILGrammarParser::T__47)
      | (1ULL << TILGrammarParser::T__48)
      | (1ULL << TILGrammarParser::T__49))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AnythingContext ------------------------------------------------------------------

TILGrammarParser::AnythingContext::AnythingContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> TILGrammarParser::AnythingContext::ZERO() {
  return getTokens(TILGrammarParser::ZERO);
}

tree::TerminalNode* TILGrammarParser::AnythingContext::ZERO(size_t i) {
  return getToken(TILGrammarParser::ZERO, i);
}

std::vector<tree::TerminalNode *> TILGrammarParser::AnythingContext::NONZERO_DIGIT() {
  return getTokens(TILGrammarParser::NONZERO_DIGIT);
}

tree::TerminalNode* TILGrammarParser::AnythingContext::NONZERO_DIGIT(size_t i) {
  return getToken(TILGrammarParser::NONZERO_DIGIT, i);
}


size_t TILGrammarParser::AnythingContext::getRuleIndex() const {
  return TILGrammarParser::RuleAnything;
}

void TILGrammarParser::AnythingContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterAnything(this);
}

void TILGrammarParser::AnythingContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitAnything(this);
}

TILGrammarParser::AnythingContext* TILGrammarParser::anything() {
  AnythingContext *_localctx = _tracker.createInstance<AnythingContext>(_ctx, getState());
  enterRule(_localctx, 56, TILGrammarParser::RuleAnything);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(271);
    match(TILGrammarParser::T__50);
    setState(275);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == TILGrammarParser::ZERO

    || _la == TILGrammarParser::NONZERO_DIGIT) {
      setState(272);
      _la = _input->LA(1);
      if (!(_la == TILGrammarParser::ZERO

      || _la == TILGrammarParser::NONZERO_DIGIT)) {
      _errHandler->recoverInline(this);
      }
      else {
        _errHandler->reportMatch(this);
        consume();
      }
      setState(277);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NumberContext ------------------------------------------------------------------

TILGrammarParser::NumberContext::NumberContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> TILGrammarParser::NumberContext::ZERO() {
  return getTokens(TILGrammarParser::ZERO);
}

tree::TerminalNode* TILGrammarParser::NumberContext::ZERO(size_t i) {
  return getToken(TILGrammarParser::ZERO, i);
}

std::vector<tree::TerminalNode *> TILGrammarParser::NumberContext::NONZERO_DIGIT() {
  return getTokens(TILGrammarParser::NONZERO_DIGIT);
}

tree::TerminalNode* TILGrammarParser::NumberContext::NONZERO_DIGIT(size_t i) {
  return getToken(TILGrammarParser::NONZERO_DIGIT, i);
}


size_t TILGrammarParser::NumberContext::getRuleIndex() const {
  return TILGrammarParser::RuleNumber;
}

void TILGrammarParser::NumberContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterNumber(this);
}

void TILGrammarParser::NumberContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitNumber(this);
}

TILGrammarParser::NumberContext* TILGrammarParser::number() {
  NumberContext *_localctx = _tracker.createInstance<NumberContext>(_ctx, getState());
  enterRule(_localctx, 58, TILGrammarParser::RuleNumber);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(278);
    _la = _input->LA(1);
    if (!(_la == TILGrammarParser::ZERO

    || _la == TILGrammarParser::NONZERO_DIGIT)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
    setState(282);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == TILGrammarParser::ZERO

    || _la == TILGrammarParser::NONZERO_DIGIT) {
      setState(279);
      _la = _input->LA(1);
      if (!(_la == TILGrammarParser::ZERO

      || _la == TILGrammarParser::NONZERO_DIGIT)) {
      _errHandler->recoverInline(this);
      }
      else {
        _errHandler->reportMatch(this);
        consume();
      }
      setState(284);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(293);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 20, _ctx)) {
    case 1: {
      setState(285);
      match(TILGrammarParser::T__0);
      setState(286);
      _la = _input->LA(1);
      if (!(_la == TILGrammarParser::ZERO

      || _la == TILGrammarParser::NONZERO_DIGIT)) {
      _errHandler->recoverInline(this);
      }
      else {
        _errHandler->reportMatch(this);
        consume();
      }
      setState(290);
      _errHandler->sync(this);
      _la = _input->LA(1);
      while (_la == TILGrammarParser::ZERO

      || _la == TILGrammarParser::NONZERO_DIGIT) {
        setState(287);
        _la = _input->LA(1);
        if (!(_la == TILGrammarParser::ZERO

        || _la == TILGrammarParser::NONZERO_DIGIT)) {
        _errHandler->recoverInline(this);
        }
        else {
          _errHandler->reportMatch(this);
          consume();
        }
        setState(292);
        _errHandler->sync(this);
        _la = _input->LA(1);
      }
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Upperletter_nameContext ------------------------------------------------------------------

TILGrammarParser::Upperletter_nameContext::Upperletter_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> TILGrammarParser::Upperletter_nameContext::UPPERCASE_LETTER() {
  return getTokens(TILGrammarParser::UPPERCASE_LETTER);
}

tree::TerminalNode* TILGrammarParser::Upperletter_nameContext::UPPERCASE_LETTER(size_t i) {
  return getToken(TILGrammarParser::UPPERCASE_LETTER, i);
}

std::vector<tree::TerminalNode *> TILGrammarParser::Upperletter_nameContext::LOWERCASE_LETTER() {
  return getTokens(TILGrammarParser::LOWERCASE_LETTER);
}

tree::TerminalNode* TILGrammarParser::Upperletter_nameContext::LOWERCASE_LETTER(size_t i) {
  return getToken(TILGrammarParser::LOWERCASE_LETTER, i);
}

std::vector<tree::TerminalNode *> TILGrammarParser::Upperletter_nameContext::ZERO() {
  return getTokens(TILGrammarParser::ZERO);
}

tree::TerminalNode* TILGrammarParser::Upperletter_nameContext::ZERO(size_t i) {
  return getToken(TILGrammarParser::ZERO, i);
}

std::vector<tree::TerminalNode *> TILGrammarParser::Upperletter_nameContext::NONZERO_DIGIT() {
  return getTokens(TILGrammarParser::NONZERO_DIGIT);
}

tree::TerminalNode* TILGrammarParser::Upperletter_nameContext::NONZERO_DIGIT(size_t i) {
  return getToken(TILGrammarParser::NONZERO_DIGIT, i);
}


size_t TILGrammarParser::Upperletter_nameContext::getRuleIndex() const {
  return TILGrammarParser::RuleUpperletter_name;
}

void TILGrammarParser::Upperletter_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterUpperletter_name(this);
}

void TILGrammarParser::Upperletter_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitUpperletter_name(this);
}

TILGrammarParser::Upperletter_nameContext* TILGrammarParser::upperletter_name() {
  Upperletter_nameContext *_localctx = _tracker.createInstance<Upperletter_nameContext>(_ctx, getState());
  enterRule(_localctx, 60, TILGrammarParser::RuleUpperletter_name);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(295);
    match(TILGrammarParser::UPPERCASE_LETTER);
    setState(299);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 21, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(296);
        _la = _input->LA(1);
        if (!((((_la & ~ 0x3fULL) == 0) &&
          ((1ULL << _la) & ((1ULL << TILGrammarParser::T__51)
          | (1ULL << TILGrammarParser::LOWERCASE_LETTER)
          | (1ULL << TILGrammarParser::UPPERCASE_LETTER)
          | (1ULL << TILGrammarParser::ZERO)
          | (1ULL << TILGrammarParser::NONZERO_DIGIT))) != 0))) {
        _errHandler->recoverInline(this);
        }
        else {
          _errHandler->reportMatch(this);
          consume();
        } 
      }
      setState(301);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 21, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Lowerletter_nameContext ------------------------------------------------------------------

TILGrammarParser::Lowerletter_nameContext::Lowerletter_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> TILGrammarParser::Lowerletter_nameContext::LOWERCASE_LETTER() {
  return getTokens(TILGrammarParser::LOWERCASE_LETTER);
}

tree::TerminalNode* TILGrammarParser::Lowerletter_nameContext::LOWERCASE_LETTER(size_t i) {
  return getToken(TILGrammarParser::LOWERCASE_LETTER, i);
}

std::vector<tree::TerminalNode *> TILGrammarParser::Lowerletter_nameContext::UPPERCASE_LETTER() {
  return getTokens(TILGrammarParser::UPPERCASE_LETTER);
}

tree::TerminalNode* TILGrammarParser::Lowerletter_nameContext::UPPERCASE_LETTER(size_t i) {
  return getToken(TILGrammarParser::UPPERCASE_LETTER, i);
}

std::vector<tree::TerminalNode *> TILGrammarParser::Lowerletter_nameContext::ZERO() {
  return getTokens(TILGrammarParser::ZERO);
}

tree::TerminalNode* TILGrammarParser::Lowerletter_nameContext::ZERO(size_t i) {
  return getToken(TILGrammarParser::ZERO, i);
}

std::vector<tree::TerminalNode *> TILGrammarParser::Lowerletter_nameContext::NONZERO_DIGIT() {
  return getTokens(TILGrammarParser::NONZERO_DIGIT);
}

tree::TerminalNode* TILGrammarParser::Lowerletter_nameContext::NONZERO_DIGIT(size_t i) {
  return getToken(TILGrammarParser::NONZERO_DIGIT, i);
}


size_t TILGrammarParser::Lowerletter_nameContext::getRuleIndex() const {
  return TILGrammarParser::RuleLowerletter_name;
}

void TILGrammarParser::Lowerletter_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLowerletter_name(this);
}

void TILGrammarParser::Lowerletter_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLowerletter_name(this);
}

TILGrammarParser::Lowerletter_nameContext* TILGrammarParser::lowerletter_name() {
  Lowerletter_nameContext *_localctx = _tracker.createInstance<Lowerletter_nameContext>(_ctx, getState());
  enterRule(_localctx, 62, TILGrammarParser::RuleLowerletter_name);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(302);
    match(TILGrammarParser::LOWERCASE_LETTER);
    setState(306);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 22, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(303);
        _la = _input->LA(1);
        if (!((((_la & ~ 0x3fULL) == 0) &&
          ((1ULL << _la) & ((1ULL << TILGrammarParser::T__51)
          | (1ULL << TILGrammarParser::LOWERCASE_LETTER)
          | (1ULL << TILGrammarParser::UPPERCASE_LETTER)
          | (1ULL << TILGrammarParser::ZERO)
          | (1ULL << TILGrammarParser::NONZERO_DIGIT))) != 0))) {
        _errHandler->recoverInline(this);
        }
        else {
          _errHandler->reportMatch(this);
          consume();
        } 
      }
      setState(308);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 22, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Quoted_nameContext ------------------------------------------------------------------

TILGrammarParser::Quoted_nameContext::Quoted_nameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t TILGrammarParser::Quoted_nameContext::getRuleIndex() const {
  return TILGrammarParser::RuleQuoted_name;
}

void TILGrammarParser::Quoted_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterQuoted_name(this);
}

void TILGrammarParser::Quoted_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitQuoted_name(this);
}

TILGrammarParser::Quoted_nameContext* TILGrammarParser::quoted_name() {
  Quoted_nameContext *_localctx = _tracker.createInstance<Quoted_nameContext>(_ctx, getState());
  enterRule(_localctx, 64, TILGrammarParser::RuleQuoted_name);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(309);
    match(TILGrammarParser::T__52);
    setState(313);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 23, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(310);
        match(TILGrammarParser::T__52); 
      }
      setState(315);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 23, _ctx);
    }
    setState(316);
    match(TILGrammarParser::T__52);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WhitespaceContext ------------------------------------------------------------------

TILGrammarParser::WhitespaceContext::WhitespaceContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* TILGrammarParser::WhitespaceContext::WHITESPACE_CHARACTER() {
  return getToken(TILGrammarParser::WHITESPACE_CHARACTER, 0);
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::WhitespaceContext::optional_whitespace() {
  return getRuleContext<TILGrammarParser::Optional_whitespaceContext>(0);
}


size_t TILGrammarParser::WhitespaceContext::getRuleIndex() const {
  return TILGrammarParser::RuleWhitespace;
}

void TILGrammarParser::WhitespaceContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterWhitespace(this);
}

void TILGrammarParser::WhitespaceContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitWhitespace(this);
}

TILGrammarParser::WhitespaceContext* TILGrammarParser::whitespace() {
  WhitespaceContext *_localctx = _tracker.createInstance<WhitespaceContext>(_ctx, getState());
  enterRule(_localctx, 66, TILGrammarParser::RuleWhitespace);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(318);
    match(TILGrammarParser::WHITESPACE_CHARACTER);
    setState(319);
    optional_whitespace();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Optional_whitespaceContext ------------------------------------------------------------------

TILGrammarParser::Optional_whitespaceContext::Optional_whitespaceContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> TILGrammarParser::Optional_whitespaceContext::WHITESPACE_CHARACTER() {
  return getTokens(TILGrammarParser::WHITESPACE_CHARACTER);
}

tree::TerminalNode* TILGrammarParser::Optional_whitespaceContext::WHITESPACE_CHARACTER(size_t i) {
  return getToken(TILGrammarParser::WHITESPACE_CHARACTER, i);
}


size_t TILGrammarParser::Optional_whitespaceContext::getRuleIndex() const {
  return TILGrammarParser::RuleOptional_whitespace;
}

void TILGrammarParser::Optional_whitespaceContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterOptional_whitespace(this);
}

void TILGrammarParser::Optional_whitespaceContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<TILGrammarListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitOptional_whitespace(this);
}

TILGrammarParser::Optional_whitespaceContext* TILGrammarParser::optional_whitespace() {
  Optional_whitespaceContext *_localctx = _tracker.createInstance<Optional_whitespaceContext>(_ctx, getState());
  enterRule(_localctx, 68, TILGrammarParser::RuleOptional_whitespace);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(324);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 24, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(321);
        match(TILGrammarParser::WHITESPACE_CHARACTER); 
      }
      setState(326);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 24, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> TILGrammarParser::_decisionToDFA;
atn::PredictionContextCache TILGrammarParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN TILGrammarParser::_atn;
std::vector<uint16_t> TILGrammarParser::_serializedATN;

std::vector<std::string> TILGrammarParser::_ruleNames = {
  "start", "sentence", "sentence_content", "termination", "type_definition", 
  "entity_definition", "construction", "global_variable_definition", "data_type", 
  "embeded_type", "list_type", "touple_type", "user_type", "enclosed_data_type", 
  "variable", "trivialisation", "composition", "closure", "lambda_variables", 
  "typed_variables", "typed_variable", "n_execution", "entity", "type_name", 
  "entity_name", "variable_name", "keyword", "symbols", "anything", "number", 
  "upperletter_name", "lowerletter_name", "quoted_name", "whitespace", "optional_whitespace"
};

std::vector<std::string> TILGrammarParser::_literalNames = {
  "", "'.'", "'TypeDef'", "':='", "','", "'/'", "'@wt'", "'->'", "'@tw'", 
  "'Bool'", "'Indiv'", "'Time'", "'String'", "'World'", "'Real'", "'Int'", 
  "'*'", "'List'", "'('", "')'", "'Tuple'", "'''", "'['", "']'", "'\\'", 
  "':'", "'^'", "'ForAll'", "'Exist'", "'Every'", "'Some'", "'No'", "'True'", 
  "'False'", "'And'", "'Or'", "'Not'", "'Implies'", "'Sing'", "'Sub'", "'Tr'", 
  "'TrueC'", "'FalseC'", "'ImproperC'", "'TrueP'", "'FalseP'", "'UndefP'", 
  "'ToInt'", "'+'", "'-'", "'='", "'Any'", "'_'", "'%'", "", "", "'0'"
};

std::vector<std::string> TILGrammarParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "LOWERCASE_LETTER", "UPPERCASE_LETTER", "ZERO", "NONZERO_DIGIT", "WHITESPACE_CHARACTER"
};

dfa::Vocabulary TILGrammarParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> TILGrammarParser::_tokenNames;

TILGrammarParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  static const uint16_t serializedATNSegment0[] = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
       0x3, 0x3c, 0x14a, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 
       0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 
       0x7, 0x4, 0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 
       0x4, 0xb, 0x9, 0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 
       0xe, 0x9, 0xe, 0x4, 0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 
       0x9, 0x11, 0x4, 0x12, 0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x4, 0x14, 
       0x9, 0x14, 0x4, 0x15, 0x9, 0x15, 0x4, 0x16, 0x9, 0x16, 0x4, 0x17, 
       0x9, 0x17, 0x4, 0x18, 0x9, 0x18, 0x4, 0x19, 0x9, 0x19, 0x4, 0x1a, 
       0x9, 0x1a, 0x4, 0x1b, 0x9, 0x1b, 0x4, 0x1c, 0x9, 0x1c, 0x4, 0x1d, 
       0x9, 0x1d, 0x4, 0x1e, 0x9, 0x1e, 0x4, 0x1f, 0x9, 0x1f, 0x4, 0x20, 
       0x9, 0x20, 0x4, 0x21, 0x9, 0x21, 0x4, 0x22, 0x9, 0x22, 0x4, 0x23, 
       0x9, 0x23, 0x4, 0x24, 0x9, 0x24, 0x3, 0x2, 0x7, 0x2, 0x4a, 0xa, 0x2, 
       0xc, 0x2, 0xe, 0x2, 0x4d, 0xb, 0x2, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
       0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x5, 0x4, 0x56, 0xa, 0x4, 
       0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x6, 0x3, 0x6, 0x3, 
       0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x7, 
       0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x7, 0x7, 0x6a, 
       0xa, 0x7, 0xc, 0x7, 0xe, 0x7, 0x6d, 0xb, 0x7, 0x3, 0x7, 0x3, 0x7, 
       0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 
       0x8, 0x3, 0x8, 0x5, 0x8, 0x79, 0xa, 0x8, 0x3, 0x8, 0x5, 0x8, 0x7c, 
       0xa, 0x8, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 
       0x9, 0x7, 0x9, 0x84, 0xa, 0x9, 0xc, 0x9, 0xe, 0x9, 0x87, 0xb, 0x9, 
       0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0xa, 0x3, 
       0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x5, 0xa, 0x93, 0xa, 0xa, 0x3, 
       0xa, 0x5, 0xa, 0x96, 0xa, 0xa, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x3, 
       0xb, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x5, 0xb, 
       0xa1, 0xa, 0xb, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 
       0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 
       0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xd, 0x3, 0xe, 0x3, 0xe, 
       0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x7, 
       0xf, 0xbb, 0xa, 0xf, 0xc, 0xf, 0xe, 0xf, 0xbe, 0xb, 0xf, 0x3, 0xf, 
       0x3, 0xf, 0x3, 0xf, 0x3, 0x10, 0x3, 0x10, 0x3, 0x11, 0x3, 0x11, 0x3, 
       0x11, 0x3, 0x11, 0x5, 0x11, 0xc9, 0xa, 0x11, 0x3, 0x12, 0x3, 0x12, 
       0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x7, 0x12, 
       0xd2, 0xa, 0x12, 0xc, 0x12, 0xe, 0x12, 0xd5, 0xb, 0x12, 0x3, 0x12, 
       0x3, 0x12, 0x3, 0x12, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 
       0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x14, 0x3, 0x14, 
       0x3, 0x14, 0x3, 0x14, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 
       0x3, 0x15, 0x7, 0x15, 0xeb, 0xa, 0x15, 0xc, 0x15, 0xe, 0x15, 0xee, 
       0xb, 0x15, 0x3, 0x16, 0x3, 0x16, 0x3, 0x16, 0x3, 0x16, 0x3, 0x16, 
       0x3, 0x16, 0x5, 0x16, 0xf6, 0xa, 0x16, 0x3, 0x17, 0x3, 0x17, 0x3, 
       0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x5, 0x17, 0xfe, 0xa, 0x17, 
       0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x5, 0x18, 0x104, 0xa, 
       0x18, 0x3, 0x19, 0x3, 0x19, 0x3, 0x1a, 0x3, 0x1a, 0x5, 0x1a, 0x10a, 
       0xa, 0x1a, 0x3, 0x1b, 0x3, 0x1b, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1d, 
       0x3, 0x1d, 0x3, 0x1e, 0x3, 0x1e, 0x7, 0x1e, 0x114, 0xa, 0x1e, 0xc, 
       0x1e, 0xe, 0x1e, 0x117, 0xb, 0x1e, 0x3, 0x1f, 0x3, 0x1f, 0x7, 0x1f, 
       0x11b, 0xa, 0x1f, 0xc, 0x1f, 0xe, 0x1f, 0x11e, 0xb, 0x1f, 0x3, 0x1f, 
       0x3, 0x1f, 0x3, 0x1f, 0x7, 0x1f, 0x123, 0xa, 0x1f, 0xc, 0x1f, 0xe, 
       0x1f, 0x126, 0xb, 0x1f, 0x5, 0x1f, 0x128, 0xa, 0x1f, 0x3, 0x20, 0x3, 
       0x20, 0x7, 0x20, 0x12c, 0xa, 0x20, 0xc, 0x20, 0xe, 0x20, 0x12f, 0xb, 
       0x20, 0x3, 0x21, 0x3, 0x21, 0x7, 0x21, 0x133, 0xa, 0x21, 0xc, 0x21, 
       0xe, 0x21, 0x136, 0xb, 0x21, 0x3, 0x22, 0x3, 0x22, 0x7, 0x22, 0x13a, 
       0xa, 0x22, 0xc, 0x22, 0xe, 0x22, 0x13d, 0xb, 0x22, 0x3, 0x22, 0x3, 
       0x22, 0x3, 0x23, 0x3, 0x23, 0x3, 0x23, 0x3, 0x24, 0x7, 0x24, 0x145, 
       0xa, 0x24, 0xc, 0x24, 0xe, 0x24, 0x148, 0xb, 0x24, 0x3, 0x24, 0x2, 
       0x2, 0x25, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x10, 0x12, 0x14, 0x16, 
       0x18, 0x1a, 0x1c, 0x1e, 0x20, 0x22, 0x24, 0x26, 0x28, 0x2a, 0x2c, 
       0x2e, 0x30, 0x32, 0x34, 0x36, 0x38, 0x3a, 0x3c, 0x3e, 0x40, 0x42, 
       0x44, 0x46, 0x2, 0x6, 0x3, 0x2, 0x1d, 0x31, 0x5, 0x2, 0x7, 0x7, 0x12, 
       0x12, 0x32, 0x34, 0x3, 0x2, 0x3a, 0x3b, 0x4, 0x2, 0x36, 0x36, 0x38, 
       0x3b, 0x2, 0x150, 0x2, 0x4b, 0x3, 0x2, 0x2, 0x2, 0x4, 0x4e, 0x3, 
       0x2, 0x2, 0x2, 0x6, 0x55, 0x3, 0x2, 0x2, 0x2, 0x8, 0x57, 0x3, 0x2, 
       0x2, 0x2, 0xa, 0x5b, 0x3, 0x2, 0x2, 0x2, 0xc, 0x63, 0x3, 0x2, 0x2, 
       0x2, 0xe, 0x78, 0x3, 0x2, 0x2, 0x2, 0x10, 0x7d, 0x3, 0x2, 0x2, 0x2, 
       0x12, 0x92, 0x3, 0x2, 0x2, 0x2, 0x14, 0xa0, 0x3, 0x2, 0x2, 0x2, 0x16, 
       0xa2, 0x3, 0x2, 0x2, 0x2, 0x18, 0xaa, 0x3, 0x2, 0x2, 0x2, 0x1a, 0xb2, 
       0x3, 0x2, 0x2, 0x2, 0x1c, 0xb4, 0x3, 0x2, 0x2, 0x2, 0x1e, 0xc2, 0x3, 
       0x2, 0x2, 0x2, 0x20, 0xc4, 0x3, 0x2, 0x2, 0x2, 0x22, 0xca, 0x3, 0x2, 
       0x2, 0x2, 0x24, 0xd9, 0x3, 0x2, 0x2, 0x2, 0x26, 0xe1, 0x3, 0x2, 0x2, 
       0x2, 0x28, 0xe5, 0x3, 0x2, 0x2, 0x2, 0x2a, 0xef, 0x3, 0x2, 0x2, 0x2, 
       0x2c, 0xf7, 0x3, 0x2, 0x2, 0x2, 0x2e, 0x103, 0x3, 0x2, 0x2, 0x2, 
       0x30, 0x105, 0x3, 0x2, 0x2, 0x2, 0x32, 0x109, 0x3, 0x2, 0x2, 0x2, 
       0x34, 0x10b, 0x3, 0x2, 0x2, 0x2, 0x36, 0x10d, 0x3, 0x2, 0x2, 0x2, 
       0x38, 0x10f, 0x3, 0x2, 0x2, 0x2, 0x3a, 0x111, 0x3, 0x2, 0x2, 0x2, 
       0x3c, 0x118, 0x3, 0x2, 0x2, 0x2, 0x3e, 0x129, 0x3, 0x2, 0x2, 0x2, 
       0x40, 0x130, 0x3, 0x2, 0x2, 0x2, 0x42, 0x137, 0x3, 0x2, 0x2, 0x2, 
       0x44, 0x140, 0x3, 0x2, 0x2, 0x2, 0x46, 0x146, 0x3, 0x2, 0x2, 0x2, 
       0x48, 0x4a, 0x5, 0x4, 0x3, 0x2, 0x49, 0x48, 0x3, 0x2, 0x2, 0x2, 0x4a, 
       0x4d, 0x3, 0x2, 0x2, 0x2, 0x4b, 0x49, 0x3, 0x2, 0x2, 0x2, 0x4b, 0x4c, 
       0x3, 0x2, 0x2, 0x2, 0x4c, 0x3, 0x3, 0x2, 0x2, 0x2, 0x4d, 0x4b, 0x3, 
       0x2, 0x2, 0x2, 0x4e, 0x4f, 0x5, 0x6, 0x4, 0x2, 0x4f, 0x50, 0x5, 0x8, 
       0x5, 0x2, 0x50, 0x5, 0x3, 0x2, 0x2, 0x2, 0x51, 0x56, 0x5, 0xa, 0x6, 
       0x2, 0x52, 0x56, 0x5, 0xc, 0x7, 0x2, 0x53, 0x56, 0x5, 0xe, 0x8, 0x2, 
       0x54, 0x56, 0x5, 0x10, 0x9, 0x2, 0x55, 0x51, 0x3, 0x2, 0x2, 0x2, 
       0x55, 0x52, 0x3, 0x2, 0x2, 0x2, 0x55, 0x53, 0x3, 0x2, 0x2, 0x2, 0x55, 
       0x54, 0x3, 0x2, 0x2, 0x2, 0x56, 0x7, 0x3, 0x2, 0x2, 0x2, 0x57, 0x58, 
       0x5, 0x46, 0x24, 0x2, 0x58, 0x59, 0x7, 0x3, 0x2, 0x2, 0x59, 0x5a, 
       0x5, 0x46, 0x24, 0x2, 0x5a, 0x9, 0x3, 0x2, 0x2, 0x2, 0x5b, 0x5c, 
       0x7, 0x4, 0x2, 0x2, 0x5c, 0x5d, 0x5, 0x44, 0x23, 0x2, 0x5d, 0x5e, 
       0x5, 0x30, 0x19, 0x2, 0x5e, 0x5f, 0x5, 0x46, 0x24, 0x2, 0x5f, 0x60, 
       0x7, 0x5, 0x2, 0x2, 0x60, 0x61, 0x5, 0x46, 0x24, 0x2, 0x61, 0x62, 
       0x5, 0x12, 0xa, 0x2, 0x62, 0xb, 0x3, 0x2, 0x2, 0x2, 0x63, 0x6b, 0x5, 
       0x32, 0x1a, 0x2, 0x64, 0x65, 0x5, 0x46, 0x24, 0x2, 0x65, 0x66, 0x7, 
       0x6, 0x2, 0x2, 0x66, 0x67, 0x5, 0x46, 0x24, 0x2, 0x67, 0x68, 0x5, 
       0x32, 0x1a, 0x2, 0x68, 0x6a, 0x3, 0x2, 0x2, 0x2, 0x69, 0x64, 0x3, 
       0x2, 0x2, 0x2, 0x6a, 0x6d, 0x3, 0x2, 0x2, 0x2, 0x6b, 0x69, 0x3, 0x2, 
       0x2, 0x2, 0x6b, 0x6c, 0x3, 0x2, 0x2, 0x2, 0x6c, 0x6e, 0x3, 0x2, 0x2, 
       0x2, 0x6d, 0x6b, 0x3, 0x2, 0x2, 0x2, 0x6e, 0x6f, 0x5, 0x46, 0x24, 
       0x2, 0x6f, 0x70, 0x7, 0x7, 0x2, 0x2, 0x70, 0x71, 0x5, 0x46, 0x24, 
       0x2, 0x71, 0x72, 0x5, 0x12, 0xa, 0x2, 0x72, 0xd, 0x3, 0x2, 0x2, 0x2, 
       0x73, 0x79, 0x5, 0x20, 0x11, 0x2, 0x74, 0x79, 0x5, 0x1e, 0x10, 0x2, 
       0x75, 0x79, 0x5, 0x24, 0x13, 0x2, 0x76, 0x79, 0x5, 0x2c, 0x17, 0x2, 
       0x77, 0x79, 0x5, 0x22, 0x12, 0x2, 0x78, 0x73, 0x3, 0x2, 0x2, 0x2, 
       0x78, 0x74, 0x3, 0x2, 0x2, 0x2, 0x78, 0x75, 0x3, 0x2, 0x2, 0x2, 0x78, 
       0x76, 0x3, 0x2, 0x2, 0x2, 0x78, 0x77, 0x3, 0x2, 0x2, 0x2, 0x79, 0x7b, 
       0x3, 0x2, 0x2, 0x2, 0x7a, 0x7c, 0x7, 0x8, 0x2, 0x2, 0x7b, 0x7a, 0x3, 
       0x2, 0x2, 0x2, 0x7b, 0x7c, 0x3, 0x2, 0x2, 0x2, 0x7c, 0xf, 0x3, 0x2, 
       0x2, 0x2, 0x7d, 0x85, 0x5, 0x34, 0x1b, 0x2, 0x7e, 0x7f, 0x5, 0x46, 
       0x24, 0x2, 0x7f, 0x80, 0x7, 0x6, 0x2, 0x2, 0x80, 0x81, 0x5, 0x46, 
       0x24, 0x2, 0x81, 0x82, 0x5, 0x34, 0x1b, 0x2, 0x82, 0x84, 0x3, 0x2, 
       0x2, 0x2, 0x83, 0x7e, 0x3, 0x2, 0x2, 0x2, 0x84, 0x87, 0x3, 0x2, 0x2, 
       0x2, 0x85, 0x83, 0x3, 0x2, 0x2, 0x2, 0x85, 0x86, 0x3, 0x2, 0x2, 0x2, 
       0x86, 0x88, 0x3, 0x2, 0x2, 0x2, 0x87, 0x85, 0x3, 0x2, 0x2, 0x2, 0x88, 
       0x89, 0x5, 0x46, 0x24, 0x2, 0x89, 0x8a, 0x7, 0x9, 0x2, 0x2, 0x8a, 
       0x8b, 0x5, 0x46, 0x24, 0x2, 0x8b, 0x8c, 0x5, 0x12, 0xa, 0x2, 0x8c, 
       0x11, 0x3, 0x2, 0x2, 0x2, 0x8d, 0x93, 0x5, 0x14, 0xb, 0x2, 0x8e, 
       0x93, 0x5, 0x16, 0xc, 0x2, 0x8f, 0x93, 0x5, 0x18, 0xd, 0x2, 0x90, 
       0x93, 0x5, 0x1a, 0xe, 0x2, 0x91, 0x93, 0x5, 0x1c, 0xf, 0x2, 0x92, 
       0x8d, 0x3, 0x2, 0x2, 0x2, 0x92, 0x8e, 0x3, 0x2, 0x2, 0x2, 0x92, 0x8f, 
       0x3, 0x2, 0x2, 0x2, 0x92, 0x90, 0x3, 0x2, 0x2, 0x2, 0x92, 0x91, 0x3, 
       0x2, 0x2, 0x2, 0x93, 0x95, 0x3, 0x2, 0x2, 0x2, 0x94, 0x96, 0x7, 0xa, 
       0x2, 0x2, 0x95, 0x94, 0x3, 0x2, 0x2, 0x2, 0x95, 0x96, 0x3, 0x2, 0x2, 
       0x2, 0x96, 0x13, 0x3, 0x2, 0x2, 0x2, 0x97, 0xa1, 0x7, 0xb, 0x2, 0x2, 
       0x98, 0xa1, 0x7, 0xc, 0x2, 0x2, 0x99, 0xa1, 0x7, 0xd, 0x2, 0x2, 0x9a, 
       0xa1, 0x7, 0xe, 0x2, 0x2, 0x9b, 0xa1, 0x7, 0xf, 0x2, 0x2, 0x9c, 0xa1, 
       0x7, 0x10, 0x2, 0x2, 0x9d, 0xa1, 0x7, 0x11, 0x2, 0x2, 0x9e, 0xa1, 
       0x5, 0x3a, 0x1e, 0x2, 0x9f, 0xa1, 0x7, 0x12, 0x2, 0x2, 0xa0, 0x97, 
       0x3, 0x2, 0x2, 0x2, 0xa0, 0x98, 0x3, 0x2, 0x2, 0x2, 0xa0, 0x99, 0x3, 
       0x2, 0x2, 0x2, 0xa0, 0x9a, 0x3, 0x2, 0x2, 0x2, 0xa0, 0x9b, 0x3, 0x2, 
       0x2, 0x2, 0xa0, 0x9c, 0x3, 0x2, 0x2, 0x2, 0xa0, 0x9d, 0x3, 0x2, 0x2, 
       0x2, 0xa0, 0x9e, 0x3, 0x2, 0x2, 0x2, 0xa0, 0x9f, 0x3, 0x2, 0x2, 0x2, 
       0xa1, 0x15, 0x3, 0x2, 0x2, 0x2, 0xa2, 0xa3, 0x7, 0x13, 0x2, 0x2, 
       0xa3, 0xa4, 0x5, 0x46, 0x24, 0x2, 0xa4, 0xa5, 0x7, 0x14, 0x2, 0x2, 
       0xa5, 0xa6, 0x5, 0x46, 0x24, 0x2, 0xa6, 0xa7, 0x5, 0x12, 0xa, 0x2, 
       0xa7, 0xa8, 0x5, 0x46, 0x24, 0x2, 0xa8, 0xa9, 0x7, 0x15, 0x2, 0x2, 
       0xa9, 0x17, 0x3, 0x2, 0x2, 0x2, 0xaa, 0xab, 0x7, 0x16, 0x2, 0x2, 
       0xab, 0xac, 0x5, 0x46, 0x24, 0x2, 0xac, 0xad, 0x7, 0x14, 0x2, 0x2, 
       0xad, 0xae, 0x5, 0x46, 0x24, 0x2, 0xae, 0xaf, 0x5, 0x12, 0xa, 0x2, 
       0xaf, 0xb0, 0x5, 0x46, 0x24, 0x2, 0xb0, 0xb1, 0x7, 0x15, 0x2, 0x2, 
       0xb1, 0x19, 0x3, 0x2, 0x2, 0x2, 0xb2, 0xb3, 0x5, 0x30, 0x19, 0x2, 
       0xb3, 0x1b, 0x3, 0x2, 0x2, 0x2, 0xb4, 0xb5, 0x7, 0x14, 0x2, 0x2, 
       0xb5, 0xb6, 0x5, 0x46, 0x24, 0x2, 0xb6, 0xbc, 0x5, 0x12, 0xa, 0x2, 
       0xb7, 0xb8, 0x5, 0x44, 0x23, 0x2, 0xb8, 0xb9, 0x5, 0x12, 0xa, 0x2, 
       0xb9, 0xbb, 0x3, 0x2, 0x2, 0x2, 0xba, 0xb7, 0x3, 0x2, 0x2, 0x2, 0xbb, 
       0xbe, 0x3, 0x2, 0x2, 0x2, 0xbc, 0xba, 0x3, 0x2, 0x2, 0x2, 0xbc, 0xbd, 
       0x3, 0x2, 0x2, 0x2, 0xbd, 0xbf, 0x3, 0x2, 0x2, 0x2, 0xbe, 0xbc, 0x3, 
       0x2, 0x2, 0x2, 0xbf, 0xc0, 0x5, 0x46, 0x24, 0x2, 0xc0, 0xc1, 0x7, 
       0x15, 0x2, 0x2, 0xc1, 0x1d, 0x3, 0x2, 0x2, 0x2, 0xc2, 0xc3, 0x5, 
       0x34, 0x1b, 0x2, 0xc3, 0x1f, 0x3, 0x2, 0x2, 0x2, 0xc4, 0xc5, 0x7, 
       0x17, 0x2, 0x2, 0xc5, 0xc8, 0x5, 0x46, 0x24, 0x2, 0xc6, 0xc9, 0x5, 
       0xe, 0x8, 0x2, 0xc7, 0xc9, 0x5, 0x2e, 0x18, 0x2, 0xc8, 0xc6, 0x3, 
       0x2, 0x2, 0x2, 0xc8, 0xc7, 0x3, 0x2, 0x2, 0x2, 0xc9, 0x21, 0x3, 0x2, 
       0x2, 0x2, 0xca, 0xcb, 0x7, 0x18, 0x2, 0x2, 0xcb, 0xcc, 0x5, 0x46, 
       0x24, 0x2, 0xcc, 0xcd, 0x5, 0xe, 0x8, 0x2, 0xcd, 0xce, 0x5, 0x46, 
       0x24, 0x2, 0xce, 0xcf, 0x5, 0xe, 0x8, 0x2, 0xcf, 0xd3, 0x5, 0x46, 
       0x24, 0x2, 0xd0, 0xd2, 0x5, 0xe, 0x8, 0x2, 0xd1, 0xd0, 0x3, 0x2, 
       0x2, 0x2, 0xd2, 0xd5, 0x3, 0x2, 0x2, 0x2, 0xd3, 0xd1, 0x3, 0x2, 0x2, 
       0x2, 0xd3, 0xd4, 0x3, 0x2, 0x2, 0x2, 0xd4, 0xd6, 0x3, 0x2, 0x2, 0x2, 
       0xd5, 0xd3, 0x3, 0x2, 0x2, 0x2, 0xd6, 0xd7, 0x5, 0x46, 0x24, 0x2, 
       0xd7, 0xd8, 0x7, 0x19, 0x2, 0x2, 0xd8, 0x23, 0x3, 0x2, 0x2, 0x2, 
       0xd9, 0xda, 0x7, 0x18, 0x2, 0x2, 0xda, 0xdb, 0x5, 0x46, 0x24, 0x2, 
       0xdb, 0xdc, 0x5, 0x26, 0x14, 0x2, 0xdc, 0xdd, 0x5, 0x46, 0x24, 0x2, 
       0xdd, 0xde, 0x5, 0xe, 0x8, 0x2, 0xde, 0xdf, 0x5, 0x46, 0x24, 0x2, 
       0xdf, 0xe0, 0x7, 0x19, 0x2, 0x2, 0xe0, 0x25, 0x3, 0x2, 0x2, 0x2, 
       0xe1, 0xe2, 0x7, 0x1a, 0x2, 0x2, 0xe2, 0xe3, 0x5, 0x46, 0x24, 0x2, 
       0xe3, 0xe4, 0x5, 0x28, 0x15, 0x2, 0xe4, 0x27, 0x3, 0x2, 0x2, 0x2, 
       0xe5, 0xec, 0x5, 0x2a, 0x16, 0x2, 0xe6, 0xe7, 0x5, 0x46, 0x24, 0x2, 
       0xe7, 0xe8, 0x7, 0x6, 0x2, 0x2, 0xe8, 0xe9, 0x5, 0x2a, 0x16, 0x2, 
       0xe9, 0xeb, 0x3, 0x2, 0x2, 0x2, 0xea, 0xe6, 0x3, 0x2, 0x2, 0x2, 0xeb, 
       0xee, 0x3, 0x2, 0x2, 0x2, 0xec, 0xea, 0x3, 0x2, 0x2, 0x2, 0xec, 0xed, 
       0x3, 0x2, 0x2, 0x2, 0xed, 0x29, 0x3, 0x2, 0x2, 0x2, 0xee, 0xec, 0x3, 
       0x2, 0x2, 0x2, 0xef, 0xf5, 0x5, 0x40, 0x21, 0x2, 0xf0, 0xf1, 0x5, 
       0x46, 0x24, 0x2, 0xf1, 0xf2, 0x7, 0x1b, 0x2, 0x2, 0xf2, 0xf3, 0x5, 
       0x46, 0x24, 0x2, 0xf3, 0xf4, 0x5, 0x12, 0xa, 0x2, 0xf4, 0xf6, 0x3, 
       0x2, 0x2, 0x2, 0xf5, 0xf0, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xf6, 0x3, 0x2, 
       0x2, 0x2, 0xf6, 0x2b, 0x3, 0x2, 0x2, 0x2, 0xf7, 0xf8, 0x7, 0x1c, 
       0x2, 0x2, 0xf8, 0xf9, 0x5, 0x46, 0x24, 0x2, 0xf9, 0xfa, 0x7, 0x3b, 
       0x2, 0x2, 0xfa, 0xfd, 0x5, 0x46, 0x24, 0x2, 0xfb, 0xfe, 0x5, 0xe, 
       0x8, 0x2, 0xfc, 0xfe, 0x5, 0x2e, 0x18, 0x2, 0xfd, 0xfb, 0x3, 0x2, 
       0x2, 0x2, 0xfd, 0xfc, 0x3, 0x2, 0x2, 0x2, 0xfe, 0x2d, 0x3, 0x2, 0x2, 
       0x2, 0xff, 0x104, 0x5, 0x36, 0x1c, 0x2, 0x100, 0x104, 0x5, 0x32, 
       0x1a, 0x2, 0x101, 0x104, 0x5, 0x3c, 0x1f, 0x2, 0x102, 0x104, 0x5, 
       0x38, 0x1d, 0x2, 0x103, 0xff, 0x3, 0x2, 0x2, 0x2, 0x103, 0x100, 0x3, 
       0x2, 0x2, 0x2, 0x103, 0x101, 0x3, 0x2, 0x2, 0x2, 0x103, 0x102, 0x3, 
       0x2, 0x2, 0x2, 0x104, 0x2f, 0x3, 0x2, 0x2, 0x2, 0x105, 0x106, 0x5, 
       0x3e, 0x20, 0x2, 0x106, 0x31, 0x3, 0x2, 0x2, 0x2, 0x107, 0x10a, 0x5, 
       0x3e, 0x20, 0x2, 0x108, 0x10a, 0x5, 0x42, 0x22, 0x2, 0x109, 0x107, 
       0x3, 0x2, 0x2, 0x2, 0x109, 0x108, 0x3, 0x2, 0x2, 0x2, 0x10a, 0x33, 
       0x3, 0x2, 0x2, 0x2, 0x10b, 0x10c, 0x5, 0x40, 0x21, 0x2, 0x10c, 0x35, 
       0x3, 0x2, 0x2, 0x2, 0x10d, 0x10e, 0x9, 0x2, 0x2, 0x2, 0x10e, 0x37, 
       0x3, 0x2, 0x2, 0x2, 0x10f, 0x110, 0x9, 0x3, 0x2, 0x2, 0x110, 0x39, 
       0x3, 0x2, 0x2, 0x2, 0x111, 0x115, 0x7, 0x35, 0x2, 0x2, 0x112, 0x114, 
       0x9, 0x4, 0x2, 0x2, 0x113, 0x112, 0x3, 0x2, 0x2, 0x2, 0x114, 0x117, 
       0x3, 0x2, 0x2, 0x2, 0x115, 0x113, 0x3, 0x2, 0x2, 0x2, 0x115, 0x116, 
       0x3, 0x2, 0x2, 0x2, 0x116, 0x3b, 0x3, 0x2, 0x2, 0x2, 0x117, 0x115, 
       0x3, 0x2, 0x2, 0x2, 0x118, 0x11c, 0x9, 0x4, 0x2, 0x2, 0x119, 0x11b, 
       0x9, 0x4, 0x2, 0x2, 0x11a, 0x119, 0x3, 0x2, 0x2, 0x2, 0x11b, 0x11e, 
       0x3, 0x2, 0x2, 0x2, 0x11c, 0x11a, 0x3, 0x2, 0x2, 0x2, 0x11c, 0x11d, 
       0x3, 0x2, 0x2, 0x2, 0x11d, 0x127, 0x3, 0x2, 0x2, 0x2, 0x11e, 0x11c, 
       0x3, 0x2, 0x2, 0x2, 0x11f, 0x120, 0x7, 0x3, 0x2, 0x2, 0x120, 0x124, 
       0x9, 0x4, 0x2, 0x2, 0x121, 0x123, 0x9, 0x4, 0x2, 0x2, 0x122, 0x121, 
       0x3, 0x2, 0x2, 0x2, 0x123, 0x126, 0x3, 0x2, 0x2, 0x2, 0x124, 0x122, 
       0x3, 0x2, 0x2, 0x2, 0x124, 0x125, 0x3, 0x2, 0x2, 0x2, 0x125, 0x128, 
       0x3, 0x2, 0x2, 0x2, 0x126, 0x124, 0x3, 0x2, 0x2, 0x2, 0x127, 0x11f, 
       0x3, 0x2, 0x2, 0x2, 0x127, 0x128, 0x3, 0x2, 0x2, 0x2, 0x128, 0x3d, 
       0x3, 0x2, 0x2, 0x2, 0x129, 0x12d, 0x7, 0x39, 0x2, 0x2, 0x12a, 0x12c, 
       0x9, 0x5, 0x2, 0x2, 0x12b, 0x12a, 0x3, 0x2, 0x2, 0x2, 0x12c, 0x12f, 
       0x3, 0x2, 0x2, 0x2, 0x12d, 0x12b, 0x3, 0x2, 0x2, 0x2, 0x12d, 0x12e, 
       0x3, 0x2, 0x2, 0x2, 0x12e, 0x3f, 0x3, 0x2, 0x2, 0x2, 0x12f, 0x12d, 
       0x3, 0x2, 0x2, 0x2, 0x130, 0x134, 0x7, 0x38, 0x2, 0x2, 0x131, 0x133, 
       0x9, 0x5, 0x2, 0x2, 0x132, 0x131, 0x3, 0x2, 0x2, 0x2, 0x133, 0x136, 
       0x3, 0x2, 0x2, 0x2, 0x134, 0x132, 0x3, 0x2, 0x2, 0x2, 0x134, 0x135, 
       0x3, 0x2, 0x2, 0x2, 0x135, 0x41, 0x3, 0x2, 0x2, 0x2, 0x136, 0x134, 
       0x3, 0x2, 0x2, 0x2, 0x137, 0x13b, 0x7, 0x37, 0x2, 0x2, 0x138, 0x13a, 
       0x7, 0x37, 0x2, 0x2, 0x139, 0x138, 0x3, 0x2, 0x2, 0x2, 0x13a, 0x13d, 
       0x3, 0x2, 0x2, 0x2, 0x13b, 0x139, 0x3, 0x2, 0x2, 0x2, 0x13b, 0x13c, 
       0x3, 0x2, 0x2, 0x2, 0x13c, 0x13e, 0x3, 0x2, 0x2, 0x2, 0x13d, 0x13b, 
       0x3, 0x2, 0x2, 0x2, 0x13e, 0x13f, 0x7, 0x37, 0x2, 0x2, 0x13f, 0x43, 
       0x3, 0x2, 0x2, 0x2, 0x140, 0x141, 0x7, 0x3c, 0x2, 0x2, 0x141, 0x142, 
       0x5, 0x46, 0x24, 0x2, 0x142, 0x45, 0x3, 0x2, 0x2, 0x2, 0x143, 0x145, 
       0x7, 0x3c, 0x2, 0x2, 0x144, 0x143, 0x3, 0x2, 0x2, 0x2, 0x145, 0x148, 
       0x3, 0x2, 0x2, 0x2, 0x146, 0x144, 0x3, 0x2, 0x2, 0x2, 0x146, 0x147, 
       0x3, 0x2, 0x2, 0x2, 0x147, 0x47, 0x3, 0x2, 0x2, 0x2, 0x148, 0x146, 
       0x3, 0x2, 0x2, 0x2, 0x1b, 0x4b, 0x55, 0x6b, 0x78, 0x7b, 0x85, 0x92, 
       0x95, 0xa0, 0xbc, 0xc8, 0xd3, 0xec, 0xf5, 0xfd, 0x103, 0x109, 0x115, 
       0x11c, 0x124, 0x127, 0x12d, 0x134, 0x13b, 0x146, 
  };

  _serializedATN.insert(_serializedATN.end(), serializedATNSegment0,
    serializedATNSegment0 + sizeof(serializedATNSegment0) / sizeof(serializedATNSegment0[0]));


  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

TILGrammarParser::Initializer TILGrammarParser::_init;
