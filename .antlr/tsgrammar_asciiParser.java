// Generated from /Users/matejnevlud/github/zpj-til-parser/tsgrammar_ascii.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class tsgrammar_asciiParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		T__45=46, T__46=47, T__47=48, T__48=49, T__49=50, T__50=51, T__51=52, 
		T__52=53, T__53=54, LOWERCASE_LETTER=55, UPPERCASE_LETTER=56, NONZERO_DIGIT=57, 
		Whitespace_character=58;
	public static final int
		RULE_start = 0, RULE_sentence = 1, RULE_sentence_content = 2, RULE_termination = 3, 
		RULE_type_definition = 4, RULE_entity_definition = 5, RULE_construction = 6, 
		RULE_global_variable_definition = 7, RULE_data_type = 8, RULE_embeded_type = 9, 
		RULE_list_type = 10, RULE_touple_type = 11, RULE_user_type = 12, RULE_enclosed_data_type = 13, 
		RULE_variable = 14, RULE_trivialisation = 15, RULE_composition = 16, RULE_closure = 17, 
		RULE_lambda_variables = 18, RULE_typed_variables = 19, RULE_typed_variable = 20, 
		RULE_n_execution = 21, RULE_entity = 22, RULE_type_name = 23, RULE_entity_name = 24, 
		RULE_variable_name = 25, RULE_keyword = 26, RULE_symbols = 27, RULE_zero = 28, 
		RULE_anything = 29, RULE_number = 30, RULE_upperletter_name = 31, RULE_lowerletter_name = 32, 
		RULE_quoted_name = 33, RULE_whitespace = 34, RULE_optional_whitespace = 35;
	private static String[] makeRuleNames() {
		return new String[] {
			"start", "sentence", "sentence_content", "termination", "type_definition", 
			"entity_definition", "construction", "global_variable_definition", "data_type", 
			"embeded_type", "list_type", "touple_type", "user_type", "enclosed_data_type", 
			"variable", "trivialisation", "composition", "closure", "lambda_variables", 
			"typed_variables", "typed_variable", "n_execution", "entity", "type_name", 
			"entity_name", "variable_name", "keyword", "symbols", "zero", "anything", 
			"number", "upperletter_name", "lowerletter_name", "quoted_name", "whitespace", 
			"optional_whitespace"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'.'", "'TypeDef'", "':='", "','", "'/'", "'@wt'", "'->'", "'@tw'", 
			"'Bool'", "'Indiv'", "'Time'", "'String'", "'World'", "'Real'", "'Int'", 
			"'*'", "'List'", "'('", "')'", "'Tuple'", "'''", "'['", "']'", "'\\'", 
			"':'", "'^'", "'ForAll'", "'Exist'", "'Every'", "'Some'", "'No'", "'True'", 
			"'False'", "'And'", "'Or'", "'Not'", "'Implies'", "'Sing'", "'Sub'", 
			"'Tr'", "'TrueC'", "'FalseC'", "'ImproperC'", "'TrueP'", "'FalseP'", 
			"'UndefP'", "'ToInt'", "'+'", "'-'", "'='", "'0'", "'Any'", "'_'", "'%'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, "LOWERCASE_LETTER", "UPPERCASE_LETTER", 
			"NONZERO_DIGIT", "Whitespace_character"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "tsgrammar_ascii.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public tsgrammar_asciiParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class StartContext extends ParserRuleContext {
		public List<SentenceContext> sentence() {
			return getRuleContexts(SentenceContext.class);
		}
		public SentenceContext sentence(int i) {
			return getRuleContext(SentenceContext.class,i);
		}
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_start);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(75);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__20) | (1L << T__21) | (1L << T__25) | (1L << T__53) | (1L << LOWERCASE_LETTER) | (1L << UPPERCASE_LETTER))) != 0)) {
				{
				{
				setState(72);
				sentence();
				}
				}
				setState(77);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SentenceContext extends ParserRuleContext {
		public Sentence_contentContext sentence_content() {
			return getRuleContext(Sentence_contentContext.class,0);
		}
		public TerminationContext termination() {
			return getRuleContext(TerminationContext.class,0);
		}
		public SentenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentence; }
	}

	public final SentenceContext sentence() throws RecognitionException {
		SentenceContext _localctx = new SentenceContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_sentence);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(78);
			sentence_content();
			setState(79);
			termination();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sentence_contentContext extends ParserRuleContext {
		public Type_definitionContext type_definition() {
			return getRuleContext(Type_definitionContext.class,0);
		}
		public Entity_definitionContext entity_definition() {
			return getRuleContext(Entity_definitionContext.class,0);
		}
		public ConstructionContext construction() {
			return getRuleContext(ConstructionContext.class,0);
		}
		public Global_variable_definitionContext global_variable_definition() {
			return getRuleContext(Global_variable_definitionContext.class,0);
		}
		public Sentence_contentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentence_content; }
	}

	public final Sentence_contentContext sentence_content() throws RecognitionException {
		Sentence_contentContext _localctx = new Sentence_contentContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_sentence_content);
		try {
			setState(85);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(81);
				type_definition();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(82);
				entity_definition();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(83);
				construction();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(84);
				global_variable_definition();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TerminationContext extends ParserRuleContext {
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public TerminationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_termination; }
	}

	public final TerminationContext termination() throws RecognitionException {
		TerminationContext _localctx = new TerminationContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_termination);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(87);
			optional_whitespace();
			setState(88);
			match(T__0);
			setState(89);
			optional_whitespace();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_definitionContext extends ParserRuleContext {
		public WhitespaceContext whitespace() {
			return getRuleContext(WhitespaceContext.class,0);
		}
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public Type_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_definition; }
	}

	public final Type_definitionContext type_definition() throws RecognitionException {
		Type_definitionContext _localctx = new Type_definitionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_type_definition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(91);
			match(T__1);
			setState(92);
			whitespace();
			setState(93);
			type_name();
			setState(94);
			optional_whitespace();
			setState(95);
			match(T__2);
			setState(96);
			optional_whitespace();
			setState(97);
			data_type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Entity_definitionContext extends ParserRuleContext {
		public List<Entity_nameContext> entity_name() {
			return getRuleContexts(Entity_nameContext.class);
		}
		public Entity_nameContext entity_name(int i) {
			return getRuleContext(Entity_nameContext.class,i);
		}
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public Entity_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entity_definition; }
	}

	public final Entity_definitionContext entity_definition() throws RecognitionException {
		Entity_definitionContext _localctx = new Entity_definitionContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_entity_definition);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(99);
			entity_name();
			setState(107);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(100);
					optional_whitespace();
					setState(101);
					match(T__3);
					setState(102);
					optional_whitespace();
					setState(103);
					entity_name();
					}
					} 
				}
				setState(109);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			setState(110);
			optional_whitespace();
			setState(111);
			match(T__4);
			setState(112);
			optional_whitespace();
			setState(113);
			data_type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructionContext extends ParserRuleContext {
		public TrivialisationContext trivialisation() {
			return getRuleContext(TrivialisationContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public ClosureContext closure() {
			return getRuleContext(ClosureContext.class,0);
		}
		public N_executionContext n_execution() {
			return getRuleContext(N_executionContext.class,0);
		}
		public CompositionContext composition() {
			return getRuleContext(CompositionContext.class,0);
		}
		public ConstructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_construction; }
	}

	public final ConstructionContext construction() throws RecognitionException {
		ConstructionContext _localctx = new ConstructionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_construction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(120);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				{
				setState(115);
				trivialisation();
				}
				break;
			case 2:
				{
				setState(116);
				variable();
				}
				break;
			case 3:
				{
				setState(117);
				closure();
				}
				break;
			case 4:
				{
				setState(118);
				n_execution();
				}
				break;
			case 5:
				{
				setState(119);
				composition();
				}
				break;
			}
			setState(123);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				{
				setState(122);
				match(T__5);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Global_variable_definitionContext extends ParserRuleContext {
		public List<Variable_nameContext> variable_name() {
			return getRuleContexts(Variable_nameContext.class);
		}
		public Variable_nameContext variable_name(int i) {
			return getRuleContext(Variable_nameContext.class,i);
		}
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public Global_variable_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_global_variable_definition; }
	}

	public final Global_variable_definitionContext global_variable_definition() throws RecognitionException {
		Global_variable_definitionContext _localctx = new Global_variable_definitionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_global_variable_definition);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(125);
			variable_name();
			setState(133);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(126);
					optional_whitespace();
					setState(127);
					match(T__3);
					setState(128);
					optional_whitespace();
					setState(129);
					variable_name();
					}
					} 
				}
				setState(135);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			}
			setState(136);
			optional_whitespace();
			setState(137);
			match(T__6);
			setState(138);
			optional_whitespace();
			setState(139);
			data_type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Data_typeContext extends ParserRuleContext {
		public Embeded_typeContext embeded_type() {
			return getRuleContext(Embeded_typeContext.class,0);
		}
		public List_typeContext list_type() {
			return getRuleContext(List_typeContext.class,0);
		}
		public Touple_typeContext touple_type() {
			return getRuleContext(Touple_typeContext.class,0);
		}
		public User_typeContext user_type() {
			return getRuleContext(User_typeContext.class,0);
		}
		public Enclosed_data_typeContext enclosed_data_type() {
			return getRuleContext(Enclosed_data_typeContext.class,0);
		}
		public Data_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_data_type; }
	}

	public final Data_typeContext data_type() throws RecognitionException {
		Data_typeContext _localctx = new Data_typeContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_data_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(146);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__8:
			case T__9:
			case T__10:
			case T__11:
			case T__12:
			case T__13:
			case T__14:
			case T__15:
			case T__51:
				{
				setState(141);
				embeded_type();
				}
				break;
			case T__16:
				{
				setState(142);
				list_type();
				}
				break;
			case T__19:
				{
				setState(143);
				touple_type();
				}
				break;
			case UPPERCASE_LETTER:
				{
				setState(144);
				user_type();
				}
				break;
			case T__17:
				{
				setState(145);
				enclosed_data_type();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(149);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__7) {
				{
				setState(148);
				match(T__7);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Embeded_typeContext extends ParserRuleContext {
		public AnythingContext anything() {
			return getRuleContext(AnythingContext.class,0);
		}
		public Embeded_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_embeded_type; }
	}

	public final Embeded_typeContext embeded_type() throws RecognitionException {
		Embeded_typeContext _localctx = new Embeded_typeContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_embeded_type);
		try {
			setState(160);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__8:
				enterOuterAlt(_localctx, 1);
				{
				setState(151);
				match(T__8);
				}
				break;
			case T__9:
				enterOuterAlt(_localctx, 2);
				{
				setState(152);
				match(T__9);
				}
				break;
			case T__10:
				enterOuterAlt(_localctx, 3);
				{
				setState(153);
				match(T__10);
				}
				break;
			case T__11:
				enterOuterAlt(_localctx, 4);
				{
				setState(154);
				match(T__11);
				}
				break;
			case T__12:
				enterOuterAlt(_localctx, 5);
				{
				setState(155);
				match(T__12);
				}
				break;
			case T__13:
				enterOuterAlt(_localctx, 6);
				{
				setState(156);
				match(T__13);
				}
				break;
			case T__14:
				enterOuterAlt(_localctx, 7);
				{
				setState(157);
				match(T__14);
				}
				break;
			case T__51:
				enterOuterAlt(_localctx, 8);
				{
				setState(158);
				anything();
				}
				break;
			case T__15:
				enterOuterAlt(_localctx, 9);
				{
				setState(159);
				match(T__15);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class List_typeContext extends ParserRuleContext {
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public List_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_list_type; }
	}

	public final List_typeContext list_type() throws RecognitionException {
		List_typeContext _localctx = new List_typeContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_list_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(162);
			match(T__16);
			setState(163);
			optional_whitespace();
			setState(164);
			match(T__17);
			setState(165);
			optional_whitespace();
			setState(166);
			data_type();
			setState(167);
			optional_whitespace();
			setState(168);
			match(T__18);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Touple_typeContext extends ParserRuleContext {
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public Touple_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_touple_type; }
	}

	public final Touple_typeContext touple_type() throws RecognitionException {
		Touple_typeContext _localctx = new Touple_typeContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_touple_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(170);
			match(T__19);
			setState(171);
			optional_whitespace();
			setState(172);
			match(T__17);
			setState(173);
			optional_whitespace();
			setState(174);
			data_type();
			setState(175);
			optional_whitespace();
			setState(176);
			match(T__18);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class User_typeContext extends ParserRuleContext {
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public User_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_user_type; }
	}

	public final User_typeContext user_type() throws RecognitionException {
		User_typeContext _localctx = new User_typeContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_user_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(178);
			type_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Enclosed_data_typeContext extends ParserRuleContext {
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public List<Data_typeContext> data_type() {
			return getRuleContexts(Data_typeContext.class);
		}
		public Data_typeContext data_type(int i) {
			return getRuleContext(Data_typeContext.class,i);
		}
		public List<WhitespaceContext> whitespace() {
			return getRuleContexts(WhitespaceContext.class);
		}
		public WhitespaceContext whitespace(int i) {
			return getRuleContext(WhitespaceContext.class,i);
		}
		public Enclosed_data_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enclosed_data_type; }
	}

	public final Enclosed_data_typeContext enclosed_data_type() throws RecognitionException {
		Enclosed_data_typeContext _localctx = new Enclosed_data_typeContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_enclosed_data_type);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(180);
			match(T__17);
			setState(181);
			optional_whitespace();
			setState(182);
			data_type();
			setState(188);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(183);
					whitespace();
					setState(184);
					data_type();
					}
					} 
				}
				setState(190);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,9,_ctx);
			}
			setState(191);
			optional_whitespace();
			setState(192);
			match(T__18);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_variable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(194);
			variable_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TrivialisationContext extends ParserRuleContext {
		public Optional_whitespaceContext optional_whitespace() {
			return getRuleContext(Optional_whitespaceContext.class,0);
		}
		public ConstructionContext construction() {
			return getRuleContext(ConstructionContext.class,0);
		}
		public EntityContext entity() {
			return getRuleContext(EntityContext.class,0);
		}
		public TrivialisationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_trivialisation; }
	}

	public final TrivialisationContext trivialisation() throws RecognitionException {
		TrivialisationContext _localctx = new TrivialisationContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_trivialisation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(196);
			match(T__20);
			setState(197);
			optional_whitespace();
			setState(200);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__20:
			case T__21:
			case T__25:
			case LOWERCASE_LETTER:
				{
				setState(198);
				construction();
				}
				break;
			case T__4:
			case T__15:
			case T__26:
			case T__27:
			case T__28:
			case T__29:
			case T__30:
			case T__31:
			case T__32:
			case T__33:
			case T__34:
			case T__35:
			case T__36:
			case T__37:
			case T__38:
			case T__39:
			case T__40:
			case T__41:
			case T__42:
			case T__43:
			case T__44:
			case T__45:
			case T__46:
			case T__47:
			case T__48:
			case T__49:
			case T__50:
			case T__53:
			case UPPERCASE_LETTER:
			case NONZERO_DIGIT:
				{
				setState(199);
				entity();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompositionContext extends ParserRuleContext {
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public List<ConstructionContext> construction() {
			return getRuleContexts(ConstructionContext.class);
		}
		public ConstructionContext construction(int i) {
			return getRuleContext(ConstructionContext.class,i);
		}
		public CompositionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_composition; }
	}

	public final CompositionContext composition() throws RecognitionException {
		CompositionContext _localctx = new CompositionContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_composition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(202);
			match(T__21);
			setState(203);
			optional_whitespace();
			setState(204);
			construction();
			setState(205);
			optional_whitespace();
			setState(206);
			construction();
			setState(210);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__20) | (1L << T__21) | (1L << T__25) | (1L << LOWERCASE_LETTER))) != 0)) {
				{
				{
				setState(207);
				construction();
				}
				}
				setState(212);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(213);
			optional_whitespace();
			setState(214);
			match(T__22);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClosureContext extends ParserRuleContext {
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Lambda_variablesContext lambda_variables() {
			return getRuleContext(Lambda_variablesContext.class,0);
		}
		public ConstructionContext construction() {
			return getRuleContext(ConstructionContext.class,0);
		}
		public ClosureContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_closure; }
	}

	public final ClosureContext closure() throws RecognitionException {
		ClosureContext _localctx = new ClosureContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_closure);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(216);
			match(T__21);
			setState(217);
			optional_whitespace();
			setState(218);
			lambda_variables();
			setState(219);
			optional_whitespace();
			setState(220);
			construction();
			setState(221);
			optional_whitespace();
			setState(222);
			match(T__22);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Lambda_variablesContext extends ParserRuleContext {
		public Optional_whitespaceContext optional_whitespace() {
			return getRuleContext(Optional_whitespaceContext.class,0);
		}
		public Typed_variablesContext typed_variables() {
			return getRuleContext(Typed_variablesContext.class,0);
		}
		public Lambda_variablesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lambda_variables; }
	}

	public final Lambda_variablesContext lambda_variables() throws RecognitionException {
		Lambda_variablesContext _localctx = new Lambda_variablesContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_lambda_variables);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(224);
			match(T__23);
			setState(225);
			optional_whitespace();
			setState(226);
			typed_variables();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Typed_variablesContext extends ParserRuleContext {
		public List<Typed_variableContext> typed_variable() {
			return getRuleContexts(Typed_variableContext.class);
		}
		public Typed_variableContext typed_variable(int i) {
			return getRuleContext(Typed_variableContext.class,i);
		}
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Typed_variablesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typed_variables; }
	}

	public final Typed_variablesContext typed_variables() throws RecognitionException {
		Typed_variablesContext _localctx = new Typed_variablesContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_typed_variables);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(228);
			typed_variable();
			setState(235);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(229);
					optional_whitespace();
					setState(230);
					match(T__3);
					setState(231);
					typed_variable();
					}
					} 
				}
				setState(237);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Typed_variableContext extends ParserRuleContext {
		public Lowerletter_nameContext lowerletter_name() {
			return getRuleContext(Lowerletter_nameContext.class,0);
		}
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public Typed_variableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typed_variable; }
	}

	public final Typed_variableContext typed_variable() throws RecognitionException {
		Typed_variableContext _localctx = new Typed_variableContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_typed_variable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(238);
			lowerletter_name();
			setState(244);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				{
				setState(239);
				optional_whitespace();
				setState(240);
				match(T__24);
				setState(241);
				optional_whitespace();
				setState(242);
				data_type();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class N_executionContext extends ParserRuleContext {
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public TerminalNode NONZERO_DIGIT() { return getToken(tsgrammar_asciiParser.NONZERO_DIGIT, 0); }
		public ConstructionContext construction() {
			return getRuleContext(ConstructionContext.class,0);
		}
		public EntityContext entity() {
			return getRuleContext(EntityContext.class,0);
		}
		public N_executionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_n_execution; }
	}

	public final N_executionContext n_execution() throws RecognitionException {
		N_executionContext _localctx = new N_executionContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_n_execution);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(246);
			match(T__25);
			setState(247);
			optional_whitespace();
			setState(248);
			match(NONZERO_DIGIT);
			setState(249);
			optional_whitespace();
			setState(252);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__20:
			case T__21:
			case T__25:
			case LOWERCASE_LETTER:
				{
				setState(250);
				construction();
				}
				break;
			case T__4:
			case T__15:
			case T__26:
			case T__27:
			case T__28:
			case T__29:
			case T__30:
			case T__31:
			case T__32:
			case T__33:
			case T__34:
			case T__35:
			case T__36:
			case T__37:
			case T__38:
			case T__39:
			case T__40:
			case T__41:
			case T__42:
			case T__43:
			case T__44:
			case T__45:
			case T__46:
			case T__47:
			case T__48:
			case T__49:
			case T__50:
			case T__53:
			case UPPERCASE_LETTER:
			case NONZERO_DIGIT:
				{
				setState(251);
				entity();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EntityContext extends ParserRuleContext {
		public KeywordContext keyword() {
			return getRuleContext(KeywordContext.class,0);
		}
		public Entity_nameContext entity_name() {
			return getRuleContext(Entity_nameContext.class,0);
		}
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public SymbolsContext symbols() {
			return getRuleContext(SymbolsContext.class,0);
		}
		public EntityContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entity; }
	}

	public final EntityContext entity() throws RecognitionException {
		EntityContext _localctx = new EntityContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_entity);
		try {
			setState(258);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__26:
			case T__27:
			case T__28:
			case T__29:
			case T__30:
			case T__31:
			case T__32:
			case T__33:
			case T__34:
			case T__35:
			case T__36:
			case T__37:
			case T__38:
			case T__39:
			case T__40:
			case T__41:
			case T__42:
			case T__43:
			case T__44:
			case T__45:
			case T__46:
				enterOuterAlt(_localctx, 1);
				{
				setState(254);
				keyword();
				}
				break;
			case T__53:
			case UPPERCASE_LETTER:
				enterOuterAlt(_localctx, 2);
				{
				setState(255);
				entity_name();
				}
				break;
			case T__50:
			case NONZERO_DIGIT:
				enterOuterAlt(_localctx, 3);
				{
				setState(256);
				number();
				}
				break;
			case T__4:
			case T__15:
			case T__47:
			case T__48:
			case T__49:
				enterOuterAlt(_localctx, 4);
				{
				setState(257);
				symbols();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_nameContext extends ParserRuleContext {
		public Upperletter_nameContext upperletter_name() {
			return getRuleContext(Upperletter_nameContext.class,0);
		}
		public Type_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_name; }
	}

	public final Type_nameContext type_name() throws RecognitionException {
		Type_nameContext _localctx = new Type_nameContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_type_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(260);
			upperletter_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Entity_nameContext extends ParserRuleContext {
		public Upperletter_nameContext upperletter_name() {
			return getRuleContext(Upperletter_nameContext.class,0);
		}
		public Quoted_nameContext quoted_name() {
			return getRuleContext(Quoted_nameContext.class,0);
		}
		public Entity_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entity_name; }
	}

	public final Entity_nameContext entity_name() throws RecognitionException {
		Entity_nameContext _localctx = new Entity_nameContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_entity_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(264);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case UPPERCASE_LETTER:
				{
				setState(262);
				upperletter_name();
				}
				break;
			case T__53:
				{
				setState(263);
				quoted_name();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Variable_nameContext extends ParserRuleContext {
		public Lowerletter_nameContext lowerletter_name() {
			return getRuleContext(Lowerletter_nameContext.class,0);
		}
		public Variable_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable_name; }
	}

	public final Variable_nameContext variable_name() throws RecognitionException {
		Variable_nameContext _localctx = new Variable_nameContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_variable_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(266);
			lowerletter_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeywordContext extends ParserRuleContext {
		public KeywordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyword; }
	}

	public final KeywordContext keyword() throws RecognitionException {
		KeywordContext _localctx = new KeywordContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_keyword);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(268);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39) | (1L << T__40) | (1L << T__41) | (1L << T__42) | (1L << T__43) | (1L << T__44) | (1L << T__45) | (1L << T__46))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SymbolsContext extends ParserRuleContext {
		public SymbolsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_symbols; }
	}

	public final SymbolsContext symbols() throws RecognitionException {
		SymbolsContext _localctx = new SymbolsContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_symbols);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(270);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__15) | (1L << T__47) | (1L << T__48) | (1L << T__49))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ZeroContext extends ParserRuleContext {
		public ZeroContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_zero; }
	}

	public final ZeroContext zero() throws RecognitionException {
		ZeroContext _localctx = new ZeroContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_zero);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(272);
			match(T__50);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnythingContext extends ParserRuleContext {
		public List<ZeroContext> zero() {
			return getRuleContexts(ZeroContext.class);
		}
		public ZeroContext zero(int i) {
			return getRuleContext(ZeroContext.class,i);
		}
		public List<TerminalNode> NONZERO_DIGIT() { return getTokens(tsgrammar_asciiParser.NONZERO_DIGIT); }
		public TerminalNode NONZERO_DIGIT(int i) {
			return getToken(tsgrammar_asciiParser.NONZERO_DIGIT, i);
		}
		public AnythingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_anything; }
	}

	public final AnythingContext anything() throws RecognitionException {
		AnythingContext _localctx = new AnythingContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_anything);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(274);
			match(T__51);
			setState(279);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__50 || _la==NONZERO_DIGIT) {
				{
				setState(277);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__50:
					{
					setState(275);
					zero();
					}
					break;
				case NONZERO_DIGIT:
					{
					setState(276);
					match(NONZERO_DIGIT);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(281);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public List<ZeroContext> zero() {
			return getRuleContexts(ZeroContext.class);
		}
		public ZeroContext zero(int i) {
			return getRuleContext(ZeroContext.class,i);
		}
		public List<TerminalNode> NONZERO_DIGIT() { return getTokens(tsgrammar_asciiParser.NONZERO_DIGIT); }
		public TerminalNode NONZERO_DIGIT(int i) {
			return getToken(tsgrammar_asciiParser.NONZERO_DIGIT, i);
		}
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_number);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(284);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__50:
				{
				setState(282);
				zero();
				}
				break;
			case NONZERO_DIGIT:
				{
				setState(283);
				match(NONZERO_DIGIT);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(290);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__50 || _la==NONZERO_DIGIT) {
				{
				setState(288);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__50:
					{
					setState(286);
					zero();
					}
					break;
				case NONZERO_DIGIT:
					{
					setState(287);
					match(NONZERO_DIGIT);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(292);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(305);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,25,_ctx) ) {
			case 1:
				{
				setState(293);
				match(T__0);
				setState(296);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__50:
					{
					setState(294);
					zero();
					}
					break;
				case NONZERO_DIGIT:
					{
					setState(295);
					match(NONZERO_DIGIT);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(302);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__50 || _la==NONZERO_DIGIT) {
					{
					setState(300);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case T__50:
						{
						setState(298);
						zero();
						}
						break;
					case NONZERO_DIGIT:
						{
						setState(299);
						match(NONZERO_DIGIT);
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
					setState(304);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Upperletter_nameContext extends ParserRuleContext {
		public List<TerminalNode> UPPERCASE_LETTER() { return getTokens(tsgrammar_asciiParser.UPPERCASE_LETTER); }
		public TerminalNode UPPERCASE_LETTER(int i) {
			return getToken(tsgrammar_asciiParser.UPPERCASE_LETTER, i);
		}
		public List<TerminalNode> LOWERCASE_LETTER() { return getTokens(tsgrammar_asciiParser.LOWERCASE_LETTER); }
		public TerminalNode LOWERCASE_LETTER(int i) {
			return getToken(tsgrammar_asciiParser.LOWERCASE_LETTER, i);
		}
		public List<ZeroContext> zero() {
			return getRuleContexts(ZeroContext.class);
		}
		public ZeroContext zero(int i) {
			return getRuleContext(ZeroContext.class,i);
		}
		public List<TerminalNode> NONZERO_DIGIT() { return getTokens(tsgrammar_asciiParser.NONZERO_DIGIT); }
		public TerminalNode NONZERO_DIGIT(int i) {
			return getToken(tsgrammar_asciiParser.NONZERO_DIGIT, i);
		}
		public Upperletter_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_upperletter_name; }
	}

	public final Upperletter_nameContext upperletter_name() throws RecognitionException {
		Upperletter_nameContext _localctx = new Upperletter_nameContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_upperletter_name);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(307);
			match(UPPERCASE_LETTER);
			setState(315);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					setState(313);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case LOWERCASE_LETTER:
						{
						setState(308);
						match(LOWERCASE_LETTER);
						}
						break;
					case UPPERCASE_LETTER:
						{
						setState(309);
						match(UPPERCASE_LETTER);
						}
						break;
					case T__52:
						{
						setState(310);
						match(T__52);
						}
						break;
					case T__50:
						{
						setState(311);
						zero();
						}
						break;
					case NONZERO_DIGIT:
						{
						setState(312);
						match(NONZERO_DIGIT);
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					} 
				}
				setState(317);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,27,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Lowerletter_nameContext extends ParserRuleContext {
		public List<TerminalNode> LOWERCASE_LETTER() { return getTokens(tsgrammar_asciiParser.LOWERCASE_LETTER); }
		public TerminalNode LOWERCASE_LETTER(int i) {
			return getToken(tsgrammar_asciiParser.LOWERCASE_LETTER, i);
		}
		public List<TerminalNode> UPPERCASE_LETTER() { return getTokens(tsgrammar_asciiParser.UPPERCASE_LETTER); }
		public TerminalNode UPPERCASE_LETTER(int i) {
			return getToken(tsgrammar_asciiParser.UPPERCASE_LETTER, i);
		}
		public List<ZeroContext> zero() {
			return getRuleContexts(ZeroContext.class);
		}
		public ZeroContext zero(int i) {
			return getRuleContext(ZeroContext.class,i);
		}
		public List<TerminalNode> NONZERO_DIGIT() { return getTokens(tsgrammar_asciiParser.NONZERO_DIGIT); }
		public TerminalNode NONZERO_DIGIT(int i) {
			return getToken(tsgrammar_asciiParser.NONZERO_DIGIT, i);
		}
		public Lowerletter_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lowerletter_name; }
	}

	public final Lowerletter_nameContext lowerletter_name() throws RecognitionException {
		Lowerletter_nameContext _localctx = new Lowerletter_nameContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_lowerletter_name);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(318);
			match(LOWERCASE_LETTER);
			setState(326);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					setState(324);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case LOWERCASE_LETTER:
						{
						setState(319);
						match(LOWERCASE_LETTER);
						}
						break;
					case UPPERCASE_LETTER:
						{
						setState(320);
						match(UPPERCASE_LETTER);
						}
						break;
					case T__52:
						{
						setState(321);
						match(T__52);
						}
						break;
					case T__50:
						{
						setState(322);
						zero();
						}
						break;
					case NONZERO_DIGIT:
						{
						setState(323);
						match(NONZERO_DIGIT);
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					} 
				}
				setState(328);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,29,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Quoted_nameContext extends ParserRuleContext {
		public Quoted_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_quoted_name; }
	}

	public final Quoted_nameContext quoted_name() throws RecognitionException {
		Quoted_nameContext _localctx = new Quoted_nameContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_quoted_name);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(329);
			match(T__53);
			setState(333);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(330);
					match(T__53);
					}
					} 
				}
				setState(335);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			}
			setState(336);
			match(T__53);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhitespaceContext extends ParserRuleContext {
		public TerminalNode Whitespace_character() { return getToken(tsgrammar_asciiParser.Whitespace_character, 0); }
		public Optional_whitespaceContext optional_whitespace() {
			return getRuleContext(Optional_whitespaceContext.class,0);
		}
		public WhitespaceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whitespace; }
	}

	public final WhitespaceContext whitespace() throws RecognitionException {
		WhitespaceContext _localctx = new WhitespaceContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_whitespace);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(338);
			match(Whitespace_character);
			setState(339);
			optional_whitespace();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Optional_whitespaceContext extends ParserRuleContext {
		public List<TerminalNode> Whitespace_character() { return getTokens(tsgrammar_asciiParser.Whitespace_character); }
		public TerminalNode Whitespace_character(int i) {
			return getToken(tsgrammar_asciiParser.Whitespace_character, i);
		}
		public Optional_whitespaceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_optional_whitespace; }
	}

	public final Optional_whitespaceContext optional_whitespace() throws RecognitionException {
		Optional_whitespaceContext _localctx = new Optional_whitespaceContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_optional_whitespace);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(344);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Whitespace_character) {
				{
				{
				setState(341);
				match(Whitespace_character);
				}
				}
				setState(346);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3<\u015e\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\3\2\7\2L\n\2\f\2\16\2O\13\2\3\3\3\3\3\3"+
		"\3\4\3\4\3\4\3\4\5\4X\n\4\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\7\3\7\3\7\3\7\3\7\3\7\7\7l\n\7\f\7\16\7o\13\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\b\3\b\3\b\3\b\3\b\5\b{\n\b\3\b\5\b~\n\b\3\t\3\t\3\t\3\t\3\t\3\t\7"+
		"\t\u0086\n\t\f\t\16\t\u0089\13\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3"+
		"\n\5\n\u0095\n\n\3\n\5\n\u0098\n\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13"+
		"\3\13\3\13\5\13\u00a3\n\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r"+
		"\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\7\17\u00bd"+
		"\n\17\f\17\16\17\u00c0\13\17\3\17\3\17\3\17\3\20\3\20\3\21\3\21\3\21\3"+
		"\21\5\21\u00cb\n\21\3\22\3\22\3\22\3\22\3\22\3\22\7\22\u00d3\n\22\f\22"+
		"\16\22\u00d6\13\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3"+
		"\23\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\7\25\u00ec\n\25\f\25"+
		"\16\25\u00ef\13\25\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u00f7\n\26\3\27"+
		"\3\27\3\27\3\27\3\27\3\27\5\27\u00ff\n\27\3\30\3\30\3\30\3\30\5\30\u0105"+
		"\n\30\3\31\3\31\3\32\3\32\5\32\u010b\n\32\3\33\3\33\3\34\3\34\3\35\3\35"+
		"\3\36\3\36\3\37\3\37\3\37\7\37\u0118\n\37\f\37\16\37\u011b\13\37\3 \3"+
		" \5 \u011f\n \3 \3 \7 \u0123\n \f \16 \u0126\13 \3 \3 \3 \5 \u012b\n "+
		"\3 \3 \7 \u012f\n \f \16 \u0132\13 \5 \u0134\n \3!\3!\3!\3!\3!\3!\7!\u013c"+
		"\n!\f!\16!\u013f\13!\3\"\3\"\3\"\3\"\3\"\3\"\7\"\u0147\n\"\f\"\16\"\u014a"+
		"\13\"\3#\3#\7#\u014e\n#\f#\16#\u0151\13#\3#\3#\3$\3$\3$\3%\7%\u0159\n"+
		"%\f%\16%\u015c\13%\3%\2\2&\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$"+
		"&(*,.\60\62\64\668:<>@BDFH\2\4\3\2\35\61\5\2\7\7\22\22\62\64\2\u0170\2"+
		"M\3\2\2\2\4P\3\2\2\2\6W\3\2\2\2\bY\3\2\2\2\n]\3\2\2\2\fe\3\2\2\2\16z\3"+
		"\2\2\2\20\177\3\2\2\2\22\u0094\3\2\2\2\24\u00a2\3\2\2\2\26\u00a4\3\2\2"+
		"\2\30\u00ac\3\2\2\2\32\u00b4\3\2\2\2\34\u00b6\3\2\2\2\36\u00c4\3\2\2\2"+
		" \u00c6\3\2\2\2\"\u00cc\3\2\2\2$\u00da\3\2\2\2&\u00e2\3\2\2\2(\u00e6\3"+
		"\2\2\2*\u00f0\3\2\2\2,\u00f8\3\2\2\2.\u0104\3\2\2\2\60\u0106\3\2\2\2\62"+
		"\u010a\3\2\2\2\64\u010c\3\2\2\2\66\u010e\3\2\2\28\u0110\3\2\2\2:\u0112"+
		"\3\2\2\2<\u0114\3\2\2\2>\u011e\3\2\2\2@\u0135\3\2\2\2B\u0140\3\2\2\2D"+
		"\u014b\3\2\2\2F\u0154\3\2\2\2H\u015a\3\2\2\2JL\5\4\3\2KJ\3\2\2\2LO\3\2"+
		"\2\2MK\3\2\2\2MN\3\2\2\2N\3\3\2\2\2OM\3\2\2\2PQ\5\6\4\2QR\5\b\5\2R\5\3"+
		"\2\2\2SX\5\n\6\2TX\5\f\7\2UX\5\16\b\2VX\5\20\t\2WS\3\2\2\2WT\3\2\2\2W"+
		"U\3\2\2\2WV\3\2\2\2X\7\3\2\2\2YZ\5H%\2Z[\7\3\2\2[\\\5H%\2\\\t\3\2\2\2"+
		"]^\7\4\2\2^_\5F$\2_`\5\60\31\2`a\5H%\2ab\7\5\2\2bc\5H%\2cd\5\22\n\2d\13"+
		"\3\2\2\2em\5\62\32\2fg\5H%\2gh\7\6\2\2hi\5H%\2ij\5\62\32\2jl\3\2\2\2k"+
		"f\3\2\2\2lo\3\2\2\2mk\3\2\2\2mn\3\2\2\2np\3\2\2\2om\3\2\2\2pq\5H%\2qr"+
		"\7\7\2\2rs\5H%\2st\5\22\n\2t\r\3\2\2\2u{\5 \21\2v{\5\36\20\2w{\5$\23\2"+
		"x{\5,\27\2y{\5\"\22\2zu\3\2\2\2zv\3\2\2\2zw\3\2\2\2zx\3\2\2\2zy\3\2\2"+
		"\2{}\3\2\2\2|~\7\b\2\2}|\3\2\2\2}~\3\2\2\2~\17\3\2\2\2\177\u0087\5\64"+
		"\33\2\u0080\u0081\5H%\2\u0081\u0082\7\6\2\2\u0082\u0083\5H%\2\u0083\u0084"+
		"\5\64\33\2\u0084\u0086\3\2\2\2\u0085\u0080\3\2\2\2\u0086\u0089\3\2\2\2"+
		"\u0087\u0085\3\2\2\2\u0087\u0088\3\2\2\2\u0088\u008a\3\2\2\2\u0089\u0087"+
		"\3\2\2\2\u008a\u008b\5H%\2\u008b\u008c\7\t\2\2\u008c\u008d\5H%\2\u008d"+
		"\u008e\5\22\n\2\u008e\21\3\2\2\2\u008f\u0095\5\24\13\2\u0090\u0095\5\26"+
		"\f\2\u0091\u0095\5\30\r\2\u0092\u0095\5\32\16\2\u0093\u0095\5\34\17\2"+
		"\u0094\u008f\3\2\2\2\u0094\u0090\3\2\2\2\u0094\u0091\3\2\2\2\u0094\u0092"+
		"\3\2\2\2\u0094\u0093\3\2\2\2\u0095\u0097\3\2\2\2\u0096\u0098\7\n\2\2\u0097"+
		"\u0096\3\2\2\2\u0097\u0098\3\2\2\2\u0098\23\3\2\2\2\u0099\u00a3\7\13\2"+
		"\2\u009a\u00a3\7\f\2\2\u009b\u00a3\7\r\2\2\u009c\u00a3\7\16\2\2\u009d"+
		"\u00a3\7\17\2\2\u009e\u00a3\7\20\2\2\u009f\u00a3\7\21\2\2\u00a0\u00a3"+
		"\5<\37\2\u00a1\u00a3\7\22\2\2\u00a2\u0099\3\2\2\2\u00a2\u009a\3\2\2\2"+
		"\u00a2\u009b\3\2\2\2\u00a2\u009c\3\2\2\2\u00a2\u009d\3\2\2\2\u00a2\u009e"+
		"\3\2\2\2\u00a2\u009f\3\2\2\2\u00a2\u00a0\3\2\2\2\u00a2\u00a1\3\2\2\2\u00a3"+
		"\25\3\2\2\2\u00a4\u00a5\7\23\2\2\u00a5\u00a6\5H%\2\u00a6\u00a7\7\24\2"+
		"\2\u00a7\u00a8\5H%\2\u00a8\u00a9\5\22\n\2\u00a9\u00aa\5H%\2\u00aa\u00ab"+
		"\7\25\2\2\u00ab\27\3\2\2\2\u00ac\u00ad\7\26\2\2\u00ad\u00ae\5H%\2\u00ae"+
		"\u00af\7\24\2\2\u00af\u00b0\5H%\2\u00b0\u00b1\5\22\n\2\u00b1\u00b2\5H"+
		"%\2\u00b2\u00b3\7\25\2\2\u00b3\31\3\2\2\2\u00b4\u00b5\5\60\31\2\u00b5"+
		"\33\3\2\2\2\u00b6\u00b7\7\24\2\2\u00b7\u00b8\5H%\2\u00b8\u00be\5\22\n"+
		"\2\u00b9\u00ba\5F$\2\u00ba\u00bb\5\22\n\2\u00bb\u00bd\3\2\2\2\u00bc\u00b9"+
		"\3\2\2\2\u00bd\u00c0\3\2\2\2\u00be\u00bc\3\2\2\2\u00be\u00bf\3\2\2\2\u00bf"+
		"\u00c1\3\2\2\2\u00c0\u00be\3\2\2\2\u00c1\u00c2\5H%\2\u00c2\u00c3\7\25"+
		"\2\2\u00c3\35\3\2\2\2\u00c4\u00c5\5\64\33\2\u00c5\37\3\2\2\2\u00c6\u00c7"+
		"\7\27\2\2\u00c7\u00ca\5H%\2\u00c8\u00cb\5\16\b\2\u00c9\u00cb\5.\30\2\u00ca"+
		"\u00c8\3\2\2\2\u00ca\u00c9\3\2\2\2\u00cb!\3\2\2\2\u00cc\u00cd\7\30\2\2"+
		"\u00cd\u00ce\5H%\2\u00ce\u00cf\5\16\b\2\u00cf\u00d0\5H%\2\u00d0\u00d4"+
		"\5\16\b\2\u00d1\u00d3\5\16\b\2\u00d2\u00d1\3\2\2\2\u00d3\u00d6\3\2\2\2"+
		"\u00d4\u00d2\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5\u00d7\3\2\2\2\u00d6\u00d4"+
		"\3\2\2\2\u00d7\u00d8\5H%\2\u00d8\u00d9\7\31\2\2\u00d9#\3\2\2\2\u00da\u00db"+
		"\7\30\2\2\u00db\u00dc\5H%\2\u00dc\u00dd\5&\24\2\u00dd\u00de\5H%\2\u00de"+
		"\u00df\5\16\b\2\u00df\u00e0\5H%\2\u00e0\u00e1\7\31\2\2\u00e1%\3\2\2\2"+
		"\u00e2\u00e3\7\32\2\2\u00e3\u00e4\5H%\2\u00e4\u00e5\5(\25\2\u00e5\'\3"+
		"\2\2\2\u00e6\u00ed\5*\26\2\u00e7\u00e8\5H%\2\u00e8\u00e9\7\6\2\2\u00e9"+
		"\u00ea\5*\26\2\u00ea\u00ec\3\2\2\2\u00eb\u00e7\3\2\2\2\u00ec\u00ef\3\2"+
		"\2\2\u00ed\u00eb\3\2\2\2\u00ed\u00ee\3\2\2\2\u00ee)\3\2\2\2\u00ef\u00ed"+
		"\3\2\2\2\u00f0\u00f6\5B\"\2\u00f1\u00f2\5H%\2\u00f2\u00f3\7\33\2\2\u00f3"+
		"\u00f4\5H%\2\u00f4\u00f5\5\22\n\2\u00f5\u00f7\3\2\2\2\u00f6\u00f1\3\2"+
		"\2\2\u00f6\u00f7\3\2\2\2\u00f7+\3\2\2\2\u00f8\u00f9\7\34\2\2\u00f9\u00fa"+
		"\5H%\2\u00fa\u00fb\7;\2\2\u00fb\u00fe\5H%\2\u00fc\u00ff\5\16\b\2\u00fd"+
		"\u00ff\5.\30\2\u00fe\u00fc\3\2\2\2\u00fe\u00fd\3\2\2\2\u00ff-\3\2\2\2"+
		"\u0100\u0105\5\66\34\2\u0101\u0105\5\62\32\2\u0102\u0105\5> \2\u0103\u0105"+
		"\58\35\2\u0104\u0100\3\2\2\2\u0104\u0101\3\2\2\2\u0104\u0102\3\2\2\2\u0104"+
		"\u0103\3\2\2\2\u0105/\3\2\2\2\u0106\u0107\5@!\2\u0107\61\3\2\2\2\u0108"+
		"\u010b\5@!\2\u0109\u010b\5D#\2\u010a\u0108\3\2\2\2\u010a\u0109\3\2\2\2"+
		"\u010b\63\3\2\2\2\u010c\u010d\5B\"\2\u010d\65\3\2\2\2\u010e\u010f\t\2"+
		"\2\2\u010f\67\3\2\2\2\u0110\u0111\t\3\2\2\u01119\3\2\2\2\u0112\u0113\7"+
		"\65\2\2\u0113;\3\2\2\2\u0114\u0119\7\66\2\2\u0115\u0118\5:\36\2\u0116"+
		"\u0118\7;\2\2\u0117\u0115\3\2\2\2\u0117\u0116\3\2\2\2\u0118\u011b\3\2"+
		"\2\2\u0119\u0117\3\2\2\2\u0119\u011a\3\2\2\2\u011a=\3\2\2\2\u011b\u0119"+
		"\3\2\2\2\u011c\u011f\5:\36\2\u011d\u011f\7;\2\2\u011e\u011c\3\2\2\2\u011e"+
		"\u011d\3\2\2\2\u011f\u0124\3\2\2\2\u0120\u0123\5:\36\2\u0121\u0123\7;"+
		"\2\2\u0122\u0120\3\2\2\2\u0122\u0121\3\2\2\2\u0123\u0126\3\2\2\2\u0124"+
		"\u0122\3\2\2\2\u0124\u0125\3\2\2\2\u0125\u0133\3\2\2\2\u0126\u0124\3\2"+
		"\2\2\u0127\u012a\7\3\2\2\u0128\u012b\5:\36\2\u0129\u012b\7;\2\2\u012a"+
		"\u0128\3\2\2\2\u012a\u0129\3\2\2\2\u012b\u0130\3\2\2\2\u012c\u012f\5:"+
		"\36\2\u012d\u012f\7;\2\2\u012e\u012c\3\2\2\2\u012e\u012d\3\2\2\2\u012f"+
		"\u0132\3\2\2\2\u0130\u012e\3\2\2\2\u0130\u0131\3\2\2\2\u0131\u0134\3\2"+
		"\2\2\u0132\u0130\3\2\2\2\u0133\u0127\3\2\2\2\u0133\u0134\3\2\2\2\u0134"+
		"?\3\2\2\2\u0135\u013d\7:\2\2\u0136\u013c\79\2\2\u0137\u013c\7:\2\2\u0138"+
		"\u013c\7\67\2\2\u0139\u013c\5:\36\2\u013a\u013c\7;\2\2\u013b\u0136\3\2"+
		"\2\2\u013b\u0137\3\2\2\2\u013b\u0138\3\2\2\2\u013b\u0139\3\2\2\2\u013b"+
		"\u013a\3\2\2\2\u013c\u013f\3\2\2\2\u013d\u013b\3\2\2\2\u013d\u013e\3\2"+
		"\2\2\u013eA\3\2\2\2\u013f\u013d\3\2\2\2\u0140\u0148\79\2\2\u0141\u0147"+
		"\79\2\2\u0142\u0147\7:\2\2\u0143\u0147\7\67\2\2\u0144\u0147\5:\36\2\u0145"+
		"\u0147\7;\2\2\u0146\u0141\3\2\2\2\u0146\u0142\3\2\2\2\u0146\u0143\3\2"+
		"\2\2\u0146\u0144\3\2\2\2\u0146\u0145\3\2\2\2\u0147\u014a\3\2\2\2\u0148"+
		"\u0146\3\2\2\2\u0148\u0149\3\2\2\2\u0149C\3\2\2\2\u014a\u0148\3\2\2\2"+
		"\u014b\u014f\78\2\2\u014c\u014e\78\2\2\u014d\u014c\3\2\2\2\u014e\u0151"+
		"\3\2\2\2\u014f\u014d\3\2\2\2\u014f\u0150\3\2\2\2\u0150\u0152\3\2\2\2\u0151"+
		"\u014f\3\2\2\2\u0152\u0153\78\2\2\u0153E\3\2\2\2\u0154\u0155\7<\2\2\u0155"+
		"\u0156\5H%\2\u0156G\3\2\2\2\u0157\u0159\7<\2\2\u0158\u0157\3\2\2\2\u0159"+
		"\u015c\3\2\2\2\u015a\u0158\3\2\2\2\u015a\u015b\3\2\2\2\u015bI\3\2\2\2"+
		"\u015c\u015a\3\2\2\2\"MWmz}\u0087\u0094\u0097\u00a2\u00be\u00ca\u00d4"+
		"\u00ed\u00f6\u00fe\u0104\u010a\u0117\u0119\u011e\u0122\u0124\u012a\u012e"+
		"\u0130\u0133\u013b\u013d\u0146\u0148\u014f\u015a";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}