# Generated from TILGrammarPy.g4 by ANTLR 4.9.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3;")
        buf.write("\u0149\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\3\2\7\2J\n")
        buf.write("\2\f\2\16\2M\13\2\3\3\3\3\3\3\3\4\3\4\3\4\3\4\5\4V\n\4")
        buf.write("\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3")
        buf.write("\7\3\7\3\7\3\7\3\7\7\7j\n\7\f\7\16\7m\13\7\3\7\3\7\3\7")
        buf.write("\3\7\3\7\3\b\3\b\3\b\3\b\3\b\5\by\n\b\3\b\5\b|\n\b\3\t")
        buf.write("\3\t\3\t\3\t\3\t\3\t\7\t\u0084\n\t\f\t\16\t\u0087\13\t")
        buf.write("\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\5\n\u0093\n\n")
        buf.write("\3\n\5\n\u0096\n\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13")
        buf.write("\3\13\3\13\5\13\u00a1\n\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f")
        buf.write("\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\17\3")
        buf.write("\17\3\17\3\17\3\17\3\17\7\17\u00bb\n\17\f\17\16\17\u00be")
        buf.write("\13\17\3\17\3\17\3\17\3\20\3\20\3\21\3\21\3\21\3\21\5")
        buf.write("\21\u00c9\n\21\3\22\3\22\3\22\3\22\3\22\3\22\7\22\u00d1")
        buf.write("\n\22\f\22\16\22\u00d4\13\22\3\22\3\22\3\22\3\23\3\23")
        buf.write("\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\25")
        buf.write("\3\25\3\25\3\25\3\25\7\25\u00ea\n\25\f\25\16\25\u00ed")
        buf.write("\13\25\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u00f5\n\26\3")
        buf.write("\27\3\27\3\27\3\27\3\27\3\27\5\27\u00fd\n\27\3\30\3\30")
        buf.write("\3\30\3\30\5\30\u0103\n\30\3\31\3\31\3\32\3\32\5\32\u0109")
        buf.write("\n\32\3\33\3\33\3\34\3\34\3\35\3\35\3\36\3\36\7\36\u0113")
        buf.write("\n\36\f\36\16\36\u0116\13\36\3\37\3\37\7\37\u011a\n\37")
        buf.write("\f\37\16\37\u011d\13\37\3\37\3\37\3\37\7\37\u0122\n\37")
        buf.write("\f\37\16\37\u0125\13\37\5\37\u0127\n\37\3 \3 \7 \u012b")
        buf.write("\n \f \16 \u012e\13 \3!\3!\7!\u0132\n!\f!\16!\u0135\13")
        buf.write("!\3\"\3\"\7\"\u0139\n\"\f\"\16\"\u013c\13\"\3\"\3\"\3")
        buf.write("#\3#\3#\3$\7$\u0144\n$\f$\16$\u0147\13$\3$\2\2%\2\4\6")
        buf.write("\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\66")
        buf.write("8:<>@BDF\2\6\3\2\34\60\5\2\7\7\22\22\61\63\3\29:\4\2\65")
        buf.write("\65\67:\2\u014f\2K\3\2\2\2\4N\3\2\2\2\6U\3\2\2\2\bW\3")
        buf.write("\2\2\2\n[\3\2\2\2\fc\3\2\2\2\16x\3\2\2\2\20}\3\2\2\2\22")
        buf.write("\u0092\3\2\2\2\24\u00a0\3\2\2\2\26\u00a2\3\2\2\2\30\u00aa")
        buf.write("\3\2\2\2\32\u00b2\3\2\2\2\34\u00b4\3\2\2\2\36\u00c2\3")
        buf.write("\2\2\2 \u00c4\3\2\2\2\"\u00ca\3\2\2\2$\u00d8\3\2\2\2&")
        buf.write("\u00e0\3\2\2\2(\u00e4\3\2\2\2*\u00ee\3\2\2\2,\u00f6\3")
        buf.write("\2\2\2.\u0102\3\2\2\2\60\u0104\3\2\2\2\62\u0108\3\2\2")
        buf.write("\2\64\u010a\3\2\2\2\66\u010c\3\2\2\28\u010e\3\2\2\2:\u0110")
        buf.write("\3\2\2\2<\u0117\3\2\2\2>\u0128\3\2\2\2@\u012f\3\2\2\2")
        buf.write("B\u0136\3\2\2\2D\u013f\3\2\2\2F\u0145\3\2\2\2HJ\5\4\3")
        buf.write("\2IH\3\2\2\2JM\3\2\2\2KI\3\2\2\2KL\3\2\2\2L\3\3\2\2\2")
        buf.write("MK\3\2\2\2NO\5\6\4\2OP\5\b\5\2P\5\3\2\2\2QV\5\n\6\2RV")
        buf.write("\5\f\7\2SV\5\16\b\2TV\5\20\t\2UQ\3\2\2\2UR\3\2\2\2US\3")
        buf.write("\2\2\2UT\3\2\2\2V\7\3\2\2\2WX\5F$\2XY\7\3\2\2YZ\5F$\2")
        buf.write("Z\t\3\2\2\2[\\\7\4\2\2\\]\5D#\2]^\5\60\31\2^_\5F$\2_`")
        buf.write("\7\5\2\2`a\5F$\2ab\5\22\n\2b\13\3\2\2\2ck\5\62\32\2de")
        buf.write("\5F$\2ef\7\6\2\2fg\5F$\2gh\5\62\32\2hj\3\2\2\2id\3\2\2")
        buf.write("\2jm\3\2\2\2ki\3\2\2\2kl\3\2\2\2ln\3\2\2\2mk\3\2\2\2n")
        buf.write("o\5F$\2op\7\7\2\2pq\5F$\2qr\5\22\n\2r\r\3\2\2\2sy\5 \21")
        buf.write("\2ty\5\36\20\2uy\5$\23\2vy\5,\27\2wy\5\"\22\2xs\3\2\2")
        buf.write("\2xt\3\2\2\2xu\3\2\2\2xv\3\2\2\2xw\3\2\2\2y{\3\2\2\2z")
        buf.write("|\7\b\2\2{z\3\2\2\2{|\3\2\2\2|\17\3\2\2\2}\u0085\5\64")
        buf.write("\33\2~\177\5F$\2\177\u0080\7\6\2\2\u0080\u0081\5F$\2\u0081")
        buf.write("\u0082\5\64\33\2\u0082\u0084\3\2\2\2\u0083~\3\2\2\2\u0084")
        buf.write("\u0087\3\2\2\2\u0085\u0083\3\2\2\2\u0085\u0086\3\2\2\2")
        buf.write("\u0086\u0088\3\2\2\2\u0087\u0085\3\2\2\2\u0088\u0089\5")
        buf.write("F$\2\u0089\u008a\7\t\2\2\u008a\u008b\5F$\2\u008b\u008c")
        buf.write("\5\22\n\2\u008c\21\3\2\2\2\u008d\u0093\5\24\13\2\u008e")
        buf.write("\u0093\5\26\f\2\u008f\u0093\5\30\r\2\u0090\u0093\5\32")
        buf.write("\16\2\u0091\u0093\5\34\17\2\u0092\u008d\3\2\2\2\u0092")
        buf.write("\u008e\3\2\2\2\u0092\u008f\3\2\2\2\u0092\u0090\3\2\2\2")
        buf.write("\u0092\u0091\3\2\2\2\u0093\u0095\3\2\2\2\u0094\u0096\7")
        buf.write("\n\2\2\u0095\u0094\3\2\2\2\u0095\u0096\3\2\2\2\u0096\23")
        buf.write("\3\2\2\2\u0097\u00a1\7\13\2\2\u0098\u00a1\7\f\2\2\u0099")
        buf.write("\u00a1\7\r\2\2\u009a\u00a1\7\16\2\2\u009b\u00a1\7\17\2")
        buf.write("\2\u009c\u00a1\7\20\2\2\u009d\u00a1\7\21\2\2\u009e\u00a1")
        buf.write("\5:\36\2\u009f\u00a1\7\22\2\2\u00a0\u0097\3\2\2\2\u00a0")
        buf.write("\u0098\3\2\2\2\u00a0\u0099\3\2\2\2\u00a0\u009a\3\2\2\2")
        buf.write("\u00a0\u009b\3\2\2\2\u00a0\u009c\3\2\2\2\u00a0\u009d\3")
        buf.write("\2\2\2\u00a0\u009e\3\2\2\2\u00a0\u009f\3\2\2\2\u00a1\25")
        buf.write("\3\2\2\2\u00a2\u00a3\7\23\2\2\u00a3\u00a4\5F$\2\u00a4")
        buf.write("\u00a5\7\24\2\2\u00a5\u00a6\5F$\2\u00a6\u00a7\5\22\n\2")
        buf.write("\u00a7\u00a8\5F$\2\u00a8\u00a9\7\25\2\2\u00a9\27\3\2\2")
        buf.write("\2\u00aa\u00ab\7\26\2\2\u00ab\u00ac\5F$\2\u00ac\u00ad")
        buf.write("\7\24\2\2\u00ad\u00ae\5F$\2\u00ae\u00af\5\22\n\2\u00af")
        buf.write("\u00b0\5F$\2\u00b0\u00b1\7\25\2\2\u00b1\31\3\2\2\2\u00b2")
        buf.write("\u00b3\5\60\31\2\u00b3\33\3\2\2\2\u00b4\u00b5\7\24\2\2")
        buf.write("\u00b5\u00b6\5F$\2\u00b6\u00bc\5\22\n\2\u00b7\u00b8\5")
        buf.write("D#\2\u00b8\u00b9\5\22\n\2\u00b9\u00bb\3\2\2\2\u00ba\u00b7")
        buf.write("\3\2\2\2\u00bb\u00be\3\2\2\2\u00bc\u00ba\3\2\2\2\u00bc")
        buf.write("\u00bd\3\2\2\2\u00bd\u00bf\3\2\2\2\u00be\u00bc\3\2\2\2")
        buf.write("\u00bf\u00c0\5F$\2\u00c0\u00c1\7\25\2\2\u00c1\35\3\2\2")
        buf.write("\2\u00c2\u00c3\5\64\33\2\u00c3\37\3\2\2\2\u00c4\u00c5")
        buf.write("\7\27\2\2\u00c5\u00c8\5F$\2\u00c6\u00c9\5\16\b\2\u00c7")
        buf.write("\u00c9\5.\30\2\u00c8\u00c6\3\2\2\2\u00c8\u00c7\3\2\2\2")
        buf.write("\u00c9!\3\2\2\2\u00ca\u00cb\7\30\2\2\u00cb\u00cc\5F$\2")
        buf.write("\u00cc\u00cd\5\16\b\2\u00cd\u00ce\5F$\2\u00ce\u00d2\5")
        buf.write("\16\b\2\u00cf\u00d1\5\16\b\2\u00d0\u00cf\3\2\2\2\u00d1")
        buf.write("\u00d4\3\2\2\2\u00d2\u00d0\3\2\2\2\u00d2\u00d3\3\2\2\2")
        buf.write("\u00d3\u00d5\3\2\2\2\u00d4\u00d2\3\2\2\2\u00d5\u00d6\5")
        buf.write("F$\2\u00d6\u00d7\7\31\2\2\u00d7#\3\2\2\2\u00d8\u00d9\7")
        buf.write("\30\2\2\u00d9\u00da\5F$\2\u00da\u00db\5&\24\2\u00db\u00dc")
        buf.write("\5F$\2\u00dc\u00dd\5\16\b\2\u00dd\u00de\5F$\2\u00de\u00df")
        buf.write("\7\31\2\2\u00df%\3\2\2\2\u00e0\u00e1\7\27\2\2\u00e1\u00e2")
        buf.write("\5F$\2\u00e2\u00e3\5(\25\2\u00e3\'\3\2\2\2\u00e4\u00eb")
        buf.write("\5*\26\2\u00e5\u00e6\5F$\2\u00e6\u00e7\7\6\2\2\u00e7\u00e8")
        buf.write("\5*\26\2\u00e8\u00ea\3\2\2\2\u00e9\u00e5\3\2\2\2\u00ea")
        buf.write("\u00ed\3\2\2\2\u00eb\u00e9\3\2\2\2\u00eb\u00ec\3\2\2\2")
        buf.write("\u00ec)\3\2\2\2\u00ed\u00eb\3\2\2\2\u00ee\u00f4\5@!\2")
        buf.write("\u00ef\u00f0\5F$\2\u00f0\u00f1\7\32\2\2\u00f1\u00f2\5")
        buf.write("F$\2\u00f2\u00f3\5\22\n\2\u00f3\u00f5\3\2\2\2\u00f4\u00ef")
        buf.write("\3\2\2\2\u00f4\u00f5\3\2\2\2\u00f5+\3\2\2\2\u00f6\u00f7")
        buf.write("\7\33\2\2\u00f7\u00f8\5F$\2\u00f8\u00f9\7:\2\2\u00f9\u00fc")
        buf.write("\5F$\2\u00fa\u00fd\5\16\b\2\u00fb\u00fd\5.\30\2\u00fc")
        buf.write("\u00fa\3\2\2\2\u00fc\u00fb\3\2\2\2\u00fd-\3\2\2\2\u00fe")
        buf.write("\u0103\5\66\34\2\u00ff\u0103\5\62\32\2\u0100\u0103\5<")
        buf.write("\37\2\u0101\u0103\58\35\2\u0102\u00fe\3\2\2\2\u0102\u00ff")
        buf.write("\3\2\2\2\u0102\u0100\3\2\2\2\u0102\u0101\3\2\2\2\u0103")
        buf.write("/\3\2\2\2\u0104\u0105\5> \2\u0105\61\3\2\2\2\u0106\u0109")
        buf.write("\5> \2\u0107\u0109\5B\"\2\u0108\u0106\3\2\2\2\u0108\u0107")
        buf.write("\3\2\2\2\u0109\63\3\2\2\2\u010a\u010b\5@!\2\u010b\65\3")
        buf.write("\2\2\2\u010c\u010d\t\2\2\2\u010d\67\3\2\2\2\u010e\u010f")
        buf.write("\t\3\2\2\u010f9\3\2\2\2\u0110\u0114\7\64\2\2\u0111\u0113")
        buf.write("\t\4\2\2\u0112\u0111\3\2\2\2\u0113\u0116\3\2\2\2\u0114")
        buf.write("\u0112\3\2\2\2\u0114\u0115\3\2\2\2\u0115;\3\2\2\2\u0116")
        buf.write("\u0114\3\2\2\2\u0117\u011b\t\4\2\2\u0118\u011a\t\4\2\2")
        buf.write("\u0119\u0118\3\2\2\2\u011a\u011d\3\2\2\2\u011b\u0119\3")
        buf.write("\2\2\2\u011b\u011c\3\2\2\2\u011c\u0126\3\2\2\2\u011d\u011b")
        buf.write("\3\2\2\2\u011e\u011f\7\3\2\2\u011f\u0123\t\4\2\2\u0120")
        buf.write("\u0122\t\4\2\2\u0121\u0120\3\2\2\2\u0122\u0125\3\2\2\2")
        buf.write("\u0123\u0121\3\2\2\2\u0123\u0124\3\2\2\2\u0124\u0127\3")
        buf.write("\2\2\2\u0125\u0123\3\2\2\2\u0126\u011e\3\2\2\2\u0126\u0127")
        buf.write("\3\2\2\2\u0127=\3\2\2\2\u0128\u012c\78\2\2\u0129\u012b")
        buf.write("\t\5\2\2\u012a\u0129\3\2\2\2\u012b\u012e\3\2\2\2\u012c")
        buf.write("\u012a\3\2\2\2\u012c\u012d\3\2\2\2\u012d?\3\2\2\2\u012e")
        buf.write("\u012c\3\2\2\2\u012f\u0133\7\67\2\2\u0130\u0132\t\5\2")
        buf.write("\2\u0131\u0130\3\2\2\2\u0132\u0135\3\2\2\2\u0133\u0131")
        buf.write("\3\2\2\2\u0133\u0134\3\2\2\2\u0134A\3\2\2\2\u0135\u0133")
        buf.write("\3\2\2\2\u0136\u013a\7\66\2\2\u0137\u0139\7\66\2\2\u0138")
        buf.write("\u0137\3\2\2\2\u0139\u013c\3\2\2\2\u013a\u0138\3\2\2\2")
        buf.write("\u013a\u013b\3\2\2\2\u013b\u013d\3\2\2\2\u013c\u013a\3")
        buf.write("\2\2\2\u013d\u013e\7\66\2\2\u013eC\3\2\2\2\u013f\u0140")
        buf.write("\7;\2\2\u0140\u0141\5F$\2\u0141E\3\2\2\2\u0142\u0144\7")
        buf.write(";\2\2\u0143\u0142\3\2\2\2\u0144\u0147\3\2\2\2\u0145\u0143")
        buf.write("\3\2\2\2\u0145\u0146\3\2\2\2\u0146G\3\2\2\2\u0147\u0145")
        buf.write("\3\2\2\2\33KUkx{\u0085\u0092\u0095\u00a0\u00bc\u00c8\u00d2")
        buf.write("\u00eb\u00f4\u00fc\u0102\u0108\u0114\u011b\u0123\u0126")
        buf.write("\u012c\u0133\u013a\u0145")
        return buf.getvalue()


class TILGrammarPyParser ( Parser ):

    grammarFileName = "TILGrammarPy.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'.'", "'TypeDef'", "':='", "','", "'/'", 
                     "'@wt'", "'->'", "'@tw'", "'Bool'", "'Indiv'", "'Time'", 
                     "'String'", "'World'", "'Real'", "'Int'", "'*'", "'List'", 
                     "'('", "')'", "'Tuple'", "'''", "'['", "']'", "':'", 
                     "'^'", "'ForAll'", "'Exist'", "'Every'", "'Some'", 
                     "'No'", "'True'", "'False'", "'And'", "'Or'", "'Not'", 
                     "'Implies'", "'Sing'", "'Sub'", "'Tr'", "'TrueC'", 
                     "'FalseC'", "'ImproperC'", "'TrueP'", "'FalseP'", "'UndefP'", 
                     "'ToInt'", "'+'", "'-'", "'='", "'Any'", "'_'", "'%'", 
                     "<INVALID>", "<INVALID>", "'0'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "LOWERCASE_LETTER", "UPPERCASE_LETTER", 
                      "ZERO", "NONZERO_DIGIT", "Whitespace_character" ]

    RULE_start = 0
    RULE_sentence = 1
    RULE_sentence_content = 2
    RULE_termination = 3
    RULE_type_definition = 4
    RULE_entity_definition = 5
    RULE_construction = 6
    RULE_global_variable_definition = 7
    RULE_data_type = 8
    RULE_embeded_type = 9
    RULE_list_type = 10
    RULE_touple_type = 11
    RULE_user_type = 12
    RULE_enclosed_data_type = 13
    RULE_variable = 14
    RULE_trivialisation = 15
    RULE_composition = 16
    RULE_closure = 17
    RULE_lambda_variables = 18
    RULE_typed_variables = 19
    RULE_typed_variable = 20
    RULE_n_execution = 21
    RULE_entity = 22
    RULE_type_name = 23
    RULE_entity_name = 24
    RULE_variable_name = 25
    RULE_keyword = 26
    RULE_symbols = 27
    RULE_anything = 28
    RULE_number = 29
    RULE_upperletter_name = 30
    RULE_lowerletter_name = 31
    RULE_quoted_name = 32
    RULE_whitespace = 33
    RULE_optional_whitespace = 34

    ruleNames =  [ "start", "sentence", "sentence_content", "termination", 
                   "type_definition", "entity_definition", "construction", 
                   "global_variable_definition", "data_type", "embeded_type", 
                   "list_type", "touple_type", "user_type", "enclosed_data_type", 
                   "variable", "trivialisation", "composition", "closure", 
                   "lambda_variables", "typed_variables", "typed_variable", 
                   "n_execution", "entity", "type_name", "entity_name", 
                   "variable_name", "keyword", "symbols", "anything", "number", 
                   "upperletter_name", "lowerletter_name", "quoted_name", 
                   "whitespace", "optional_whitespace" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    T__15=16
    T__16=17
    T__17=18
    T__18=19
    T__19=20
    T__20=21
    T__21=22
    T__22=23
    T__23=24
    T__24=25
    T__25=26
    T__26=27
    T__27=28
    T__28=29
    T__29=30
    T__30=31
    T__31=32
    T__32=33
    T__33=34
    T__34=35
    T__35=36
    T__36=37
    T__37=38
    T__38=39
    T__39=40
    T__40=41
    T__41=42
    T__42=43
    T__43=44
    T__44=45
    T__45=46
    T__46=47
    T__47=48
    T__48=49
    T__49=50
    T__50=51
    T__51=52
    LOWERCASE_LETTER=53
    UPPERCASE_LETTER=54
    ZERO=55
    NONZERO_DIGIT=56
    Whitespace_character=57

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.9.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class StartContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def sentence(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.SentenceContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.SentenceContext,i)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_start

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStart" ):
                listener.enterStart(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStart" ):
                listener.exitStart(self)




    def start(self):

        localctx = TILGrammarPyParser.StartContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_start)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 73
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TILGrammarPyParser.T__1) | (1 << TILGrammarPyParser.T__20) | (1 << TILGrammarPyParser.T__21) | (1 << TILGrammarPyParser.T__24) | (1 << TILGrammarPyParser.T__51) | (1 << TILGrammarPyParser.LOWERCASE_LETTER) | (1 << TILGrammarPyParser.UPPERCASE_LETTER))) != 0):
                self.state = 70
                self.sentence()
                self.state = 75
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SentenceContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def sentence_content(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Sentence_contentContext,0)


        def termination(self):
            return self.getTypedRuleContext(TILGrammarPyParser.TerminationContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_sentence

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSentence" ):
                listener.enterSentence(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSentence" ):
                listener.exitSentence(self)




    def sentence(self):

        localctx = TILGrammarPyParser.SentenceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_sentence)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 76
            self.sentence_content()
            self.state = 77
            self.termination()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Sentence_contentContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_definition(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Type_definitionContext,0)


        def entity_definition(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Entity_definitionContext,0)


        def construction(self):
            return self.getTypedRuleContext(TILGrammarPyParser.ConstructionContext,0)


        def global_variable_definition(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Global_variable_definitionContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_sentence_content

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSentence_content" ):
                listener.enterSentence_content(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSentence_content" ):
                listener.exitSentence_content(self)




    def sentence_content(self):

        localctx = TILGrammarPyParser.Sentence_contentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_sentence_content)
        try:
            self.state = 83
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 79
                self.type_definition()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 80
                self.entity_definition()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 81
                self.construction()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 82
                self.global_variable_definition()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TerminationContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def optional_whitespace(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Optional_whitespaceContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,i)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_termination

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTermination" ):
                listener.enterTermination(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTermination" ):
                listener.exitTermination(self)




    def termination(self):

        localctx = TILGrammarPyParser.TerminationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_termination)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 85
            self.optional_whitespace()
            self.state = 86
            self.match(TILGrammarPyParser.T__0)
            self.state = 87
            self.optional_whitespace()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_definitionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def whitespace(self):
            return self.getTypedRuleContext(TILGrammarPyParser.WhitespaceContext,0)


        def type_name(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Type_nameContext,0)


        def optional_whitespace(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Optional_whitespaceContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,i)


        def data_type(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Data_typeContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_type_definition

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_definition" ):
                listener.enterType_definition(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_definition" ):
                listener.exitType_definition(self)




    def type_definition(self):

        localctx = TILGrammarPyParser.Type_definitionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_type_definition)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 89
            self.match(TILGrammarPyParser.T__1)
            self.state = 90
            self.whitespace()
            self.state = 91
            self.type_name()
            self.state = 92
            self.optional_whitespace()
            self.state = 93
            self.match(TILGrammarPyParser.T__2)
            self.state = 94
            self.optional_whitespace()
            self.state = 95
            self.data_type()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Entity_definitionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def entity_name(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Entity_nameContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Entity_nameContext,i)


        def optional_whitespace(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Optional_whitespaceContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,i)


        def data_type(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Data_typeContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_entity_definition

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEntity_definition" ):
                listener.enterEntity_definition(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEntity_definition" ):
                listener.exitEntity_definition(self)




    def entity_definition(self):

        localctx = TILGrammarPyParser.Entity_definitionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_entity_definition)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 97
            self.entity_name()
            self.state = 105
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,2,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 98
                    self.optional_whitespace()
                    self.state = 99
                    self.match(TILGrammarPyParser.T__3)
                    self.state = 100
                    self.optional_whitespace()
                    self.state = 101
                    self.entity_name() 
                self.state = 107
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,2,self._ctx)

            self.state = 108
            self.optional_whitespace()
            self.state = 109
            self.match(TILGrammarPyParser.T__4)
            self.state = 110
            self.optional_whitespace()
            self.state = 111
            self.data_type()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ConstructionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def trivialisation(self):
            return self.getTypedRuleContext(TILGrammarPyParser.TrivialisationContext,0)


        def variable(self):
            return self.getTypedRuleContext(TILGrammarPyParser.VariableContext,0)


        def closure(self):
            return self.getTypedRuleContext(TILGrammarPyParser.ClosureContext,0)


        def n_execution(self):
            return self.getTypedRuleContext(TILGrammarPyParser.N_executionContext,0)


        def composition(self):
            return self.getTypedRuleContext(TILGrammarPyParser.CompositionContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_construction

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConstruction" ):
                listener.enterConstruction(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConstruction" ):
                listener.exitConstruction(self)




    def construction(self):

        localctx = TILGrammarPyParser.ConstructionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_construction)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 118
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.state = 113
                self.trivialisation()
                pass

            elif la_ == 2:
                self.state = 114
                self.variable()
                pass

            elif la_ == 3:
                self.state = 115
                self.closure()
                pass

            elif la_ == 4:
                self.state = 116
                self.n_execution()
                pass

            elif la_ == 5:
                self.state = 117
                self.composition()
                pass


            self.state = 121
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.state = 120
                self.match(TILGrammarPyParser.T__5)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Global_variable_definitionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def variable_name(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Variable_nameContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Variable_nameContext,i)


        def optional_whitespace(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Optional_whitespaceContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,i)


        def data_type(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Data_typeContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_global_variable_definition

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterGlobal_variable_definition" ):
                listener.enterGlobal_variable_definition(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitGlobal_variable_definition" ):
                listener.exitGlobal_variable_definition(self)




    def global_variable_definition(self):

        localctx = TILGrammarPyParser.Global_variable_definitionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_global_variable_definition)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 123
            self.variable_name()
            self.state = 131
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,5,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 124
                    self.optional_whitespace()
                    self.state = 125
                    self.match(TILGrammarPyParser.T__3)
                    self.state = 126
                    self.optional_whitespace()
                    self.state = 127
                    self.variable_name() 
                self.state = 133
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,5,self._ctx)

            self.state = 134
            self.optional_whitespace()
            self.state = 135
            self.match(TILGrammarPyParser.T__6)
            self.state = 136
            self.optional_whitespace()
            self.state = 137
            self.data_type()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Data_typeContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def embeded_type(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Embeded_typeContext,0)


        def list_type(self):
            return self.getTypedRuleContext(TILGrammarPyParser.List_typeContext,0)


        def touple_type(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Touple_typeContext,0)


        def user_type(self):
            return self.getTypedRuleContext(TILGrammarPyParser.User_typeContext,0)


        def enclosed_data_type(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Enclosed_data_typeContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_data_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterData_type" ):
                listener.enterData_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitData_type" ):
                listener.exitData_type(self)




    def data_type(self):

        localctx = TILGrammarPyParser.Data_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_data_type)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 144
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [TILGrammarPyParser.T__8, TILGrammarPyParser.T__9, TILGrammarPyParser.T__10, TILGrammarPyParser.T__11, TILGrammarPyParser.T__12, TILGrammarPyParser.T__13, TILGrammarPyParser.T__14, TILGrammarPyParser.T__15, TILGrammarPyParser.T__49]:
                self.state = 139
                self.embeded_type()
                pass
            elif token in [TILGrammarPyParser.T__16]:
                self.state = 140
                self.list_type()
                pass
            elif token in [TILGrammarPyParser.T__19]:
                self.state = 141
                self.touple_type()
                pass
            elif token in [TILGrammarPyParser.UPPERCASE_LETTER]:
                self.state = 142
                self.user_type()
                pass
            elif token in [TILGrammarPyParser.T__17]:
                self.state = 143
                self.enclosed_data_type()
                pass
            else:
                raise NoViableAltException(self)

            self.state = 147
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==TILGrammarPyParser.T__7:
                self.state = 146
                self.match(TILGrammarPyParser.T__7)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Embeded_typeContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def anything(self):
            return self.getTypedRuleContext(TILGrammarPyParser.AnythingContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_embeded_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEmbeded_type" ):
                listener.enterEmbeded_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEmbeded_type" ):
                listener.exitEmbeded_type(self)




    def embeded_type(self):

        localctx = TILGrammarPyParser.Embeded_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_embeded_type)
        try:
            self.state = 158
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [TILGrammarPyParser.T__8]:
                self.enterOuterAlt(localctx, 1)
                self.state = 149
                self.match(TILGrammarPyParser.T__8)
                pass
            elif token in [TILGrammarPyParser.T__9]:
                self.enterOuterAlt(localctx, 2)
                self.state = 150
                self.match(TILGrammarPyParser.T__9)
                pass
            elif token in [TILGrammarPyParser.T__10]:
                self.enterOuterAlt(localctx, 3)
                self.state = 151
                self.match(TILGrammarPyParser.T__10)
                pass
            elif token in [TILGrammarPyParser.T__11]:
                self.enterOuterAlt(localctx, 4)
                self.state = 152
                self.match(TILGrammarPyParser.T__11)
                pass
            elif token in [TILGrammarPyParser.T__12]:
                self.enterOuterAlt(localctx, 5)
                self.state = 153
                self.match(TILGrammarPyParser.T__12)
                pass
            elif token in [TILGrammarPyParser.T__13]:
                self.enterOuterAlt(localctx, 6)
                self.state = 154
                self.match(TILGrammarPyParser.T__13)
                pass
            elif token in [TILGrammarPyParser.T__14]:
                self.enterOuterAlt(localctx, 7)
                self.state = 155
                self.match(TILGrammarPyParser.T__14)
                pass
            elif token in [TILGrammarPyParser.T__49]:
                self.enterOuterAlt(localctx, 8)
                self.state = 156
                self.anything()
                pass
            elif token in [TILGrammarPyParser.T__15]:
                self.enterOuterAlt(localctx, 9)
                self.state = 157
                self.match(TILGrammarPyParser.T__15)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class List_typeContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def optional_whitespace(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Optional_whitespaceContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,i)


        def data_type(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Data_typeContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_list_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterList_type" ):
                listener.enterList_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitList_type" ):
                listener.exitList_type(self)




    def list_type(self):

        localctx = TILGrammarPyParser.List_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_list_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 160
            self.match(TILGrammarPyParser.T__16)
            self.state = 161
            self.optional_whitespace()
            self.state = 162
            self.match(TILGrammarPyParser.T__17)
            self.state = 163
            self.optional_whitespace()
            self.state = 164
            self.data_type()
            self.state = 165
            self.optional_whitespace()
            self.state = 166
            self.match(TILGrammarPyParser.T__18)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Touple_typeContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def optional_whitespace(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Optional_whitespaceContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,i)


        def data_type(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Data_typeContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_touple_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTouple_type" ):
                listener.enterTouple_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTouple_type" ):
                listener.exitTouple_type(self)




    def touple_type(self):

        localctx = TILGrammarPyParser.Touple_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_touple_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 168
            self.match(TILGrammarPyParser.T__19)
            self.state = 169
            self.optional_whitespace()
            self.state = 170
            self.match(TILGrammarPyParser.T__17)
            self.state = 171
            self.optional_whitespace()
            self.state = 172
            self.data_type()
            self.state = 173
            self.optional_whitespace()
            self.state = 174
            self.match(TILGrammarPyParser.T__18)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class User_typeContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def type_name(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Type_nameContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_user_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUser_type" ):
                listener.enterUser_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUser_type" ):
                listener.exitUser_type(self)




    def user_type(self):

        localctx = TILGrammarPyParser.User_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_user_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 176
            self.type_name()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Enclosed_data_typeContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def optional_whitespace(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Optional_whitespaceContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,i)


        def data_type(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Data_typeContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Data_typeContext,i)


        def whitespace(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.WhitespaceContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.WhitespaceContext,i)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_enclosed_data_type

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEnclosed_data_type" ):
                listener.enterEnclosed_data_type(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEnclosed_data_type" ):
                listener.exitEnclosed_data_type(self)




    def enclosed_data_type(self):

        localctx = TILGrammarPyParser.Enclosed_data_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_enclosed_data_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 178
            self.match(TILGrammarPyParser.T__17)
            self.state = 179
            self.optional_whitespace()
            self.state = 180
            self.data_type()
            self.state = 186
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,9,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 181
                    self.whitespace()
                    self.state = 182
                    self.data_type() 
                self.state = 188
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,9,self._ctx)

            self.state = 189
            self.optional_whitespace()
            self.state = 190
            self.match(TILGrammarPyParser.T__18)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VariableContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def variable_name(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Variable_nameContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_variable

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVariable" ):
                listener.enterVariable(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVariable" ):
                listener.exitVariable(self)




    def variable(self):

        localctx = TILGrammarPyParser.VariableContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_variable)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 192
            self.variable_name()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TrivialisationContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def optional_whitespace(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,0)


        def construction(self):
            return self.getTypedRuleContext(TILGrammarPyParser.ConstructionContext,0)


        def entity(self):
            return self.getTypedRuleContext(TILGrammarPyParser.EntityContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_trivialisation

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTrivialisation" ):
                listener.enterTrivialisation(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTrivialisation" ):
                listener.exitTrivialisation(self)




    def trivialisation(self):

        localctx = TILGrammarPyParser.TrivialisationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_trivialisation)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 194
            self.match(TILGrammarPyParser.T__20)
            self.state = 195
            self.optional_whitespace()
            self.state = 198
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [TILGrammarPyParser.T__20, TILGrammarPyParser.T__21, TILGrammarPyParser.T__24, TILGrammarPyParser.LOWERCASE_LETTER]:
                self.state = 196
                self.construction()
                pass
            elif token in [TILGrammarPyParser.T__4, TILGrammarPyParser.T__15, TILGrammarPyParser.T__25, TILGrammarPyParser.T__26, TILGrammarPyParser.T__27, TILGrammarPyParser.T__28, TILGrammarPyParser.T__29, TILGrammarPyParser.T__30, TILGrammarPyParser.T__31, TILGrammarPyParser.T__32, TILGrammarPyParser.T__33, TILGrammarPyParser.T__34, TILGrammarPyParser.T__35, TILGrammarPyParser.T__36, TILGrammarPyParser.T__37, TILGrammarPyParser.T__38, TILGrammarPyParser.T__39, TILGrammarPyParser.T__40, TILGrammarPyParser.T__41, TILGrammarPyParser.T__42, TILGrammarPyParser.T__43, TILGrammarPyParser.T__44, TILGrammarPyParser.T__45, TILGrammarPyParser.T__46, TILGrammarPyParser.T__47, TILGrammarPyParser.T__48, TILGrammarPyParser.T__51, TILGrammarPyParser.UPPERCASE_LETTER, TILGrammarPyParser.ZERO, TILGrammarPyParser.NONZERO_DIGIT]:
                self.state = 197
                self.entity()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CompositionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def optional_whitespace(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Optional_whitespaceContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,i)


        def construction(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.ConstructionContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.ConstructionContext,i)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_composition

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComposition" ):
                listener.enterComposition(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComposition" ):
                listener.exitComposition(self)




    def composition(self):

        localctx = TILGrammarPyParser.CompositionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_composition)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 200
            self.match(TILGrammarPyParser.T__21)
            self.state = 201
            self.optional_whitespace()
            self.state = 202
            self.construction()
            self.state = 203
            self.optional_whitespace()
            self.state = 204
            self.construction()
            self.state = 208
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TILGrammarPyParser.T__20) | (1 << TILGrammarPyParser.T__21) | (1 << TILGrammarPyParser.T__24) | (1 << TILGrammarPyParser.LOWERCASE_LETTER))) != 0):
                self.state = 205
                self.construction()
                self.state = 210
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 211
            self.optional_whitespace()
            self.state = 212
            self.match(TILGrammarPyParser.T__22)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ClosureContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def optional_whitespace(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Optional_whitespaceContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,i)


        def lambda_variables(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Lambda_variablesContext,0)


        def construction(self):
            return self.getTypedRuleContext(TILGrammarPyParser.ConstructionContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_closure

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClosure" ):
                listener.enterClosure(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClosure" ):
                listener.exitClosure(self)




    def closure(self):

        localctx = TILGrammarPyParser.ClosureContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_closure)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 214
            self.match(TILGrammarPyParser.T__21)
            self.state = 215
            self.optional_whitespace()
            self.state = 216
            self.lambda_variables()
            self.state = 217
            self.optional_whitespace()
            self.state = 218
            self.construction()
            self.state = 219
            self.optional_whitespace()
            self.state = 220
            self.match(TILGrammarPyParser.T__22)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Lambda_variablesContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def optional_whitespace(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,0)


        def typed_variables(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Typed_variablesContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_lambda_variables

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLambda_variables" ):
                listener.enterLambda_variables(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLambda_variables" ):
                listener.exitLambda_variables(self)




    def lambda_variables(self):

        localctx = TILGrammarPyParser.Lambda_variablesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_lambda_variables)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 222
            self.match(TILGrammarPyParser.T__20)
            self.state = 223
            self.optional_whitespace()
            self.state = 224
            self.typed_variables()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Typed_variablesContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def typed_variable(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Typed_variableContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Typed_variableContext,i)


        def optional_whitespace(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Optional_whitespaceContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,i)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_typed_variables

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTyped_variables" ):
                listener.enterTyped_variables(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTyped_variables" ):
                listener.exitTyped_variables(self)




    def typed_variables(self):

        localctx = TILGrammarPyParser.Typed_variablesContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_typed_variables)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 226
            self.typed_variable()
            self.state = 233
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,12,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 227
                    self.optional_whitespace()
                    self.state = 228
                    self.match(TILGrammarPyParser.T__3)
                    self.state = 229
                    self.typed_variable() 
                self.state = 235
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,12,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Typed_variableContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def lowerletter_name(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Lowerletter_nameContext,0)


        def optional_whitespace(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Optional_whitespaceContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,i)


        def data_type(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Data_typeContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_typed_variable

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTyped_variable" ):
                listener.enterTyped_variable(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTyped_variable" ):
                listener.exitTyped_variable(self)




    def typed_variable(self):

        localctx = TILGrammarPyParser.Typed_variableContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_typed_variable)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 236
            self.lowerletter_name()
            self.state = 242
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,13,self._ctx)
            if la_ == 1:
                self.state = 237
                self.optional_whitespace()
                self.state = 238
                self.match(TILGrammarPyParser.T__23)
                self.state = 239
                self.optional_whitespace()
                self.state = 240
                self.data_type()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class N_executionContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def optional_whitespace(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TILGrammarPyParser.Optional_whitespaceContext)
            else:
                return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,i)


        def NONZERO_DIGIT(self):
            return self.getToken(TILGrammarPyParser.NONZERO_DIGIT, 0)

        def construction(self):
            return self.getTypedRuleContext(TILGrammarPyParser.ConstructionContext,0)


        def entity(self):
            return self.getTypedRuleContext(TILGrammarPyParser.EntityContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_n_execution

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterN_execution" ):
                listener.enterN_execution(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitN_execution" ):
                listener.exitN_execution(self)




    def n_execution(self):

        localctx = TILGrammarPyParser.N_executionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_n_execution)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 244
            self.match(TILGrammarPyParser.T__24)
            self.state = 245
            self.optional_whitespace()
            self.state = 246
            self.match(TILGrammarPyParser.NONZERO_DIGIT)
            self.state = 247
            self.optional_whitespace()
            self.state = 250
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [TILGrammarPyParser.T__20, TILGrammarPyParser.T__21, TILGrammarPyParser.T__24, TILGrammarPyParser.LOWERCASE_LETTER]:
                self.state = 248
                self.construction()
                pass
            elif token in [TILGrammarPyParser.T__4, TILGrammarPyParser.T__15, TILGrammarPyParser.T__25, TILGrammarPyParser.T__26, TILGrammarPyParser.T__27, TILGrammarPyParser.T__28, TILGrammarPyParser.T__29, TILGrammarPyParser.T__30, TILGrammarPyParser.T__31, TILGrammarPyParser.T__32, TILGrammarPyParser.T__33, TILGrammarPyParser.T__34, TILGrammarPyParser.T__35, TILGrammarPyParser.T__36, TILGrammarPyParser.T__37, TILGrammarPyParser.T__38, TILGrammarPyParser.T__39, TILGrammarPyParser.T__40, TILGrammarPyParser.T__41, TILGrammarPyParser.T__42, TILGrammarPyParser.T__43, TILGrammarPyParser.T__44, TILGrammarPyParser.T__45, TILGrammarPyParser.T__46, TILGrammarPyParser.T__47, TILGrammarPyParser.T__48, TILGrammarPyParser.T__51, TILGrammarPyParser.UPPERCASE_LETTER, TILGrammarPyParser.ZERO, TILGrammarPyParser.NONZERO_DIGIT]:
                self.state = 249
                self.entity()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class EntityContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def keyword(self):
            return self.getTypedRuleContext(TILGrammarPyParser.KeywordContext,0)


        def entity_name(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Entity_nameContext,0)


        def number(self):
            return self.getTypedRuleContext(TILGrammarPyParser.NumberContext,0)


        def symbols(self):
            return self.getTypedRuleContext(TILGrammarPyParser.SymbolsContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_entity

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEntity" ):
                listener.enterEntity(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEntity" ):
                listener.exitEntity(self)




    def entity(self):

        localctx = TILGrammarPyParser.EntityContext(self, self._ctx, self.state)
        self.enterRule(localctx, 44, self.RULE_entity)
        try:
            self.state = 256
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [TILGrammarPyParser.T__25, TILGrammarPyParser.T__26, TILGrammarPyParser.T__27, TILGrammarPyParser.T__28, TILGrammarPyParser.T__29, TILGrammarPyParser.T__30, TILGrammarPyParser.T__31, TILGrammarPyParser.T__32, TILGrammarPyParser.T__33, TILGrammarPyParser.T__34, TILGrammarPyParser.T__35, TILGrammarPyParser.T__36, TILGrammarPyParser.T__37, TILGrammarPyParser.T__38, TILGrammarPyParser.T__39, TILGrammarPyParser.T__40, TILGrammarPyParser.T__41, TILGrammarPyParser.T__42, TILGrammarPyParser.T__43, TILGrammarPyParser.T__44, TILGrammarPyParser.T__45]:
                self.enterOuterAlt(localctx, 1)
                self.state = 252
                self.keyword()
                pass
            elif token in [TILGrammarPyParser.T__51, TILGrammarPyParser.UPPERCASE_LETTER]:
                self.enterOuterAlt(localctx, 2)
                self.state = 253
                self.entity_name()
                pass
            elif token in [TILGrammarPyParser.ZERO, TILGrammarPyParser.NONZERO_DIGIT]:
                self.enterOuterAlt(localctx, 3)
                self.state = 254
                self.number()
                pass
            elif token in [TILGrammarPyParser.T__4, TILGrammarPyParser.T__15, TILGrammarPyParser.T__46, TILGrammarPyParser.T__47, TILGrammarPyParser.T__48]:
                self.enterOuterAlt(localctx, 4)
                self.state = 255
                self.symbols()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Type_nameContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def upperletter_name(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Upperletter_nameContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_type_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterType_name" ):
                listener.enterType_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitType_name" ):
                listener.exitType_name(self)




    def type_name(self):

        localctx = TILGrammarPyParser.Type_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_type_name)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 258
            self.upperletter_name()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Entity_nameContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def upperletter_name(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Upperletter_nameContext,0)


        def quoted_name(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Quoted_nameContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_entity_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEntity_name" ):
                listener.enterEntity_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEntity_name" ):
                listener.exitEntity_name(self)




    def entity_name(self):

        localctx = TILGrammarPyParser.Entity_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_entity_name)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 262
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [TILGrammarPyParser.UPPERCASE_LETTER]:
                self.state = 260
                self.upperletter_name()
                pass
            elif token in [TILGrammarPyParser.T__51]:
                self.state = 261
                self.quoted_name()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Variable_nameContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def lowerletter_name(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Lowerletter_nameContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_variable_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterVariable_name" ):
                listener.enterVariable_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitVariable_name" ):
                listener.exitVariable_name(self)




    def variable_name(self):

        localctx = TILGrammarPyParser.Variable_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 50, self.RULE_variable_name)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 264
            self.lowerletter_name()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class KeywordContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_keyword

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterKeyword" ):
                listener.enterKeyword(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitKeyword" ):
                listener.exitKeyword(self)




    def keyword(self):

        localctx = TILGrammarPyParser.KeywordContext(self, self._ctx, self.state)
        self.enterRule(localctx, 52, self.RULE_keyword)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 266
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TILGrammarPyParser.T__25) | (1 << TILGrammarPyParser.T__26) | (1 << TILGrammarPyParser.T__27) | (1 << TILGrammarPyParser.T__28) | (1 << TILGrammarPyParser.T__29) | (1 << TILGrammarPyParser.T__30) | (1 << TILGrammarPyParser.T__31) | (1 << TILGrammarPyParser.T__32) | (1 << TILGrammarPyParser.T__33) | (1 << TILGrammarPyParser.T__34) | (1 << TILGrammarPyParser.T__35) | (1 << TILGrammarPyParser.T__36) | (1 << TILGrammarPyParser.T__37) | (1 << TILGrammarPyParser.T__38) | (1 << TILGrammarPyParser.T__39) | (1 << TILGrammarPyParser.T__40) | (1 << TILGrammarPyParser.T__41) | (1 << TILGrammarPyParser.T__42) | (1 << TILGrammarPyParser.T__43) | (1 << TILGrammarPyParser.T__44) | (1 << TILGrammarPyParser.T__45))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class SymbolsContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_symbols

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSymbols" ):
                listener.enterSymbols(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSymbols" ):
                listener.exitSymbols(self)




    def symbols(self):

        localctx = TILGrammarPyParser.SymbolsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_symbols)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 268
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TILGrammarPyParser.T__4) | (1 << TILGrammarPyParser.T__15) | (1 << TILGrammarPyParser.T__46) | (1 << TILGrammarPyParser.T__47) | (1 << TILGrammarPyParser.T__48))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class AnythingContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ZERO(self, i:int=None):
            if i is None:
                return self.getTokens(TILGrammarPyParser.ZERO)
            else:
                return self.getToken(TILGrammarPyParser.ZERO, i)

        def NONZERO_DIGIT(self, i:int=None):
            if i is None:
                return self.getTokens(TILGrammarPyParser.NONZERO_DIGIT)
            else:
                return self.getToken(TILGrammarPyParser.NONZERO_DIGIT, i)

        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_anything

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAnything" ):
                listener.enterAnything(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAnything" ):
                listener.exitAnything(self)




    def anything(self):

        localctx = TILGrammarPyParser.AnythingContext(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_anything)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 270
            self.match(TILGrammarPyParser.T__49)
            self.state = 274
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==TILGrammarPyParser.ZERO or _la==TILGrammarPyParser.NONZERO_DIGIT:
                self.state = 271
                _la = self._input.LA(1)
                if not(_la==TILGrammarPyParser.ZERO or _la==TILGrammarPyParser.NONZERO_DIGIT):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 276
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class NumberContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ZERO(self, i:int=None):
            if i is None:
                return self.getTokens(TILGrammarPyParser.ZERO)
            else:
                return self.getToken(TILGrammarPyParser.ZERO, i)

        def NONZERO_DIGIT(self, i:int=None):
            if i is None:
                return self.getTokens(TILGrammarPyParser.NONZERO_DIGIT)
            else:
                return self.getToken(TILGrammarPyParser.NONZERO_DIGIT, i)

        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_number

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNumber" ):
                listener.enterNumber(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNumber" ):
                listener.exitNumber(self)




    def number(self):

        localctx = TILGrammarPyParser.NumberContext(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_number)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 277
            _la = self._input.LA(1)
            if not(_la==TILGrammarPyParser.ZERO or _la==TILGrammarPyParser.NONZERO_DIGIT):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 281
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==TILGrammarPyParser.ZERO or _la==TILGrammarPyParser.NONZERO_DIGIT:
                self.state = 278
                _la = self._input.LA(1)
                if not(_la==TILGrammarPyParser.ZERO or _la==TILGrammarPyParser.NONZERO_DIGIT):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 283
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 292
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,20,self._ctx)
            if la_ == 1:
                self.state = 284
                self.match(TILGrammarPyParser.T__0)
                self.state = 285
                _la = self._input.LA(1)
                if not(_la==TILGrammarPyParser.ZERO or _la==TILGrammarPyParser.NONZERO_DIGIT):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 289
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==TILGrammarPyParser.ZERO or _la==TILGrammarPyParser.NONZERO_DIGIT:
                    self.state = 286
                    _la = self._input.LA(1)
                    if not(_la==TILGrammarPyParser.ZERO or _la==TILGrammarPyParser.NONZERO_DIGIT):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 291
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Upperletter_nameContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def UPPERCASE_LETTER(self, i:int=None):
            if i is None:
                return self.getTokens(TILGrammarPyParser.UPPERCASE_LETTER)
            else:
                return self.getToken(TILGrammarPyParser.UPPERCASE_LETTER, i)

        def LOWERCASE_LETTER(self, i:int=None):
            if i is None:
                return self.getTokens(TILGrammarPyParser.LOWERCASE_LETTER)
            else:
                return self.getToken(TILGrammarPyParser.LOWERCASE_LETTER, i)

        def ZERO(self, i:int=None):
            if i is None:
                return self.getTokens(TILGrammarPyParser.ZERO)
            else:
                return self.getToken(TILGrammarPyParser.ZERO, i)

        def NONZERO_DIGIT(self, i:int=None):
            if i is None:
                return self.getTokens(TILGrammarPyParser.NONZERO_DIGIT)
            else:
                return self.getToken(TILGrammarPyParser.NONZERO_DIGIT, i)

        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_upperletter_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUpperletter_name" ):
                listener.enterUpperletter_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUpperletter_name" ):
                listener.exitUpperletter_name(self)




    def upperletter_name(self):

        localctx = TILGrammarPyParser.Upperletter_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 60, self.RULE_upperletter_name)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 294
            self.match(TILGrammarPyParser.UPPERCASE_LETTER)
            self.state = 298
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,21,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 295
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TILGrammarPyParser.T__50) | (1 << TILGrammarPyParser.LOWERCASE_LETTER) | (1 << TILGrammarPyParser.UPPERCASE_LETTER) | (1 << TILGrammarPyParser.ZERO) | (1 << TILGrammarPyParser.NONZERO_DIGIT))) != 0)):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume() 
                self.state = 300
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,21,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Lowerletter_nameContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LOWERCASE_LETTER(self, i:int=None):
            if i is None:
                return self.getTokens(TILGrammarPyParser.LOWERCASE_LETTER)
            else:
                return self.getToken(TILGrammarPyParser.LOWERCASE_LETTER, i)

        def UPPERCASE_LETTER(self, i:int=None):
            if i is None:
                return self.getTokens(TILGrammarPyParser.UPPERCASE_LETTER)
            else:
                return self.getToken(TILGrammarPyParser.UPPERCASE_LETTER, i)

        def ZERO(self, i:int=None):
            if i is None:
                return self.getTokens(TILGrammarPyParser.ZERO)
            else:
                return self.getToken(TILGrammarPyParser.ZERO, i)

        def NONZERO_DIGIT(self, i:int=None):
            if i is None:
                return self.getTokens(TILGrammarPyParser.NONZERO_DIGIT)
            else:
                return self.getToken(TILGrammarPyParser.NONZERO_DIGIT, i)

        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_lowerletter_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLowerletter_name" ):
                listener.enterLowerletter_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLowerletter_name" ):
                listener.exitLowerletter_name(self)




    def lowerletter_name(self):

        localctx = TILGrammarPyParser.Lowerletter_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 62, self.RULE_lowerletter_name)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 301
            self.match(TILGrammarPyParser.LOWERCASE_LETTER)
            self.state = 305
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,22,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 302
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TILGrammarPyParser.T__50) | (1 << TILGrammarPyParser.LOWERCASE_LETTER) | (1 << TILGrammarPyParser.UPPERCASE_LETTER) | (1 << TILGrammarPyParser.ZERO) | (1 << TILGrammarPyParser.NONZERO_DIGIT))) != 0)):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume() 
                self.state = 307
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,22,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Quoted_nameContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_quoted_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterQuoted_name" ):
                listener.enterQuoted_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitQuoted_name" ):
                listener.exitQuoted_name(self)




    def quoted_name(self):

        localctx = TILGrammarPyParser.Quoted_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 64, self.RULE_quoted_name)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 308
            self.match(TILGrammarPyParser.T__51)
            self.state = 312
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,23,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 309
                    self.match(TILGrammarPyParser.T__51) 
                self.state = 314
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,23,self._ctx)

            self.state = 315
            self.match(TILGrammarPyParser.T__51)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class WhitespaceContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Whitespace_character(self):
            return self.getToken(TILGrammarPyParser.Whitespace_character, 0)

        def optional_whitespace(self):
            return self.getTypedRuleContext(TILGrammarPyParser.Optional_whitespaceContext,0)


        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_whitespace

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWhitespace" ):
                listener.enterWhitespace(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWhitespace" ):
                listener.exitWhitespace(self)




    def whitespace(self):

        localctx = TILGrammarPyParser.WhitespaceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 66, self.RULE_whitespace)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 317
            self.match(TILGrammarPyParser.Whitespace_character)
            self.state = 318
            self.optional_whitespace()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Optional_whitespaceContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Whitespace_character(self, i:int=None):
            if i is None:
                return self.getTokens(TILGrammarPyParser.Whitespace_character)
            else:
                return self.getToken(TILGrammarPyParser.Whitespace_character, i)

        def getRuleIndex(self):
            return TILGrammarPyParser.RULE_optional_whitespace

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOptional_whitespace" ):
                listener.enterOptional_whitespace(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOptional_whitespace" ):
                listener.exitOptional_whitespace(self)




    def optional_whitespace(self):

        localctx = TILGrammarPyParser.Optional_whitespaceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 68, self.RULE_optional_whitespace)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 323
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==TILGrammarPyParser.Whitespace_character:
                self.state = 320
                self.match(TILGrammarPyParser.Whitespace_character)
                self.state = 325
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





