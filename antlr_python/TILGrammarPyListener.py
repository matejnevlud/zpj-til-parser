# Generated from TILGrammarPy.g4 by ANTLR 4.9.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .TILGrammarPyParser import TILGrammarPyParser
else:
    from TILGrammarPyParser import TILGrammarPyParser

# This class defines a complete listener for a parse tree produced by TILGrammarPyParser.
class TILGrammarPyListener(ParseTreeListener):

    # Enter a parse tree produced by TILGrammarPyParser#start.
    def enterStart(self, ctx:TILGrammarPyParser.StartContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#start.
    def exitStart(self, ctx:TILGrammarPyParser.StartContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#sentence.
    def enterSentence(self, ctx:TILGrammarPyParser.SentenceContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#sentence.
    def exitSentence(self, ctx:TILGrammarPyParser.SentenceContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#sentence_content.
    def enterSentence_content(self, ctx:TILGrammarPyParser.Sentence_contentContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#sentence_content.
    def exitSentence_content(self, ctx:TILGrammarPyParser.Sentence_contentContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#termination.
    def enterTermination(self, ctx:TILGrammarPyParser.TerminationContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#termination.
    def exitTermination(self, ctx:TILGrammarPyParser.TerminationContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#type_definition.
    def enterType_definition(self, ctx:TILGrammarPyParser.Type_definitionContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#type_definition.
    def exitType_definition(self, ctx:TILGrammarPyParser.Type_definitionContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#entity_definition.
    def enterEntity_definition(self, ctx:TILGrammarPyParser.Entity_definitionContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#entity_definition.
    def exitEntity_definition(self, ctx:TILGrammarPyParser.Entity_definitionContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#construction.
    def enterConstruction(self, ctx:TILGrammarPyParser.ConstructionContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#construction.
    def exitConstruction(self, ctx:TILGrammarPyParser.ConstructionContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#global_variable_definition.
    def enterGlobal_variable_definition(self, ctx:TILGrammarPyParser.Global_variable_definitionContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#global_variable_definition.
    def exitGlobal_variable_definition(self, ctx:TILGrammarPyParser.Global_variable_definitionContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#data_type.
    def enterData_type(self, ctx:TILGrammarPyParser.Data_typeContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#data_type.
    def exitData_type(self, ctx:TILGrammarPyParser.Data_typeContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#embeded_type.
    def enterEmbeded_type(self, ctx:TILGrammarPyParser.Embeded_typeContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#embeded_type.
    def exitEmbeded_type(self, ctx:TILGrammarPyParser.Embeded_typeContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#list_type.
    def enterList_type(self, ctx:TILGrammarPyParser.List_typeContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#list_type.
    def exitList_type(self, ctx:TILGrammarPyParser.List_typeContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#touple_type.
    def enterTouple_type(self, ctx:TILGrammarPyParser.Touple_typeContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#touple_type.
    def exitTouple_type(self, ctx:TILGrammarPyParser.Touple_typeContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#user_type.
    def enterUser_type(self, ctx:TILGrammarPyParser.User_typeContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#user_type.
    def exitUser_type(self, ctx:TILGrammarPyParser.User_typeContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#enclosed_data_type.
    def enterEnclosed_data_type(self, ctx:TILGrammarPyParser.Enclosed_data_typeContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#enclosed_data_type.
    def exitEnclosed_data_type(self, ctx:TILGrammarPyParser.Enclosed_data_typeContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#variable.
    def enterVariable(self, ctx:TILGrammarPyParser.VariableContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#variable.
    def exitVariable(self, ctx:TILGrammarPyParser.VariableContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#trivialisation.
    def enterTrivialisation(self, ctx:TILGrammarPyParser.TrivialisationContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#trivialisation.
    def exitTrivialisation(self, ctx:TILGrammarPyParser.TrivialisationContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#composition.
    def enterComposition(self, ctx:TILGrammarPyParser.CompositionContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#composition.
    def exitComposition(self, ctx:TILGrammarPyParser.CompositionContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#closure.
    def enterClosure(self, ctx:TILGrammarPyParser.ClosureContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#closure.
    def exitClosure(self, ctx:TILGrammarPyParser.ClosureContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#lambda_variables.
    def enterLambda_variables(self, ctx:TILGrammarPyParser.Lambda_variablesContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#lambda_variables.
    def exitLambda_variables(self, ctx:TILGrammarPyParser.Lambda_variablesContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#typed_variables.
    def enterTyped_variables(self, ctx:TILGrammarPyParser.Typed_variablesContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#typed_variables.
    def exitTyped_variables(self, ctx:TILGrammarPyParser.Typed_variablesContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#typed_variable.
    def enterTyped_variable(self, ctx:TILGrammarPyParser.Typed_variableContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#typed_variable.
    def exitTyped_variable(self, ctx:TILGrammarPyParser.Typed_variableContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#n_execution.
    def enterN_execution(self, ctx:TILGrammarPyParser.N_executionContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#n_execution.
    def exitN_execution(self, ctx:TILGrammarPyParser.N_executionContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#entity.
    def enterEntity(self, ctx:TILGrammarPyParser.EntityContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#entity.
    def exitEntity(self, ctx:TILGrammarPyParser.EntityContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#type_name.
    def enterType_name(self, ctx:TILGrammarPyParser.Type_nameContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#type_name.
    def exitType_name(self, ctx:TILGrammarPyParser.Type_nameContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#entity_name.
    def enterEntity_name(self, ctx:TILGrammarPyParser.Entity_nameContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#entity_name.
    def exitEntity_name(self, ctx:TILGrammarPyParser.Entity_nameContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#variable_name.
    def enterVariable_name(self, ctx:TILGrammarPyParser.Variable_nameContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#variable_name.
    def exitVariable_name(self, ctx:TILGrammarPyParser.Variable_nameContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#keyword.
    def enterKeyword(self, ctx:TILGrammarPyParser.KeywordContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#keyword.
    def exitKeyword(self, ctx:TILGrammarPyParser.KeywordContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#symbols.
    def enterSymbols(self, ctx:TILGrammarPyParser.SymbolsContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#symbols.
    def exitSymbols(self, ctx:TILGrammarPyParser.SymbolsContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#anything.
    def enterAnything(self, ctx:TILGrammarPyParser.AnythingContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#anything.
    def exitAnything(self, ctx:TILGrammarPyParser.AnythingContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#number.
    def enterNumber(self, ctx:TILGrammarPyParser.NumberContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#number.
    def exitNumber(self, ctx:TILGrammarPyParser.NumberContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#upperletter_name.
    def enterUpperletter_name(self, ctx:TILGrammarPyParser.Upperletter_nameContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#upperletter_name.
    def exitUpperletter_name(self, ctx:TILGrammarPyParser.Upperletter_nameContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#lowerletter_name.
    def enterLowerletter_name(self, ctx:TILGrammarPyParser.Lowerletter_nameContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#lowerletter_name.
    def exitLowerletter_name(self, ctx:TILGrammarPyParser.Lowerletter_nameContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#quoted_name.
    def enterQuoted_name(self, ctx:TILGrammarPyParser.Quoted_nameContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#quoted_name.
    def exitQuoted_name(self, ctx:TILGrammarPyParser.Quoted_nameContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#whitespace.
    def enterWhitespace(self, ctx:TILGrammarPyParser.WhitespaceContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#whitespace.
    def exitWhitespace(self, ctx:TILGrammarPyParser.WhitespaceContext):
        pass


    # Enter a parse tree produced by TILGrammarPyParser#optional_whitespace.
    def enterOptional_whitespace(self, ctx:TILGrammarPyParser.Optional_whitespaceContext):
        pass

    # Exit a parse tree produced by TILGrammarPyParser#optional_whitespace.
    def exitOptional_whitespace(self, ctx:TILGrammarPyParser.Optional_whitespaceContext):
        pass



del TILGrammarPyParser