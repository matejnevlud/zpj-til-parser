// Generated from /Users/matejnevlud/GitHub/zpj-til-parser/antlr_python/TILGrammarPy.g4 by ANTLR 4.8
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class TILGrammarPyParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.8", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		T__45=46, T__46=47, T__47=48, T__48=49, T__49=50, T__50=51, T__51=52, 
		LOWERCASE_LETTER=53, UPPERCASE_LETTER=54, ZERO=55, NONZERO_DIGIT=56, SPACE=57, 
		WHITESPACE_CHARACTER=58;
	public static final int
		RULE_start = 0, RULE_sentence = 1, RULE_sentence_content = 2, RULE_termination = 3, 
		RULE_type_definition = 4, RULE_entity_definition = 5, RULE_construction = 6, 
		RULE_global_variable_definition = 7, RULE_data_type = 8, RULE_embeded_type = 9, 
		RULE_list_type = 10, RULE_touple_type = 11, RULE_user_type = 12, RULE_enclosed_data_type = 13, 
		RULE_variable = 14, RULE_trivialisation = 15, RULE_composition = 16, RULE_closure = 17, 
		RULE_lambda_variables = 18, RULE_typed_variables = 19, RULE_typed_variable = 20, 
		RULE_n_execution = 21, RULE_entity = 22, RULE_type_name = 23, RULE_entity_name = 24, 
		RULE_variable_name = 25, RULE_keyword = 26, RULE_symbols = 27, RULE_anything = 28, 
		RULE_number = 29, RULE_upperletter_name = 30, RULE_lowerletter_name = 31, 
		RULE_quoted_name = 32, RULE_whitespace = 33, RULE_optional_whitespace = 34;
	private static String[] makeRuleNames() {
		return new String[] {
			"start", "sentence", "sentence_content", "termination", "type_definition", 
			"entity_definition", "construction", "global_variable_definition", "data_type", 
			"embeded_type", "list_type", "touple_type", "user_type", "enclosed_data_type", 
			"variable", "trivialisation", "composition", "closure", "lambda_variables", 
			"typed_variables", "typed_variable", "n_execution", "entity", "type_name", 
			"entity_name", "variable_name", "keyword", "symbols", "anything", "number", 
			"upperletter_name", "lowerletter_name", "quoted_name", "whitespace", 
			"optional_whitespace"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'.'", "'TypeDef'", "':='", "','", "'/'", "'@wt'", "'->'", "'@tw'", 
			"'Bool'", "'Indiv'", "'Time'", "'String'", "'World'", "'Real'", "'Int'", 
			"'*'", "'List'", "'('", "')'", "'Tuple'", "'''", "'['", "']'", "':'", 
			"'^'", "'ForAll'", "'Exist'", "'Every'", "'Some'", "'No'", "'True'", 
			"'False'", "'And'", "'Or'", "'Not'", "'Implies'", "'Sing'", "'Sub'", 
			"'Tr'", "'TrueC'", "'FalseC'", "'ImproperC'", "'TrueP'", "'FalseP'", 
			"'UndefP'", "'ToInt'", "'+'", "'-'", "'='", "'Any'", "'_'", "'%'", null, 
			null, "'0'", null, "' '"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, "LOWERCASE_LETTER", "UPPERCASE_LETTER", 
			"ZERO", "NONZERO_DIGIT", "SPACE", "WHITESPACE_CHARACTER"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "TILGrammarPy.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public TILGrammarPyParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class StartContext extends ParserRuleContext {
		public List<SentenceContext> sentence() {
			return getRuleContexts(SentenceContext.class);
		}
		public SentenceContext sentence(int i) {
			return getRuleContext(SentenceContext.class,i);
		}
		public StartContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_start; }
	}

	public final StartContext start() throws RecognitionException {
		StartContext _localctx = new StartContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_start);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(73);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__20) | (1L << T__21) | (1L << T__24) | (1L << T__51) | (1L << LOWERCASE_LETTER) | (1L << UPPERCASE_LETTER))) != 0)) {
				{
				{
				setState(70);
				sentence();
				}
				}
				setState(75);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SentenceContext extends ParserRuleContext {
		public Sentence_contentContext sentence_content() {
			return getRuleContext(Sentence_contentContext.class,0);
		}
		public TerminationContext termination() {
			return getRuleContext(TerminationContext.class,0);
		}
		public SentenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentence; }
	}

	public final SentenceContext sentence() throws RecognitionException {
		SentenceContext _localctx = new SentenceContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_sentence);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(76);
			sentence_content();
			setState(77);
			termination();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Sentence_contentContext extends ParserRuleContext {
		public Type_definitionContext type_definition() {
			return getRuleContext(Type_definitionContext.class,0);
		}
		public Entity_definitionContext entity_definition() {
			return getRuleContext(Entity_definitionContext.class,0);
		}
		public ConstructionContext construction() {
			return getRuleContext(ConstructionContext.class,0);
		}
		public Global_variable_definitionContext global_variable_definition() {
			return getRuleContext(Global_variable_definitionContext.class,0);
		}
		public Sentence_contentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sentence_content; }
	}

	public final Sentence_contentContext sentence_content() throws RecognitionException {
		Sentence_contentContext _localctx = new Sentence_contentContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_sentence_content);
		try {
			setState(83);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(79);
				type_definition();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(80);
				entity_definition();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(81);
				construction();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(82);
				global_variable_definition();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TerminationContext extends ParserRuleContext {
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public TerminationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_termination; }
	}

	public final TerminationContext termination() throws RecognitionException {
		TerminationContext _localctx = new TerminationContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_termination);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(85);
			optional_whitespace();
			setState(86);
			match(T__0);
			setState(87);
			optional_whitespace();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_definitionContext extends ParserRuleContext {
		public WhitespaceContext whitespace() {
			return getRuleContext(WhitespaceContext.class,0);
		}
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public Type_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_definition; }
	}

	public final Type_definitionContext type_definition() throws RecognitionException {
		Type_definitionContext _localctx = new Type_definitionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_type_definition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(89);
			match(T__1);
			setState(90);
			whitespace();
			setState(91);
			type_name();
			setState(92);
			optional_whitespace();
			setState(93);
			match(T__2);
			setState(94);
			optional_whitespace();
			setState(95);
			data_type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Entity_definitionContext extends ParserRuleContext {
		public List<Entity_nameContext> entity_name() {
			return getRuleContexts(Entity_nameContext.class);
		}
		public Entity_nameContext entity_name(int i) {
			return getRuleContext(Entity_nameContext.class,i);
		}
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public Entity_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entity_definition; }
	}

	public final Entity_definitionContext entity_definition() throws RecognitionException {
		Entity_definitionContext _localctx = new Entity_definitionContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_entity_definition);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(97);
			entity_name();
			setState(105);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(98);
					optional_whitespace();
					setState(99);
					match(T__3);
					setState(100);
					optional_whitespace();
					setState(101);
					entity_name();
					}
					} 
				}
				setState(107);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			setState(108);
			optional_whitespace();
			setState(109);
			match(T__4);
			setState(110);
			optional_whitespace();
			setState(111);
			data_type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructionContext extends ParserRuleContext {
		public TrivialisationContext trivialisation() {
			return getRuleContext(TrivialisationContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public ClosureContext closure() {
			return getRuleContext(ClosureContext.class,0);
		}
		public N_executionContext n_execution() {
			return getRuleContext(N_executionContext.class,0);
		}
		public CompositionContext composition() {
			return getRuleContext(CompositionContext.class,0);
		}
		public ConstructionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_construction; }
	}

	public final ConstructionContext construction() throws RecognitionException {
		ConstructionContext _localctx = new ConstructionContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_construction);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(118);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				{
				setState(113);
				trivialisation();
				}
				break;
			case 2:
				{
				setState(114);
				variable();
				}
				break;
			case 3:
				{
				setState(115);
				closure();
				}
				break;
			case 4:
				{
				setState(116);
				n_execution();
				}
				break;
			case 5:
				{
				setState(117);
				composition();
				}
				break;
			}
			setState(121);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				{
				setState(120);
				match(T__5);
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Global_variable_definitionContext extends ParserRuleContext {
		public List<Variable_nameContext> variable_name() {
			return getRuleContexts(Variable_nameContext.class);
		}
		public Variable_nameContext variable_name(int i) {
			return getRuleContext(Variable_nameContext.class,i);
		}
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public Global_variable_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_global_variable_definition; }
	}

	public final Global_variable_definitionContext global_variable_definition() throws RecognitionException {
		Global_variable_definitionContext _localctx = new Global_variable_definitionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_global_variable_definition);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(123);
			variable_name();
			setState(131);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(124);
					optional_whitespace();
					setState(125);
					match(T__3);
					setState(126);
					optional_whitespace();
					setState(127);
					variable_name();
					}
					} 
				}
				setState(133);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,5,_ctx);
			}
			setState(134);
			optional_whitespace();
			setState(135);
			match(T__6);
			setState(136);
			optional_whitespace();
			setState(137);
			data_type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Data_typeContext extends ParserRuleContext {
		public Embeded_typeContext embeded_type() {
			return getRuleContext(Embeded_typeContext.class,0);
		}
		public List_typeContext list_type() {
			return getRuleContext(List_typeContext.class,0);
		}
		public Touple_typeContext touple_type() {
			return getRuleContext(Touple_typeContext.class,0);
		}
		public User_typeContext user_type() {
			return getRuleContext(User_typeContext.class,0);
		}
		public Enclosed_data_typeContext enclosed_data_type() {
			return getRuleContext(Enclosed_data_typeContext.class,0);
		}
		public Data_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_data_type; }
	}

	public final Data_typeContext data_type() throws RecognitionException {
		Data_typeContext _localctx = new Data_typeContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_data_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(144);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__8:
			case T__9:
			case T__10:
			case T__11:
			case T__12:
			case T__13:
			case T__14:
			case T__15:
			case T__49:
				{
				setState(139);
				embeded_type();
				}
				break;
			case T__16:
				{
				setState(140);
				list_type();
				}
				break;
			case T__19:
				{
				setState(141);
				touple_type();
				}
				break;
			case UPPERCASE_LETTER:
				{
				setState(142);
				user_type();
				}
				break;
			case T__17:
				{
				setState(143);
				enclosed_data_type();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(147);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__7) {
				{
				setState(146);
				match(T__7);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Embeded_typeContext extends ParserRuleContext {
		public AnythingContext anything() {
			return getRuleContext(AnythingContext.class,0);
		}
		public Embeded_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_embeded_type; }
	}

	public final Embeded_typeContext embeded_type() throws RecognitionException {
		Embeded_typeContext _localctx = new Embeded_typeContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_embeded_type);
		try {
			setState(158);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__8:
				enterOuterAlt(_localctx, 1);
				{
				setState(149);
				match(T__8);
				}
				break;
			case T__9:
				enterOuterAlt(_localctx, 2);
				{
				setState(150);
				match(T__9);
				}
				break;
			case T__10:
				enterOuterAlt(_localctx, 3);
				{
				setState(151);
				match(T__10);
				}
				break;
			case T__11:
				enterOuterAlt(_localctx, 4);
				{
				setState(152);
				match(T__11);
				}
				break;
			case T__12:
				enterOuterAlt(_localctx, 5);
				{
				setState(153);
				match(T__12);
				}
				break;
			case T__13:
				enterOuterAlt(_localctx, 6);
				{
				setState(154);
				match(T__13);
				}
				break;
			case T__14:
				enterOuterAlt(_localctx, 7);
				{
				setState(155);
				match(T__14);
				}
				break;
			case T__49:
				enterOuterAlt(_localctx, 8);
				{
				setState(156);
				anything();
				}
				break;
			case T__15:
				enterOuterAlt(_localctx, 9);
				{
				setState(157);
				match(T__15);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class List_typeContext extends ParserRuleContext {
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public List_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_list_type; }
	}

	public final List_typeContext list_type() throws RecognitionException {
		List_typeContext _localctx = new List_typeContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_list_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(160);
			match(T__16);
			setState(161);
			optional_whitespace();
			setState(162);
			match(T__17);
			setState(163);
			optional_whitespace();
			setState(164);
			data_type();
			setState(165);
			optional_whitespace();
			setState(166);
			match(T__18);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Touple_typeContext extends ParserRuleContext {
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public Touple_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_touple_type; }
	}

	public final Touple_typeContext touple_type() throws RecognitionException {
		Touple_typeContext _localctx = new Touple_typeContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_touple_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(168);
			match(T__19);
			setState(169);
			optional_whitespace();
			setState(170);
			match(T__17);
			setState(171);
			optional_whitespace();
			setState(172);
			data_type();
			setState(173);
			optional_whitespace();
			setState(174);
			match(T__18);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class User_typeContext extends ParserRuleContext {
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public User_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_user_type; }
	}

	public final User_typeContext user_type() throws RecognitionException {
		User_typeContext _localctx = new User_typeContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_user_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(176);
			type_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Enclosed_data_typeContext extends ParserRuleContext {
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public List<Data_typeContext> data_type() {
			return getRuleContexts(Data_typeContext.class);
		}
		public Data_typeContext data_type(int i) {
			return getRuleContext(Data_typeContext.class,i);
		}
		public List<WhitespaceContext> whitespace() {
			return getRuleContexts(WhitespaceContext.class);
		}
		public WhitespaceContext whitespace(int i) {
			return getRuleContext(WhitespaceContext.class,i);
		}
		public Enclosed_data_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_enclosed_data_type; }
	}

	public final Enclosed_data_typeContext enclosed_data_type() throws RecognitionException {
		Enclosed_data_typeContext _localctx = new Enclosed_data_typeContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_enclosed_data_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(178);
			match(T__17);
			setState(179);
			optional_whitespace();
			setState(180);
			data_type();
			setState(186);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==WHITESPACE_CHARACTER) {
				{
				{
				setState(181);
				whitespace();
				setState(182);
				data_type();
				}
				}
				setState(188);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(189);
			optional_whitespace();
			setState(190);
			match(T__18);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public Variable_nameContext variable_name() {
			return getRuleContext(Variable_nameContext.class,0);
		}
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_variable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			variable_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TrivialisationContext extends ParserRuleContext {
		public Optional_whitespaceContext optional_whitespace() {
			return getRuleContext(Optional_whitespaceContext.class,0);
		}
		public ConstructionContext construction() {
			return getRuleContext(ConstructionContext.class,0);
		}
		public EntityContext entity() {
			return getRuleContext(EntityContext.class,0);
		}
		public TrivialisationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_trivialisation; }
	}

	public final TrivialisationContext trivialisation() throws RecognitionException {
		TrivialisationContext _localctx = new TrivialisationContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_trivialisation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(194);
			match(T__20);
			setState(195);
			optional_whitespace();
			setState(198);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__20:
			case T__21:
			case T__24:
			case LOWERCASE_LETTER:
				{
				setState(196);
				construction();
				}
				break;
			case T__4:
			case T__15:
			case T__25:
			case T__26:
			case T__27:
			case T__28:
			case T__29:
			case T__30:
			case T__31:
			case T__32:
			case T__33:
			case T__34:
			case T__35:
			case T__36:
			case T__37:
			case T__38:
			case T__39:
			case T__40:
			case T__41:
			case T__42:
			case T__43:
			case T__44:
			case T__45:
			case T__46:
			case T__47:
			case T__48:
			case T__51:
			case UPPERCASE_LETTER:
			case ZERO:
			case NONZERO_DIGIT:
				{
				setState(197);
				entity();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompositionContext extends ParserRuleContext {
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public List<ConstructionContext> construction() {
			return getRuleContexts(ConstructionContext.class);
		}
		public ConstructionContext construction(int i) {
			return getRuleContext(ConstructionContext.class,i);
		}
		public CompositionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_composition; }
	}

	public final CompositionContext composition() throws RecognitionException {
		CompositionContext _localctx = new CompositionContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_composition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(200);
			match(T__21);
			setState(201);
			optional_whitespace();
			setState(202);
			construction();
			setState(203);
			optional_whitespace();
			setState(204);
			construction();
			setState(208);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__20) | (1L << T__21) | (1L << T__24) | (1L << LOWERCASE_LETTER))) != 0)) {
				{
				{
				setState(205);
				construction();
				}
				}
				setState(210);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(211);
			optional_whitespace();
			setState(212);
			match(T__22);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClosureContext extends ParserRuleContext {
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Lambda_variablesContext lambda_variables() {
			return getRuleContext(Lambda_variablesContext.class,0);
		}
		public ConstructionContext construction() {
			return getRuleContext(ConstructionContext.class,0);
		}
		public ClosureContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_closure; }
	}

	public final ClosureContext closure() throws RecognitionException {
		ClosureContext _localctx = new ClosureContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_closure);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(214);
			match(T__21);
			setState(215);
			optional_whitespace();
			setState(216);
			lambda_variables();
			setState(217);
			optional_whitespace();
			setState(218);
			construction();
			setState(219);
			optional_whitespace();
			setState(220);
			match(T__22);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Lambda_variablesContext extends ParserRuleContext {
		public Optional_whitespaceContext optional_whitespace() {
			return getRuleContext(Optional_whitespaceContext.class,0);
		}
		public Typed_variablesContext typed_variables() {
			return getRuleContext(Typed_variablesContext.class,0);
		}
		public Lambda_variablesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lambda_variables; }
	}

	public final Lambda_variablesContext lambda_variables() throws RecognitionException {
		Lambda_variablesContext _localctx = new Lambda_variablesContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_lambda_variables);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(222);
			match(T__20);
			setState(223);
			optional_whitespace();
			setState(224);
			typed_variables();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Typed_variablesContext extends ParserRuleContext {
		public List<Typed_variableContext> typed_variable() {
			return getRuleContexts(Typed_variableContext.class);
		}
		public Typed_variableContext typed_variable(int i) {
			return getRuleContext(Typed_variableContext.class,i);
		}
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Typed_variablesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typed_variables; }
	}

	public final Typed_variablesContext typed_variables() throws RecognitionException {
		Typed_variablesContext _localctx = new Typed_variablesContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_typed_variables);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(226);
			typed_variable();
			setState(233);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(227);
					optional_whitespace();
					setState(228);
					match(T__3);
					setState(229);
					typed_variable();
					}
					} 
				}
				setState(235);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,12,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Typed_variableContext extends ParserRuleContext {
		public Lowerletter_nameContext lowerletter_name() {
			return getRuleContext(Lowerletter_nameContext.class,0);
		}
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public Data_typeContext data_type() {
			return getRuleContext(Data_typeContext.class,0);
		}
		public Typed_variableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typed_variable; }
	}

	public final Typed_variableContext typed_variable() throws RecognitionException {
		Typed_variableContext _localctx = new Typed_variableContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_typed_variable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(236);
			lowerletter_name();
			setState(242);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				{
				setState(237);
				optional_whitespace();
				setState(238);
				match(T__23);
				setState(239);
				optional_whitespace();
				setState(240);
				data_type();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class N_executionContext extends ParserRuleContext {
		public List<Optional_whitespaceContext> optional_whitespace() {
			return getRuleContexts(Optional_whitespaceContext.class);
		}
		public Optional_whitespaceContext optional_whitespace(int i) {
			return getRuleContext(Optional_whitespaceContext.class,i);
		}
		public TerminalNode NONZERO_DIGIT() { return getToken(TILGrammarPyParser.NONZERO_DIGIT, 0); }
		public ConstructionContext construction() {
			return getRuleContext(ConstructionContext.class,0);
		}
		public EntityContext entity() {
			return getRuleContext(EntityContext.class,0);
		}
		public N_executionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_n_execution; }
	}

	public final N_executionContext n_execution() throws RecognitionException {
		N_executionContext _localctx = new N_executionContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_n_execution);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(244);
			match(T__24);
			setState(245);
			optional_whitespace();
			setState(246);
			match(NONZERO_DIGIT);
			setState(247);
			optional_whitespace();
			setState(250);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__20:
			case T__21:
			case T__24:
			case LOWERCASE_LETTER:
				{
				setState(248);
				construction();
				}
				break;
			case T__4:
			case T__15:
			case T__25:
			case T__26:
			case T__27:
			case T__28:
			case T__29:
			case T__30:
			case T__31:
			case T__32:
			case T__33:
			case T__34:
			case T__35:
			case T__36:
			case T__37:
			case T__38:
			case T__39:
			case T__40:
			case T__41:
			case T__42:
			case T__43:
			case T__44:
			case T__45:
			case T__46:
			case T__47:
			case T__48:
			case T__51:
			case UPPERCASE_LETTER:
			case ZERO:
			case NONZERO_DIGIT:
				{
				setState(249);
				entity();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EntityContext extends ParserRuleContext {
		public KeywordContext keyword() {
			return getRuleContext(KeywordContext.class,0);
		}
		public Entity_nameContext entity_name() {
			return getRuleContext(Entity_nameContext.class,0);
		}
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public SymbolsContext symbols() {
			return getRuleContext(SymbolsContext.class,0);
		}
		public EntityContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entity; }
	}

	public final EntityContext entity() throws RecognitionException {
		EntityContext _localctx = new EntityContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_entity);
		try {
			setState(256);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__25:
			case T__26:
			case T__27:
			case T__28:
			case T__29:
			case T__30:
			case T__31:
			case T__32:
			case T__33:
			case T__34:
			case T__35:
			case T__36:
			case T__37:
			case T__38:
			case T__39:
			case T__40:
			case T__41:
			case T__42:
			case T__43:
			case T__44:
			case T__45:
				enterOuterAlt(_localctx, 1);
				{
				setState(252);
				keyword();
				}
				break;
			case T__51:
			case UPPERCASE_LETTER:
				enterOuterAlt(_localctx, 2);
				{
				setState(253);
				entity_name();
				}
				break;
			case ZERO:
			case NONZERO_DIGIT:
				enterOuterAlt(_localctx, 3);
				{
				setState(254);
				number();
				}
				break;
			case T__4:
			case T__15:
			case T__46:
			case T__47:
			case T__48:
				enterOuterAlt(_localctx, 4);
				{
				setState(255);
				symbols();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_nameContext extends ParserRuleContext {
		public Upperletter_nameContext upperletter_name() {
			return getRuleContext(Upperletter_nameContext.class,0);
		}
		public Type_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_name; }
	}

	public final Type_nameContext type_name() throws RecognitionException {
		Type_nameContext _localctx = new Type_nameContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_type_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(258);
			upperletter_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Entity_nameContext extends ParserRuleContext {
		public Upperletter_nameContext upperletter_name() {
			return getRuleContext(Upperletter_nameContext.class,0);
		}
		public Quoted_nameContext quoted_name() {
			return getRuleContext(Quoted_nameContext.class,0);
		}
		public Entity_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_entity_name; }
	}

	public final Entity_nameContext entity_name() throws RecognitionException {
		Entity_nameContext _localctx = new Entity_nameContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_entity_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(262);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case UPPERCASE_LETTER:
				{
				setState(260);
				upperletter_name();
				}
				break;
			case T__51:
				{
				setState(261);
				quoted_name();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Variable_nameContext extends ParserRuleContext {
		public Lowerletter_nameContext lowerletter_name() {
			return getRuleContext(Lowerletter_nameContext.class,0);
		}
		public Variable_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable_name; }
	}

	public final Variable_nameContext variable_name() throws RecognitionException {
		Variable_nameContext _localctx = new Variable_nameContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_variable_name);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(264);
			lowerletter_name();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeywordContext extends ParserRuleContext {
		public KeywordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyword; }
	}

	public final KeywordContext keyword() throws RecognitionException {
		KeywordContext _localctx = new KeywordContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_keyword);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(266);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << T__30) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__34) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39) | (1L << T__40) | (1L << T__41) | (1L << T__42) | (1L << T__43) | (1L << T__44) | (1L << T__45))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SymbolsContext extends ParserRuleContext {
		public SymbolsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_symbols; }
	}

	public final SymbolsContext symbols() throws RecognitionException {
		SymbolsContext _localctx = new SymbolsContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_symbols);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(268);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__15) | (1L << T__46) | (1L << T__47) | (1L << T__48))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AnythingContext extends ParserRuleContext {
		public List<TerminalNode> ZERO() { return getTokens(TILGrammarPyParser.ZERO); }
		public TerminalNode ZERO(int i) {
			return getToken(TILGrammarPyParser.ZERO, i);
		}
		public List<TerminalNode> NONZERO_DIGIT() { return getTokens(TILGrammarPyParser.NONZERO_DIGIT); }
		public TerminalNode NONZERO_DIGIT(int i) {
			return getToken(TILGrammarPyParser.NONZERO_DIGIT, i);
		}
		public AnythingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_anything; }
	}

	public final AnythingContext anything() throws RecognitionException {
		AnythingContext _localctx = new AnythingContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_anything);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(270);
			match(T__49);
			setState(274);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ZERO || _la==NONZERO_DIGIT) {
				{
				{
				setState(271);
				_la = _input.LA(1);
				if ( !(_la==ZERO || _la==NONZERO_DIGIT) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				}
				setState(276);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public List<TerminalNode> ZERO() { return getTokens(TILGrammarPyParser.ZERO); }
		public TerminalNode ZERO(int i) {
			return getToken(TILGrammarPyParser.ZERO, i);
		}
		public List<TerminalNode> NONZERO_DIGIT() { return getTokens(TILGrammarPyParser.NONZERO_DIGIT); }
		public TerminalNode NONZERO_DIGIT(int i) {
			return getToken(TILGrammarPyParser.NONZERO_DIGIT, i);
		}
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_number);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(277);
			_la = _input.LA(1);
			if ( !(_la==ZERO || _la==NONZERO_DIGIT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			setState(281);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ZERO || _la==NONZERO_DIGIT) {
				{
				{
				setState(278);
				_la = _input.LA(1);
				if ( !(_la==ZERO || _la==NONZERO_DIGIT) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
				}
				setState(283);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(292);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,20,_ctx) ) {
			case 1:
				{
				setState(284);
				match(T__0);
				setState(285);
				_la = _input.LA(1);
				if ( !(_la==ZERO || _la==NONZERO_DIGIT) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(289);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==ZERO || _la==NONZERO_DIGIT) {
					{
					{
					setState(286);
					_la = _input.LA(1);
					if ( !(_la==ZERO || _la==NONZERO_DIGIT) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
					}
					setState(291);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Upperletter_nameContext extends ParserRuleContext {
		public List<TerminalNode> UPPERCASE_LETTER() { return getTokens(TILGrammarPyParser.UPPERCASE_LETTER); }
		public TerminalNode UPPERCASE_LETTER(int i) {
			return getToken(TILGrammarPyParser.UPPERCASE_LETTER, i);
		}
		public List<TerminalNode> LOWERCASE_LETTER() { return getTokens(TILGrammarPyParser.LOWERCASE_LETTER); }
		public TerminalNode LOWERCASE_LETTER(int i) {
			return getToken(TILGrammarPyParser.LOWERCASE_LETTER, i);
		}
		public List<TerminalNode> ZERO() { return getTokens(TILGrammarPyParser.ZERO); }
		public TerminalNode ZERO(int i) {
			return getToken(TILGrammarPyParser.ZERO, i);
		}
		public List<TerminalNode> NONZERO_DIGIT() { return getTokens(TILGrammarPyParser.NONZERO_DIGIT); }
		public TerminalNode NONZERO_DIGIT(int i) {
			return getToken(TILGrammarPyParser.NONZERO_DIGIT, i);
		}
		public Upperletter_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_upperletter_name; }
	}

	public final Upperletter_nameContext upperletter_name() throws RecognitionException {
		Upperletter_nameContext _localctx = new Upperletter_nameContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_upperletter_name);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(294);
			match(UPPERCASE_LETTER);
			setState(298);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(295);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__50) | (1L << LOWERCASE_LETTER) | (1L << UPPERCASE_LETTER) | (1L << ZERO) | (1L << NONZERO_DIGIT))) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
					} 
				}
				setState(300);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,21,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Lowerletter_nameContext extends ParserRuleContext {
		public List<TerminalNode> LOWERCASE_LETTER() { return getTokens(TILGrammarPyParser.LOWERCASE_LETTER); }
		public TerminalNode LOWERCASE_LETTER(int i) {
			return getToken(TILGrammarPyParser.LOWERCASE_LETTER, i);
		}
		public List<TerminalNode> UPPERCASE_LETTER() { return getTokens(TILGrammarPyParser.UPPERCASE_LETTER); }
		public TerminalNode UPPERCASE_LETTER(int i) {
			return getToken(TILGrammarPyParser.UPPERCASE_LETTER, i);
		}
		public List<TerminalNode> ZERO() { return getTokens(TILGrammarPyParser.ZERO); }
		public TerminalNode ZERO(int i) {
			return getToken(TILGrammarPyParser.ZERO, i);
		}
		public List<TerminalNode> NONZERO_DIGIT() { return getTokens(TILGrammarPyParser.NONZERO_DIGIT); }
		public TerminalNode NONZERO_DIGIT(int i) {
			return getToken(TILGrammarPyParser.NONZERO_DIGIT, i);
		}
		public Lowerletter_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lowerletter_name; }
	}

	public final Lowerletter_nameContext lowerletter_name() throws RecognitionException {
		Lowerletter_nameContext _localctx = new Lowerletter_nameContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_lowerletter_name);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(301);
			match(LOWERCASE_LETTER);
			setState(305);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(302);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__50) | (1L << LOWERCASE_LETTER) | (1L << UPPERCASE_LETTER) | (1L << ZERO) | (1L << NONZERO_DIGIT))) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					}
					} 
				}
				setState(307);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,22,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Quoted_nameContext extends ParserRuleContext {
		public Quoted_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_quoted_name; }
	}

	public final Quoted_nameContext quoted_name() throws RecognitionException {
		Quoted_nameContext _localctx = new Quoted_nameContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_quoted_name);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(308);
			match(T__51);
			setState(312);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(309);
					match(T__51);
					}
					} 
				}
				setState(314);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,23,_ctx);
			}
			setState(315);
			match(T__51);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhitespaceContext extends ParserRuleContext {
		public TerminalNode WHITESPACE_CHARACTER() { return getToken(TILGrammarPyParser.WHITESPACE_CHARACTER, 0); }
		public Optional_whitespaceContext optional_whitespace() {
			return getRuleContext(Optional_whitespaceContext.class,0);
		}
		public WhitespaceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whitespace; }
	}

	public final WhitespaceContext whitespace() throws RecognitionException {
		WhitespaceContext _localctx = new WhitespaceContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_whitespace);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(317);
			match(WHITESPACE_CHARACTER);
			setState(318);
			optional_whitespace();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Optional_whitespaceContext extends ParserRuleContext {
		public List<TerminalNode> SPACE() { return getTokens(TILGrammarPyParser.SPACE); }
		public TerminalNode SPACE(int i) {
			return getToken(TILGrammarPyParser.SPACE, i);
		}
		public Optional_whitespaceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_optional_whitespace; }
	}

	public final Optional_whitespaceContext optional_whitespace() throws RecognitionException {
		Optional_whitespaceContext _localctx = new Optional_whitespaceContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_optional_whitespace);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(323);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE) {
				{
				{
				setState(320);
				match(SPACE);
				}
				}
				setState(325);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3<\u0149\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\3\2\7\2J\n\2\f\2\16\2M\13\2\3\3\3\3\3\3\3\4\3"+
		"\4\3\4\3\4\5\4V\n\4\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3"+
		"\7\3\7\3\7\3\7\3\7\3\7\7\7j\n\7\f\7\16\7m\13\7\3\7\3\7\3\7\3\7\3\7\3\b"+
		"\3\b\3\b\3\b\3\b\5\by\n\b\3\b\5\b|\n\b\3\t\3\t\3\t\3\t\3\t\3\t\7\t\u0084"+
		"\n\t\f\t\16\t\u0087\13\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\5\n\u0093"+
		"\n\n\3\n\5\n\u0096\n\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5"+
		"\13\u00a1\n\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r"+
		"\3\r\3\r\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\17\7\17\u00bb\n\17\f\17"+
		"\16\17\u00be\13\17\3\17\3\17\3\17\3\20\3\20\3\21\3\21\3\21\3\21\5\21\u00c9"+
		"\n\21\3\22\3\22\3\22\3\22\3\22\3\22\7\22\u00d1\n\22\f\22\16\22\u00d4\13"+
		"\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3"+
		"\24\3\24\3\25\3\25\3\25\3\25\3\25\7\25\u00ea\n\25\f\25\16\25\u00ed\13"+
		"\25\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u00f5\n\26\3\27\3\27\3\27\3\27"+
		"\3\27\3\27\5\27\u00fd\n\27\3\30\3\30\3\30\3\30\5\30\u0103\n\30\3\31\3"+
		"\31\3\32\3\32\5\32\u0109\n\32\3\33\3\33\3\34\3\34\3\35\3\35\3\36\3\36"+
		"\7\36\u0113\n\36\f\36\16\36\u0116\13\36\3\37\3\37\7\37\u011a\n\37\f\37"+
		"\16\37\u011d\13\37\3\37\3\37\3\37\7\37\u0122\n\37\f\37\16\37\u0125\13"+
		"\37\5\37\u0127\n\37\3 \3 \7 \u012b\n \f \16 \u012e\13 \3!\3!\7!\u0132"+
		"\n!\f!\16!\u0135\13!\3\"\3\"\7\"\u0139\n\"\f\"\16\"\u013c\13\"\3\"\3\""+
		"\3#\3#\3#\3$\7$\u0144\n$\f$\16$\u0147\13$\3$\2\2%\2\4\6\b\n\f\16\20\22"+
		"\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDF\2\6\3\2\34\60\5\2\7\7"+
		"\22\22\61\63\3\29:\4\2\65\65\67:\2\u014f\2K\3\2\2\2\4N\3\2\2\2\6U\3\2"+
		"\2\2\bW\3\2\2\2\n[\3\2\2\2\fc\3\2\2\2\16x\3\2\2\2\20}\3\2\2\2\22\u0092"+
		"\3\2\2\2\24\u00a0\3\2\2\2\26\u00a2\3\2\2\2\30\u00aa\3\2\2\2\32\u00b2\3"+
		"\2\2\2\34\u00b4\3\2\2\2\36\u00c2\3\2\2\2 \u00c4\3\2\2\2\"\u00ca\3\2\2"+
		"\2$\u00d8\3\2\2\2&\u00e0\3\2\2\2(\u00e4\3\2\2\2*\u00ee\3\2\2\2,\u00f6"+
		"\3\2\2\2.\u0102\3\2\2\2\60\u0104\3\2\2\2\62\u0108\3\2\2\2\64\u010a\3\2"+
		"\2\2\66\u010c\3\2\2\28\u010e\3\2\2\2:\u0110\3\2\2\2<\u0117\3\2\2\2>\u0128"+
		"\3\2\2\2@\u012f\3\2\2\2B\u0136\3\2\2\2D\u013f\3\2\2\2F\u0145\3\2\2\2H"+
		"J\5\4\3\2IH\3\2\2\2JM\3\2\2\2KI\3\2\2\2KL\3\2\2\2L\3\3\2\2\2MK\3\2\2\2"+
		"NO\5\6\4\2OP\5\b\5\2P\5\3\2\2\2QV\5\n\6\2RV\5\f\7\2SV\5\16\b\2TV\5\20"+
		"\t\2UQ\3\2\2\2UR\3\2\2\2US\3\2\2\2UT\3\2\2\2V\7\3\2\2\2WX\5F$\2XY\7\3"+
		"\2\2YZ\5F$\2Z\t\3\2\2\2[\\\7\4\2\2\\]\5D#\2]^\5\60\31\2^_\5F$\2_`\7\5"+
		"\2\2`a\5F$\2ab\5\22\n\2b\13\3\2\2\2ck\5\62\32\2de\5F$\2ef\7\6\2\2fg\5"+
		"F$\2gh\5\62\32\2hj\3\2\2\2id\3\2\2\2jm\3\2\2\2ki\3\2\2\2kl\3\2\2\2ln\3"+
		"\2\2\2mk\3\2\2\2no\5F$\2op\7\7\2\2pq\5F$\2qr\5\22\n\2r\r\3\2\2\2sy\5 "+
		"\21\2ty\5\36\20\2uy\5$\23\2vy\5,\27\2wy\5\"\22\2xs\3\2\2\2xt\3\2\2\2x"+
		"u\3\2\2\2xv\3\2\2\2xw\3\2\2\2y{\3\2\2\2z|\7\b\2\2{z\3\2\2\2{|\3\2\2\2"+
		"|\17\3\2\2\2}\u0085\5\64\33\2~\177\5F$\2\177\u0080\7\6\2\2\u0080\u0081"+
		"\5F$\2\u0081\u0082\5\64\33\2\u0082\u0084\3\2\2\2\u0083~\3\2\2\2\u0084"+
		"\u0087\3\2\2\2\u0085\u0083\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0088\3\2"+
		"\2\2\u0087\u0085\3\2\2\2\u0088\u0089\5F$\2\u0089\u008a\7\t\2\2\u008a\u008b"+
		"\5F$\2\u008b\u008c\5\22\n\2\u008c\21\3\2\2\2\u008d\u0093\5\24\13\2\u008e"+
		"\u0093\5\26\f\2\u008f\u0093\5\30\r\2\u0090\u0093\5\32\16\2\u0091\u0093"+
		"\5\34\17\2\u0092\u008d\3\2\2\2\u0092\u008e\3\2\2\2\u0092\u008f\3\2\2\2"+
		"\u0092\u0090\3\2\2\2\u0092\u0091\3\2\2\2\u0093\u0095\3\2\2\2\u0094\u0096"+
		"\7\n\2\2\u0095\u0094\3\2\2\2\u0095\u0096\3\2\2\2\u0096\23\3\2\2\2\u0097"+
		"\u00a1\7\13\2\2\u0098\u00a1\7\f\2\2\u0099\u00a1\7\r\2\2\u009a\u00a1\7"+
		"\16\2\2\u009b\u00a1\7\17\2\2\u009c\u00a1\7\20\2\2\u009d\u00a1\7\21\2\2"+
		"\u009e\u00a1\5:\36\2\u009f\u00a1\7\22\2\2\u00a0\u0097\3\2\2\2\u00a0\u0098"+
		"\3\2\2\2\u00a0\u0099\3\2\2\2\u00a0\u009a\3\2\2\2\u00a0\u009b\3\2\2\2\u00a0"+
		"\u009c\3\2\2\2\u00a0\u009d\3\2\2\2\u00a0\u009e\3\2\2\2\u00a0\u009f\3\2"+
		"\2\2\u00a1\25\3\2\2\2\u00a2\u00a3\7\23\2\2\u00a3\u00a4\5F$\2\u00a4\u00a5"+
		"\7\24\2\2\u00a5\u00a6\5F$\2\u00a6\u00a7\5\22\n\2\u00a7\u00a8\5F$\2\u00a8"+
		"\u00a9\7\25\2\2\u00a9\27\3\2\2\2\u00aa\u00ab\7\26\2\2\u00ab\u00ac\5F$"+
		"\2\u00ac\u00ad\7\24\2\2\u00ad\u00ae\5F$\2\u00ae\u00af\5\22\n\2\u00af\u00b0"+
		"\5F$\2\u00b0\u00b1\7\25\2\2\u00b1\31\3\2\2\2\u00b2\u00b3\5\60\31\2\u00b3"+
		"\33\3\2\2\2\u00b4\u00b5\7\24\2\2\u00b5\u00b6\5F$\2\u00b6\u00bc\5\22\n"+
		"\2\u00b7\u00b8\5D#\2\u00b8\u00b9\5\22\n\2\u00b9\u00bb\3\2\2\2\u00ba\u00b7"+
		"\3\2\2\2\u00bb\u00be\3\2\2\2\u00bc\u00ba\3\2\2\2\u00bc\u00bd\3\2\2\2\u00bd"+
		"\u00bf\3\2\2\2\u00be\u00bc\3\2\2\2\u00bf\u00c0\5F$\2\u00c0\u00c1\7\25"+
		"\2\2\u00c1\35\3\2\2\2\u00c2\u00c3\5\64\33\2\u00c3\37\3\2\2\2\u00c4\u00c5"+
		"\7\27\2\2\u00c5\u00c8\5F$\2\u00c6\u00c9\5\16\b\2\u00c7\u00c9\5.\30\2\u00c8"+
		"\u00c6\3\2\2\2\u00c8\u00c7\3\2\2\2\u00c9!\3\2\2\2\u00ca\u00cb\7\30\2\2"+
		"\u00cb\u00cc\5F$\2\u00cc\u00cd\5\16\b\2\u00cd\u00ce\5F$\2\u00ce\u00d2"+
		"\5\16\b\2\u00cf\u00d1\5\16\b\2\u00d0\u00cf\3\2\2\2\u00d1\u00d4\3\2\2\2"+
		"\u00d2\u00d0\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3\u00d5\3\2\2\2\u00d4\u00d2"+
		"\3\2\2\2\u00d5\u00d6\5F$\2\u00d6\u00d7\7\31\2\2\u00d7#\3\2\2\2\u00d8\u00d9"+
		"\7\30\2\2\u00d9\u00da\5F$\2\u00da\u00db\5&\24\2\u00db\u00dc\5F$\2\u00dc"+
		"\u00dd\5\16\b\2\u00dd\u00de\5F$\2\u00de\u00df\7\31\2\2\u00df%\3\2\2\2"+
		"\u00e0\u00e1\7\27\2\2\u00e1\u00e2\5F$\2\u00e2\u00e3\5(\25\2\u00e3\'\3"+
		"\2\2\2\u00e4\u00eb\5*\26\2\u00e5\u00e6\5F$\2\u00e6\u00e7\7\6\2\2\u00e7"+
		"\u00e8\5*\26\2\u00e8\u00ea\3\2\2\2\u00e9\u00e5\3\2\2\2\u00ea\u00ed\3\2"+
		"\2\2\u00eb\u00e9\3\2\2\2\u00eb\u00ec\3\2\2\2\u00ec)\3\2\2\2\u00ed\u00eb"+
		"\3\2\2\2\u00ee\u00f4\5@!\2\u00ef\u00f0\5F$\2\u00f0\u00f1\7\32\2\2\u00f1"+
		"\u00f2\5F$\2\u00f2\u00f3\5\22\n\2\u00f3\u00f5\3\2\2\2\u00f4\u00ef\3\2"+
		"\2\2\u00f4\u00f5\3\2\2\2\u00f5+\3\2\2\2\u00f6\u00f7\7\33\2\2\u00f7\u00f8"+
		"\5F$\2\u00f8\u00f9\7:\2\2\u00f9\u00fc\5F$\2\u00fa\u00fd\5\16\b\2\u00fb"+
		"\u00fd\5.\30\2\u00fc\u00fa\3\2\2\2\u00fc\u00fb\3\2\2\2\u00fd-\3\2\2\2"+
		"\u00fe\u0103\5\66\34\2\u00ff\u0103\5\62\32\2\u0100\u0103\5<\37\2\u0101"+
		"\u0103\58\35\2\u0102\u00fe\3\2\2\2\u0102\u00ff\3\2\2\2\u0102\u0100\3\2"+
		"\2\2\u0102\u0101\3\2\2\2\u0103/\3\2\2\2\u0104\u0105\5> \2\u0105\61\3\2"+
		"\2\2\u0106\u0109\5> \2\u0107\u0109\5B\"\2\u0108\u0106\3\2\2\2\u0108\u0107"+
		"\3\2\2\2\u0109\63\3\2\2\2\u010a\u010b\5@!\2\u010b\65\3\2\2\2\u010c\u010d"+
		"\t\2\2\2\u010d\67\3\2\2\2\u010e\u010f\t\3\2\2\u010f9\3\2\2\2\u0110\u0114"+
		"\7\64\2\2\u0111\u0113\t\4\2\2\u0112\u0111\3\2\2\2\u0113\u0116\3\2\2\2"+
		"\u0114\u0112\3\2\2\2\u0114\u0115\3\2\2\2\u0115;\3\2\2\2\u0116\u0114\3"+
		"\2\2\2\u0117\u011b\t\4\2\2\u0118\u011a\t\4\2\2\u0119\u0118\3\2\2\2\u011a"+
		"\u011d\3\2\2\2\u011b\u0119\3\2\2\2\u011b\u011c\3\2\2\2\u011c\u0126\3\2"+
		"\2\2\u011d\u011b\3\2\2\2\u011e\u011f\7\3\2\2\u011f\u0123\t\4\2\2\u0120"+
		"\u0122\t\4\2\2\u0121\u0120\3\2\2\2\u0122\u0125\3\2\2\2\u0123\u0121\3\2"+
		"\2\2\u0123\u0124\3\2\2\2\u0124\u0127\3\2\2\2\u0125\u0123\3\2\2\2\u0126"+
		"\u011e\3\2\2\2\u0126\u0127\3\2\2\2\u0127=\3\2\2\2\u0128\u012c\78\2\2\u0129"+
		"\u012b\t\5\2\2\u012a\u0129\3\2\2\2\u012b\u012e\3\2\2\2\u012c\u012a\3\2"+
		"\2\2\u012c\u012d\3\2\2\2\u012d?\3\2\2\2\u012e\u012c\3\2\2\2\u012f\u0133"+
		"\7\67\2\2\u0130\u0132\t\5\2\2\u0131\u0130\3\2\2\2\u0132\u0135\3\2\2\2"+
		"\u0133\u0131\3\2\2\2\u0133\u0134\3\2\2\2\u0134A\3\2\2\2\u0135\u0133\3"+
		"\2\2\2\u0136\u013a\7\66\2\2\u0137\u0139\7\66\2\2\u0138\u0137\3\2\2\2\u0139"+
		"\u013c\3\2\2\2\u013a\u0138\3\2\2\2\u013a\u013b\3\2\2\2\u013b\u013d\3\2"+
		"\2\2\u013c\u013a\3\2\2\2\u013d\u013e\7\66\2\2\u013eC\3\2\2\2\u013f\u0140"+
		"\7<\2\2\u0140\u0141\5F$\2\u0141E\3\2\2\2\u0142\u0144\7;\2\2\u0143\u0142"+
		"\3\2\2\2\u0144\u0147\3\2\2\2\u0145\u0143\3\2\2\2\u0145\u0146\3\2\2\2\u0146"+
		"G\3\2\2\2\u0147\u0145\3\2\2\2\33KUkx{\u0085\u0092\u0095\u00a0\u00bc\u00c8"+
		"\u00d2\u00eb\u00f4\u00fc\u0102\u0108\u0114\u011b\u0123\u0126\u012c\u0133"+
		"\u013a\u0145";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}