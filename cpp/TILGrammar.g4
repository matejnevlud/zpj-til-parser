//	https://github.com/antlr/antlr4/blob/master/doc/getting-started.md

grammar TILGrammar;

start : sentence*;

sentence : sentence_content termination;

sentence_content : type_definition | entity_definition | construction | global_variable_definition
;

termination : optional_whitespace '.' optional_whitespace;

type_definition : 'TypeDef' whitespace type_name optional_whitespace ':=' optional_whitespace data_type
;



entity_definition : entity_name ( optional_whitespace ',' optional_whitespace entity_name )* optional_whitespace '/' optional_whitespace data_type 
;


construction : ( trivialisation | variable | closure | n_execution | composition ) '@wt'?
;

global_variable_definition : variable_name (optional_whitespace ',' optional_whitespace variable_name)* optional_whitespace '->' optional_whitespace data_type
;


data_type : ( embeded_type | list_type | touple_type | user_type | enclosed_data_type ) '@tw'?
;

embeded_type : 'Bool' | 'Indiv' | 'Time' | 'String' | 'World' | 'Real' | 'Int' | anything | '*'
;

list_type : 'List' optional_whitespace '(' optional_whitespace data_type optional_whitespace ')';

touple_type : 'Tuple' optional_whitespace '(' optional_whitespace data_type optional_whitespace ')';

user_type : type_name;


enclosed_data_type : '(' optional_whitespace data_type (whitespace data_type)* optional_whitespace ')';

variable : variable_name;

trivialisation : '\'' optional_whitespace (construction | entity);


composition : '[' optional_whitespace construction optional_whitespace construction (construction)* optional_whitespace ']'
;

closure : '[' optional_whitespace lambda_variables optional_whitespace construction optional_whitespace ']'
;

lambda_variables : '\'' optional_whitespace typed_variables;


typed_variables : typed_variable (optional_whitespace ',' typed_variable)*;

typed_variable : lowerletter_name (optional_whitespace ':' optional_whitespace data_type)?;

n_execution : '^' optional_whitespace NONZERO_DIGIT optional_whitespace ( construction | entity );

entity : keyword | entity_name | number | symbols;

type_name : upperletter_name;
           
entity_name : (upperletter_name | quoted_name); 

variable_name : lowerletter_name;



keyword : 
	'ForAll' | 
	'Exist' | 
	'Every' | 
	'Some' | 
	'No' | 
	'True' | 
	'False' |
	'And' |
	'Or' |
	'Not' |
	'Implies' |
	'Sing' |
	'Sub' |
	'Tr' |
	'TrueC' |
	'FalseC' |
	'ImproperC' |
	'TrueP' |
	'FalseP' |
	'UndefP' |
	'ToInt'
;

LOWERCASE_LETTER : 'a' .. 'z';
UPPERCASE_LETTER : 'A' .. 'Z';


symbols : '+' | '-' | '*' | '/'| '=';

ZERO : '0' ;

NONZERO_DIGIT : '1' .. '9';

anything : 'Any' (ZERO | NONZERO_DIGIT)*;

number : ( ZERO | NONZERO_DIGIT) (ZERO | NONZERO_DIGIT )* ('.' (ZERO | NONZERO_DIGIT) (ZERO | NONZERO_DIGIT)* )?;

upperletter_name : UPPERCASE_LETTER ( LOWERCASE_LETTER | UPPERCASE_LETTER | '_' | ZERO | NONZERO_DIGIT )*;

lowerletter_name : LOWERCASE_LETTER ( LOWERCASE_LETTER | UPPERCASE_LETTER | '_' | ZERO | NONZERO_DIGIT )*;

quoted_name: '%' '%'* '%'; 

whitespace : WHITESPACE_CHARACTER optional_whitespace;

optional_whitespace : ( WHITESPACE_CHARACTER )*;

WHITESPACE_CHARACTER : [ \r\n\t]+;



